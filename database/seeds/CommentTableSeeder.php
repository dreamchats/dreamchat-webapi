<?php

use Illuminate\Database\Seeder;

class CommentTableSeeder extends Seeder
{
    public function run()
    {
        $shops = [
            [
                'comments_description' => 'Books',
                    'replies' => [
                        [    
                            'comments_description' => 'Comic Book',
                            'replies' => [
                                    ['comments_description' => 'Marvel Comic Book'],
                                    ['comments_description' => 'DC Comic Book'],
                                    ['comments_description' => 'Action comics'],
                            ],
                        ],
                        [    
                            'comments_description' => 'Textbooks',
                                'replies' => [
                                    ['comments_description' => 'Business'],
                                    ['comments_description' => 'Finance'],
                                    ['comments_description' => 'Computer Science'],
                            ],
                        ],
                    ],
                ],
                [
                    'comments_description' => 'Electronics',
                        'replies' => [
                        [
                            'comments_description' => 'TV',
                            'replies' => [
                                ['comments_description' => 'LED'],
                                ['comments_description' => 'Blu-ray'],
                            ],
                        ],
                        [
                            'comments_description' => 'Mobile',
                            'replies' => [
                                ['comments_description' => 'Samsung'],
                                ['comments_description' => 'iPhone'],
                                ['comments_description' => 'Xiomi'],
                            ],
                        ],
                    ],
                ],
        ];
        foreach($shops as $shop)
        {
            \App\Comments::create($shop);
        }
    }
}
?>