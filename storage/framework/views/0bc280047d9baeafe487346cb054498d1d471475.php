<?php $__env->startSection('content'); ?>
    <!-- <div class="kt-grid kt-grid--ver kt-grid--root kt-page">
        <div class="kt-grid kt-grid--hor kt-grid--root kt-login kt-login--v2 kt-login--signin" id="kt_login">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-image: url(<?php echo e(asset('/media/bg/bg-1.jpg')); ?>)">
                <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                    <div class="kt-login__container">
                        <div class="kt-login__logo">
                            <a href="#">
                                <img src="<?php echo e(asset('/media/logos/logo-mini-2-md.png')); ?>">
                            </a>
                        </div>
                        <div class="kt-login__signin">
                            <div class="kt-login__head">
                                <h3 class="kt-login__title">Sign In To Admin</h3>
                            </div>
                            <form class="kt-form"  method="POST" action="<?php echo e(route('login')); ?>">
                                 <?php echo csrf_field(); ?>
                            <div class="input-group">
                                <input id="email" type="email" class="form-control <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="email" value="<?php echo e(old('email')); ?>" required autocomplete="email" placeholder="Email" autofocus>
                                <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>
                            <div class="input-group">
                                <input id="password" type="password" class="form-control <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="password" required autocomplete="current-password" placeholder="Password">
                                <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($message); ?></strong>
                                    </span>
                                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                            </div>
                        
                        <div class="row kt-login__extra">
                            <div class="col">
                                <label class="kt-checkbox">
                                   <input class="form-check-input" type="checkbox" name="remember" id="remember" <?php echo e(old('remember') ? 'checked' : ''); ?>>

                                    <label class="form-check-label" for="remember">
                                        <?php echo e(__('Remember Me')); ?>

                                    </label>
                                    <span></span>
                                </label>
                            </div>
                            <div class="col kt-align-right">
                                <a href="javascript:;" id="kt_login_forgot" class="kt-link kt-login__link">Forget Password ?</a>
                            </div>
                        </div>
                        <div class="kt-login__actions">
                         <button type="submit" class="btn btn-pill kt-login__btn-primary" >
                                    <?php echo e(__('Sign In')); ?>

                                </button>
                        </div>
                        </form>
                        </div>
                           <div class="kt-login__forgot">
                                <div class="kt-login__head">
                                    <div class="kt-login__desc">Enter your email to reset your password:</div>
                                </div>                
                                <form class="kt-form" method="POST" action="<?php echo e(route('password.email')); ?>">
                                    <?php echo csrf_field(); ?>

                                    <div class="input-group">
                                       <input id="kt_email" type="email" class="form-control <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="email" value="<?php echo e(old('email')); ?>" placeholder="Email" required autocomplete="email" autofocus>

                                        <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($message); ?></strong>
                                            </span>
                                        <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                                    </div>
                                    <div class="kt-login__actions">
                                        <button class="btn btn-pill kt-login__btn-primary">Request</button>&nbsp;&nbsp;
                                        <button id="kt_login_forgot_cancel" class="btn btn-pill kt-login__btn-secondary">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
    <img class="wave" src="<?php echo e(asset('images/login/wave.png')); ?>">
<div class="container">
    <div class="img">
        <img src="<?php echo e(asset('images/login/bg.svg')); ?>">
    </div>
    <div class="login-content" id="Div1">
        <form method="POST" action="<?php echo e(route('login')); ?>">
            <?php echo csrf_field(); ?>
            <img src="<?php echo e(asset('images/login/male.svg')); ?>">
            <h2 class="title">Sign In To Admin</h2>
        <div class="input-div one">
            <div class="i">
                    <i class="fas fa-user"></i>
            </div>
            <div class="div">
                    <!-- <h5>Email</h5> -->
                    <input id="email" type="email" class="form-control <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="email" value="<?php echo e(old('email')); ?>" required autocomplete="email" placeholder="Email" autofocus>
                    <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?>
                    <span class="invalid-feedback login-invlaid" role="alert">
                     <strong><?php echo e($message); ?></strong>
                     </span>
                     <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
            </div>
        </div>
        <div class="input-div pass">
             <div class="i"> 
                      <i class="fas fa-lock"></i>
             </div>
            <div class="div">
                        <!-- <h5>Password</h5> -->
                        <input id="password" type="password" class="form-control <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="password" required autocomplete="current-password" placeholder="Password">
                        <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?>
                        <span class="invalid-feedback login-invlaid" role="alert">
                             <strong><?php echo e($message); ?></strong>
                        </span>
                 <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
            </div>
        </div>
                <!-- <a href="#">Forgot Password?</a> -->
            <div class="row kt-login__extra remberme">
            <div class="col">
                    <label class="kt-checkbox">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" <?php echo e(old('remember') ? 'checked' : ''); ?>>
                    <label class="form-check-label checkme" for="remember">
                        <?php echo e(__('Remember Me')); ?>

                     </label>
                    <span></span>
                 </label>
            </div>
            <div class="col kt-align-right">
                    <a href="javascript:;" id="kt_login_forgot" onclick="switchVisible();" class="kt-link kt-login__link">Forget Password ?</a>
            </div>
            </div>
                    <input type="submit" class="btn login-btn" value="Login">
            <div class="" style="display:none">
                   Don't have an account yet ?    <a href="javascript:;"  onclick="switchVisible();">Sign Up</a> 
            </div>
        </form>
    </div>
      <!-- <a href="<?php echo e(route('social.oauth', 'facebook')); ?>" class="btn btn-primary btn-block">
            Login with Facebook
        </a> -->
          <a href="<?php echo e(url('auth/facebook')); ?>" class="btn btn-lg btn-primary btn-block">
                <strong>Login With Facebook</strong>
            </a>     
         <a href="<?php echo e(route('social.oauth', 'google')); ?>" class="btn btn-danger btn-block">
            Login with Google
        </a>

        <div class="login-contents" id="Div2">
            <div class="kt-login__head">
                    <h2 class="forgottitle">Forgotten Password ?</h2>
            <div class="kt-login__desc">Enter your email to reset your password:</div>
            </div>                
            <form style="width: 360px;" class="kt-form forgotform" method="POST" action="<?php echo e(route('password.email')); ?>">
                            <?php echo csrf_field(); ?>
                     <div class="input-group">
                            <input id="kt_email" type="email" class="form-control <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="email" value="<?php echo e(old('email')); ?>" placeholder="Email" required autocomplete="email" autofocus>
                            <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?>
                            <span class="invalid-feedback" role="alert">
                             <strong><?php echo e($message); ?></strong>
                            </span>
                            <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
                     </div>
                    <div class="kt-login__actions requestbtn">
                            <button class="btn btn-pill kt-login__btn-primary">Request</button>&nbsp;&nbsp;
                            <button id="kt_login_forgot_cancel" onclick="switchVisible()" class="btn btn-pill kt-login__btn-secondary">Cancel</button>
                </div>
            </form>
        </div>  
    </div>    
    <div class="login-contents" id="Div3">
         sign up div
    </div>

<?php $__env->stopSection(); ?>





                       
<?php echo $__env->make('layouts.login', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/kaspon-techworks/lampstack-7.3.23-0/apache2/htdocs/dreamchat/dreamchat/resources/views/auth/login.blade.php ENDPATH**/ ?>