// Class definition

var KTFormControlsCredits = function () {
    // Private functions
     var personal_validation = function () {
        $( "#personal_form" ).validate({
            // define validation rules
            rules: {
                //= Client Information(step 3)
                // Billing Information

                full_name: {
                    required: true,
                    
                },
                gender: {
                    required: true
                },
                phone_number: {
                    required: true,
                     digits: true,
                    minlength: 10,
                    maxlength: 10
                },
                dob: {
                    required: true
                },
                 
            },

            //display error alert on form submit
            invalidHandler: function(event, validator) {
                swal.fire({
                    "title": "",
                    "text": "There are some errors in your submission. Please correct them.",
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary",
                    "onClose": function(e) {
                        console.log('on close event fired!');
                    }
                });

                event.preventDefault();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
                swal.fire({
                    "title": "",
                    "text": "Form validation passed. All good!",
                    "type": "success",
                    "confirmButtonClass": "btn btn-secondary"
                });

                return false;
            }
        });
    }
    var edit_credit_validation = function () {
        $( "#kt_form_edit_credits" ).validate({
            // define validation rules
            rules: {
                //= Client Information(step 3)
                // Billing Information

                edit_credit_points: {
                    required: true,
                     digits: true
                },
                edit_amount: {
                    required: true,
                    digits: true
                },
            },

            //display error alert on form submit
            invalidHandler: function(event, validator) {
                swal.fire({
                    "title": "",
                    "text": "There are some errors in your submission. Please correct them.",
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary",
                    "onClose": function(e) {
                        console.log('on close event fired!');
                    }
                });

                event.preventDefault();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
                swal.fire({
                    "title": "",
                    "text": "Form validation passed. All good!",
                    "type": "success",
                    "confirmButtonClass": "btn btn-secondary"
                });

                return false;
            }
        });
    }
    var demo2 = function () {
        $( "#kt_form_credits" ).validate({
            // define validation rules
            rules: {
                //= Client Information(step 3)
                // Billing Information

                credits_points: {
                    required: true,
                     digits: true
                },
                amount: {
                    required: true
                },
            },

            //display error alert on form submit
            invalidHandler: function(event, validator) {
                swal.fire({
                    "title": "",
                    "text": "There are some errors in your submission. Please correct them.",
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary",
                    "onClose": function(e) {
                        console.log('on close event fired!');
                    }
                });

                event.preventDefault();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
                // swal.fire({
                //     "title": "",
                //     "text": "Form validation passed. All good!",
                //     "type": "success",
                //     "confirmButtonClass": "btn btn-secondary"
                // });

                return false;
            }
        });
    }
 var cms_validation = function () {
        $( "#kt_form_cms" ).validate({
            // define validation rules
            rules: {
                //= Client Information(step 3)
                // Billing Information

                title: {
                    required: true,
                    minlength: 5,
                    maxlength: 50
                },
                content: {
                    required: true
                },
            },

            //display error alert on form submit
            invalidHandler: function(event, validator) {
                swal.fire({
                    "title": "",
                    "text": "There are some errors in your submission. Please correct them.",
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary",
                    "onClose": function(e) {
                        console.log('on close event fired!');
                    }
                });

                event.preventDefault();
            },

            submitHandler: function (form) {
                form[0].submit(); // submit the form
                swal.fire({
                    "title": "",
                    "text": "Form validation passed. All good!",
                    "type": "success",
                    "confirmButtonClass": "btn btn-secondary"
                });

                return false;
            }
        });
    }

    return {
        // public functions
        init: function() {
            demo2();
            cms_validation();
            edit_credit_validation();
            personal_validation();
        }
    };
}();

jQuery(document).ready(function() {
    KTFormControlsCredits.init();
});