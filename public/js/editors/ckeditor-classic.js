"use strict";
// Class definition

var KTCkeditor = function () {    
    // Private functions
    var ckedit = function () {
        ClassicEditor
			.create( document.querySelector( '#content' ) )
			.then( editor => {
				console.log( editor );
			} )
			.catch( error => {
				console.error( error );
			} );

		// ClassicEditor
		// 	.create( document.querySelector( '#edit_content' ) )
		// 	.then( editor => {
		// 		console.log( editor );
		// 	} )
		// 	.catch( error => {
		// 		console.error( error );
		// 	} );
    }

    return {
        // public functions
        init: function() {
            ckedit(); 
        }
    };
}();

// Initialization
jQuery(document).ready(function() {
    KTCkeditor.init();
});