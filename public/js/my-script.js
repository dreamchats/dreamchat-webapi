$EmailMsg = $('#EmailMsg').val();
if ($EmailMsg == '') {
	$('#EmailMsg').hide();
}
$(document).on('change','#email', function(){
	email_check();
});
function email_check(){
	$('#EmailMsg').html('');
	$('#EmailMsg').show();
	$email = $('#email').val();
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
	if ($email != '') {
		$.ajax({
			type: 'POST',
			url: 'unique_email',
			data: { _token: CSRF_TOKEN, email: $email },
			success: function(response) {
				if (response.success == 1) {
					$('#EmailMsg').html(response.message);
					$("#btn_submit").attr("disabled", true);

				}
				else{
					$("#btn_submit").attr("disabled", false);

				}
			}
		});
	}else{
		$('#EmailMsg').hide();
	}
}
$(document).on('change','#password-confirm', function(){
	$password = $('#password').val();
	if ($password == '') {
		$('#password_check').hide();
	}else{
		Validate();	
	}
});
function Validate() {
    var password = $('#password').val();
    var confirmPassword = $('#password-confirm').val()
    if (password != confirmPassword) {
    	// console.log(password);
    	// console.log(confirmPassword);
        // alert("Passwords do not match.");
        $('#password_check').show();
        $('#mismatch').html("Passwords do not match");
        $("#btn_submit").attr("disabled", true);

        return false;
    }else{
    	 $('#password_check').hide();
    	 $('#mismatch').html("");
    	 $("#btn_submit").attr("disabled", false);


    }
    return true;
}