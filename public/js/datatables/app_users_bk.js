 "use strict";
// Class definition



var KTDatatableJsonRemoteDemo_admin = function () {
	// Private functions
	var path = window.location.origin;

	// basic demo
	var demo1 = function () {


		var datatable = $('.kt-datatable-users').KTDatatable({
			// datasource definition
			data: {
				type: 'remote',
				source: 'users',
				pageSize: 10,
			},

			// layout definition
			layout: {
				scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
				footer: false // display/hide footer
			},

			// column sorting
			sortable: true,

			pagination: true,

			search: {
				input: $('#generalSearch')
			},

			// columns definition
			columns: [
			{
					field: 'RecordID',
					title: '#',
					sortable: false,
					width: 20,
					type: 'number',
					selector: {class: 'kt-checkbox--solid'},
					textAlign: 'center',
				},{
					field: 'full_name',
					title: 'Full Name',
				},{
					field: 'email',
					title: 'Email',
				}
				,{
					field: 'remains_credit',
					title: 'Remains Credits',
					template: function(row) {
						console.log(row);
						if(row.user_credits != null){
							return row.user_credits['remains_credit'];
						}
						else{
							return '0';
						}
					},
				},{
					field: 'account_type',
					title: 'Account type',
					template: function(row) {
						// console.log(row.user_credits);
						var account_type = {
							1: {'title': 'Private', 'state': 'success'},
							0: {'title': 'Public', 'state': 'primary'},
						};
						return '<span class="kt-badge kt-badge--' + account_type[row.account_type].state + ' kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-' + account_type[row.account_type].state + '">' +
								account_type[row.account_type].title + '</span>';
							},
				},{
					field: 'status',
					title: 'Status',
					autoHide: false,
					// callback function support for column rendering
					template: function(row) {
						var status = {
							1: {'title': 'Active', 'state': 'success'},
							4: {'title': 'Inactive', 'state': 'primary'},
							2: {'title': 'Blocked', 'state': 'danger'},
							3: {'title': 'Reported', 'state': 'warning'},

						};
						return '<span class="kt-badge kt-badge--' + status[row.status].state + ' kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-' + status[row.status].state + '">' +
								status[row.status].title + '</span>';
					},
				}, {
					field: 'Actions',
					title: 'Actions',
					sortable: false,
					width: 110,
					autoHide: false,
					overflow: 'visible',
					template: function(row) {
						 // console.log(row.id);
						//getPosts(row.id);
						if(row.status== 2){
							var title ="unblock";
							var action="unblockUser";
							var icon ="fa-user-alt-slash"
						}
						else{
							var title ="block";
							var action="blockUser";
							var icon ="fa-user";

						}
						return '\
						<a href="javascript:;"  id="'+row.id+'" class="btn btn-sm btn-clean btn-icon btn-icon-md getData" data-toggle="modal" data-target="#kt_modal_4_4" title="Posts">\
							<i class="fa fa-newspaper"></i>\
						</a>\
						<a href="javascript:;" id="'+row.id+'" class="btn btn-sm btn-clean btn-icon btn-icon-md '+action+'" title="'+title+'">\
							<i class="fa '+icon+'"></i>\
						</a>\
						<a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete">\
							<i class="la la-trash"></i>\
						</a>\
					';
					},
				}],

		});

    $('#kt_form_status_app_users').on('change', function() {
      datatable.search($(this).val().toLowerCase(), 'status');
    });

    $('#kt_form_status,#kt_form_type').selectpicker();

	};
	
	$(document).on('click', 'a.getData', function(){
		getPosts(this.id);
	});
	$(document).on('click', 'a.blockUser', function(){
		blockUsers(this.id);
	});
	$(document).on('click', 'a.unblockUser', function(){
		unblockUsers(this.id);
	});
	$(document).on('click', 'a.blockPost', function(){
		blockPost(this.id);
	});
	$(document).on('click', 'a.unblockPost', function(){
		unblockPost(this.id);
	});
		var unblockUsers= function(id){
			// alert(id);
		var path = window.location.origin;
			$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});
			swal.fire({
		        title: 'Are you sure want to unblock?',
		        text: "",
		        type: 'warning',
		        showCancelButton: true,
		        confirmButtonText: 'Yes, unBlock the user!',
		        cancelButtonText: 'No, cancel!',
		        reverseButtons: true
		    }).then(function(result){

		        if (result.value) {
		        	var request = $.post(path+'/dreamchat/public/unblockuser',{id:id});
					request.done(function(response) {
					const obj = JSON.parse(response);
					if(obj=='1'){
						// $('#kt_modal_1_5').modal('hide');
						swal.fire(

		                'Unblocked!',
		                'User has been unblocked.',
		                'success'

		            )
					}
					location.reload();
					});
		        } else if (result.dismiss === 'cancel') {
		            swal.fire(
		                'Cancelled',
		                'Action has been cancelled :)',
		                'error'
		            )
		        }
		    });
			
			
		}
		var blockUsers= function(id){
			// alert(id);
		var path = window.location.origin;
			$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});
			swal.fire({
		        title: 'Are you sure want to block the user?',
		        text: "",
		        type: 'warning',
		        showCancelButton: true,
		        confirmButtonText: 'Yes, Block the user!',
		        cancelButtonText: 'No, cancel!',
		        reverseButtons: true
		    }).then(function(result){

		        if (result.value) {
		        	var request = $.post(path+'/dreamchat/public/blockuser',{id:id});
					request.done(function(response) {
					const obj = JSON.parse(response);
					if(obj=='1'){
						// $('#kt_modal_1_5').modal('hide');
						swal.fire(

		                'Blocked!',
		                'User has been blocked',
		                'success'

		            )
					}
					location.reload();
					});
		        } else if (result.dismiss === 'cancel') {
		            swal.fire(
		                'Cancelled',
		                'Action has been cancelled :)',
		                'error'
		            )
		        }
		    });
			
			
		}

	var getPosts =function(id){
		// console.log(id);
		// $('.menu').html('Loading please wait ...');
		$("#shop").empty();
		var path = window.location.origin;
		var request = $.get(path+'/dreamchat/public/posts/'+id);
		request.done(function(response) {
		const obj = JSON.parse(response);
		  obj.forEach(function(obj) {
		  	if(obj.status== 2){
				var title ="unblock";
				var action="unblockPost";
				var icon ="fa-eye-slash"
			}
			else{
				var title ="block";
				var action="blockPost";
				var icon ="fa-wallet";

			}
		  	 $("#shop").append(
		  	 	'<div class="kt-portlet__body">\
					<div class="kt-notification kt-notification--fit">\
						<a href="#" class="kt-notification__item">\
							<div class="kt-notification__item-icon">\
								<i class="flaticon2-line-chart kt-font-success"></i>\
							</div>\
							<div class="kt-notification__item-details">\
								<div class="kt-notification__item-title">\
									'+obj.title+'\
								</div>\
								<div class="kt-notification__item-body">\
									'+obj.description+'\
								</a>\
								</div>\
								<div class="kt-notification__item-time">\
									2 hrs ago\
									<button type="button" style="float: right;" class="btn btn-secondary view_modal" id="'+obj.id+'">View</button>\
								</div>\
							</div>\
						</a>\
					</div>\
				</div>'
				);
		  	 console.log(obj);
		  	});
		 });
	}
	var unblockPost= function(id){
			// alert(id);
		var path = window.location.origin;
			$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});
			swal.fire({
		        title: 'Are you sure want to unblock?',
		        text: "",
		        type: 'warning',
		        showCancelButton: true,
		        confirmButtonText: 'Yes, unBlock the Post!',
		        cancelButtonText: 'No, cancel!',
		        reverseButtons: true
		    }).then(function(result){

		        if (result.value) {
		        	var request = $.post(path+'/dreamchat/public/unblockpost',{id:id});
					request.done(function(response) {
					const obj = JSON.parse(response);
					if(obj=='1'){
						// $('#kt_modal_1_5').modal('hide');
						swal.fire(

		                'Unblocked!',
		                'Post has been unblocked.',
		                'success'

		            )
					}
					location.reload();
					});
		        } else if (result.dismiss === 'cancel') {
		            swal.fire(
		                'Cancelled',
		                'Action has been cancelled :)',
		                'error'
		            )
		        }
		    });
			
			
		}
		var blockPost= function(id){
			// alert(id);
		var path = window.location.origin;
			$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});
			swal.fire({
		        title: 'Are you sure want to block the Post?',
		        text: "",
		        type: 'warning',
		        showCancelButton: true,
		        confirmButtonText: 'Yes, Block the Post!',
		        cancelButtonText: 'No, cancel!',
		        reverseButtons: true
		    }).then(function(result){

		        if (result.value) {
		        	var request = $.post(path+'/dreamchat/public/blockpost',{id:id});
					request.done(function(response) {
					const obj = JSON.parse(response);
					if(obj=='1'){
						// $('#kt_modal_1_5').modal('hide');
						swal.fire(

		                'Blocked!',
		                'User has been blocked',
		                'success'

		            )
					}
					location.reload();
					});
		        } else if (result.dismiss === 'cancel') {
		            swal.fire(
		                'Cancelled',
		                'Action has been cancelled :)',
		                'error'
		            )
		        }
		    });
			
			
		}
	return {
		// public functions
		init: function () {
			demo1();
		}
	};
}();

jQuery(document).ready(function () {
	KTDatatableJsonRemoteDemo_admin.init();
});

$(document).on('click', '.view_modal', function() {
	var id = this.id;
	$("#viewPostData").empty();
	$.ajax({
		headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
		type: 'POST',
		url: 'viewPost',
		data: {id: id},
		success: function(response){
			console.log(response);
			const obj = JSON.parse(response);
			var path = window.location.origin;
			if(obj.status== 2){
				var title ="unblock";
				var action="unblockPost";
				var icon ="fa-eye-slash"
			}
			else{
				var title ="block";
				var action="blockPost";
				var icon ="fa-wallet";

			}

			$('#kt_modal_4_41').modal('show');
			$("#viewPostData").append(
		  	 	'<div class="kt-portlet__body">\
					<div class="kt-notification kt-notification--fit">\
						<a href="#" class="kt-notification__item">\
							<div class="kt-notification__item-details">\
								<div class="kt-notification__item-title">\
									'+obj.title+'\
								</div>\
								<div class="kt-notification__item-body"  style="width: 350px;float: left;">\
									<img src="'+path+'/dreamchat/public/images/'+obj.image+'" width="150">\
									<a href="javascript:;" style="float: right;" id="'+obj.id+'" class="btn btn-sm btn-clean btn-icon btn-icon-md '+action+'" title="'+title+'">\
									<i class="fa '+icon+'"></i>\
								</a>\
								</div>\
							</div>\
						</a>\
					</div>\
				</div>'
			);
		}
	});

});

