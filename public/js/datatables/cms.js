"use strict";
// Class definition

var KTDatatableJsonRemoteDemo_cms = function () {
	// Private functions
	var path = window.location.origin;

	// basic demo
	var demo1 = function () {
		
		var datatable = $('.kt-datatable-cms').KTDatatable({
			// datasource definition
			data: {
				type: 'remote',
				source: 'cms',
				pageSize: 10,
			},

			// layout definition
			layout: {
				scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
				footer: false // display/hide footer
			},

			// column sorting
			sortable: true,

			pagination: true,

			search: {
				input: $('#generalSearch')
			},

			// columns definition
			columns: [
			{
					field: 'RecordID',
					title: '#',
					sortable: false,
					width: 20,
					type: 'number',
					selector: {class: 'kt-checkbox--solid'},
					textAlign: 'center',
				},{
					field: 'title',
					title: 'Title',
				},{
					field: 'content',
					title: 'Content',
				},{
					field: 'status',
					title: 'Status',
					autoHide: false,
					// callback function support for column rendering
					template: function(row) {
						var status = {
							1: {'title': 'Active', 'state': 'success'},
							2: {'title': 'Expired', 'state': 'danger'},

						};
						return '<span class="kt-badge kt-badge--' + status[row.status].state + ' kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-' + status[row.status].state + '">' +
								status[row.status].title + '</span>';
					},
				}, {
					field: 'Actions',
					title: 'Actions',
					sortable: false,
					width: 110,
					autoHide: false,
					overflow: 'visible',
					template: function(row) {
						 // console.log(row.id);
						//getPosts(row.id);
						if(row.status== 2){
							var title ="unblock";
							var action="unblockCms";
							var icon ="fa-comment-slash"
						}
						else{
							var title ="block";
							var action="blockCms";
							var icon ="fa-money-check-alt";

						}
						return '\
						<a href="javascript:;"  id="'+row.id+'" class="btn btn-sm btn-clean btn-icon btn-icon-md getCms" data-toggle="modal" data-target="#kt_modal_4_4" title="Posts">\
							<i class="fa flaticon-edit-1"></i>\
						</a>\
						<a href="javascript:;" id="'+row.id+'" class="btn btn-sm btn-clean btn-icon btn-icon-md '+action+'" title="'+title+'">\
							<i class="fa '+icon+'"></i>\
						</a>\
						<a href="javascript:;" id="'+row.id+'" class="btn btn-sm btn-clean btn-icon btn-icon-md deletePlan" title="Delete">\
							<i class="la la-trash"></i>\
						</a>\
					';
					},
				}],

		});

    $('#kt_form_status_app_credits').on('change', function() {
      datatable.search($(this).val().toLowerCase(), 'status');
    });

    $('#kt_form_status,#kt_form_type').selectpicker();

	};
	
	$(document).on('click', 'a.getCms', function(){
		getCms(this.id);
	});
	// $(document).on('click', 'a.blockUser', function(){
	// 	blockUsers(this.id);
	// });
	// $(document).on('click', 'a.unblockUser', function(){
	// 	unblockUsers(this.id);
	// });
	$(document).on('click', 'a.blockCms', function(){
		blockCms(this.id);
	});
	$(document).on('click', 'a.unblockCms', function(){
		unblockCms(this.id);
	});
	$(document).on('click', 'a.deleteCms', function(){
		deleteCms(this.id);
	});
		
	var blockCms= function(id){
			// alert(id);
		var path = window.location.origin;
			$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});
			swal.fire({
		        title: 'Are you sure want to block the Plan?',
		        text: "",
		        type: 'warning',
		        showCancelButton: true,
		        confirmButtonText: 'Yes, Block the Page!',
		        cancelButtonText: 'No, cancel!',
		        reverseButtons: true
		    }).then(function(result){

		        if (result.value) {
		        	var request = $.post(path+'/dreamchat/public/blockCms',{id:id});
					request.done(function(response) {
					const obj = JSON.parse(response);
					if(obj=='1'){
						// $('#kt_modal_1_5').modal('hide');
						swal.fire(

		                'Blocked!',
		                'Page has been blocked',
		                'success'

		            )
					}
					location.reload();
					});
		        } else if (result.dismiss === 'cancel') {
		            swal.fire(
		                'Cancelled',
		                'Action has been cancelled :)',
		                'error'
		            )
		        }
		    });
			
			
		}
	var getCms =function(id){
		// console.log(id);
		// $('.menu').html('Loading please wait ...');

		 $("#title").empty();
		  $(".ck-editor__editable").empty();
		var path = window.location.origin;
		var request = $.get(path+'/dreamchat/public/editCms/'+id);
		request.done(function(response) {
		const obj = JSON.parse(response);
		console.log(obj);
		if(obj.status== 2){
			var title ="unblock";
			var action="unblockCms";
			var icon ="fa-eye-slash"
		}
		else{
			var title ="block";
			var action="blockCms";
			var icon ="fa-wallet";

		}
	  	$("#title").append(
	  	 	'<input type="hidden" id ="edit_cms_id" class="form-control" name="cms_id" value="'+obj.id+'" required autocomplete="" autofocus>\
            <input type="text" id ="edit_title" class="form-control" name="title" value="'+obj.title+'" required autocomplete="" autofocus>\
            <span class="form-text text-muted"></span>\
            '
			);
	  	// $(".ck-content").html(obj.content);	
	  	$(".ck-editor__editable").append(obj.content);
	  	 console.log(obj);
	  	});
	}
	$(document).on('click','#update_cms', function(){
		$( "#edit_cms" ).validate({
            // define validation rules
            rules: {
                //= Client Information(step 3)
                // Billing Information

                edit_content: {
                    required: true,
                     digits: true
                },
                edit_title: {
                    required: true
                },
            },

            //display error alert on form submit
            invalidHandler: function(event, validator) {
                swal.fire({
                    "title": "",
                    "text": "There are some errors in your submission. Please correct them.",
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary",
                    "onClose": function(e) {
                        console.log('on close event fired!');
                    }
                });

                event.preventDefault();
            },

            submitHandler: function (form) {
                // form[0].submit(); // submit the form
               var id = $('#edit_cms_id').val();
				var status = $('#status').val();
				var content = $('#edit_content').val();
				var title = $('#edit_title').val();
				
				$.ajax({
					headers: {
					        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					    },
					type: 'POST',
					url: 'editCms',
					data: {
						id: id,
						status: status,
						title: title,
						content: content,
					},
					success: function(response){
						if (response == '1') {
							location.reload();
							toastr.success("Cms updated successfully!");
						}else{
							toastr.error("Error while updating. Try again");
						}
						console.log(response);
					}
				});

                // swal.fire({
                //     "title": "",
                //     "text": "Form validation passed. All good!",
                //     "type": "success",
                //     "confirmButtonClass": "btn btn-secondary"
                // });

                // return false;
            }
        });		
	});
	var deleteCms = function(id){
			// alert(id);
		var path = window.location.origin;
			$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});
			swal.fire({
		        title: 'Are you sure want to delete the Page?',
		        text: "",
		        type: 'warning',
		        showCancelButton: true,
		        confirmButtonText: 'Yes, Delete the Page!',
		        cancelButtonText: 'No, cancel!',
		        reverseButtons: true
		    }).then(function(result){

		        if (result.value) {
		        	var request = $.post(path+'/dreamchat/public/deleteCms',{id:id});
					request.done(function(response) {
					const obj = JSON.parse(response);
					if(obj=='1'){
						// $('#kt_modal_1_5').modal('hide');
						swal.fire(

		                'Deleted!',
		                'Page has been deleted',
		                'success'

		            )
					}
					location.reload();
					});
		        } else if (result.dismiss === 'cancel') {
		            swal.fire(
		                'Cancelled',
		                'Action has been cancelled :)',
		                'error'
		            )
		        }
		    });
			
			
		}
	var unblockCms= function(id){
			// alert(id);
		var path = window.location.origin;
			$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});
			swal.fire({
		        title: 'Are you sure want to unblock?',
		        text: "",
		        type: 'warning',
		        showCancelButton: true,
		        confirmButtonText: 'Yes, unBlock the Plan!',
		        cancelButtonText: 'No, cancel!',
		        reverseButtons: true
		    }).then(function(result){

		        if (result.value) {
		        	var request = $.post(path+'/dreamchat/public/unblockCms',{id:id});
					request.done(function(response) {
					const obj = JSON.parse(response);
					if(obj=='1'){
						// $('#kt_modal_1_5').modal('hide');
						swal.fire(

		                'Unblocked!',
		                'Plan has been unblocked.',
		                'success'

		            )
					}
					location.reload();
					});
		        } else if (result.dismiss === 'cancel') {
		            swal.fire(
		                'Cancelled',
		                'Action has been cancelled :)',
		                'error'
		            )
		        }
		    });
			
			
		}	
 var ckedits = function () {
        ClassicEditor
			.create( document.querySelector( '#edit_content' ) )
			.then( editors => {
				console.log( editors );
			} )
			.catch( errors => {
				console.error( errors );
			} );

		// ClassicEditor
		// 	.create( document.querySelector( '#edit_content' ) )
		// 	.then( editor => {
		// 		console.log( editor );
		// 	} )
		// 	.catch( error => {
		// 		console.error( error );
		// 	} );
    }

		var blockPlan= function(id){
			// alert(id);
		var path = window.location.origin;
			$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});
			swal.fire({
		        title: 'Are you sure want to block the Plan?',
		        text: "",
		        type: 'warning',
		        showCancelButton: true,
		        confirmButtonText: 'Yes, Block the Plan!',
		        cancelButtonText: 'No, cancel!',
		        reverseButtons: true
		    }).then(function(result){

		        if (result.value) {
		        	var request = $.post(path+'/dreamchat/public/blockCms',{id:id});
					request.done(function(response) {
					const obj = JSON.parse(response);
					if(obj=='1'){
						// $('#kt_modal_1_5').modal('hide');
						swal.fire(

		                'Blocked!',
		                'Plan has been blocked',
		                'success'

		            )
					}
					location.reload();
					});
		        } else if (result.dismiss === 'cancel') {
		            swal.fire(
		                'Cancelled',
		                'Action has been cancelled :)',
		                'error'
		            )
		        }
		    });
			
			
		}
	return {
		// public functions
		init: function () {
			ckedits();
			demo1();
		}
	};
}();


jQuery(document).ready(function () {
	KTDatatableJsonRemoteDemo_cms.init();
});

