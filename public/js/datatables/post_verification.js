"use strict";
// Class definition

var KTDatatableJsonRemoteDemo_verify_post = function () {
	// Private functions
	var path = window.location.origin;

	// basic demo
	var demo1 = function () {
		
		var datatable = $('.kt-datatable-post_verification').KTDatatable({
			// datasource definition
			data: {
				type: 'remote',
				source: 'getAllReportedPosts',
				pageSize: 10,
			},

			// layout definition
			layout: {
				scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
				footer: false // display/hide footer
			},

			// column sorting
			sortable: true,

			pagination: true,

			search: {
				input: $('#generalSearch')
			},

			// columns definition
			columns: [
			{
					field: 'RecordID',
					title: '#',
					sortable: false,
					width: 20,
					type: 'number',
					selector: {class: 'kt-checkbox--solid'},
					textAlign: 'center',
				},{
					field: 'title',
					title: 'Title',
					template: function(row) {
						return row.posts['title'];
					}
				},{
					field: 'description',
					title: 'Description',
					template: function(row) {
						return row.posts['description'];
					}
				},{
					field: 'image',
					title: 'Post',
					template: function(row) {
						return '<img src="../../public/images/'+row.posts['image']+'" style="width: 100px;height:100px;">';
					}
				}, {
					field: 'Actions',
					title: 'Actions',
					sortable: false,
					width: 110,
					autoHide: false,
					overflow: 'visible',
					template: function(row) {
						 // console.log(row.id);
						//getPosts(row.id);
						if(row.status== 2){
							var title ="unblock";
							var action="unblockCms";
							var icon ="flaticon-cancel"
						}
						else{
							var title ="block";
							var action="blockPosts";
							var icon ="flaticon-cancel";

						}
						return '\
						<a href="javascript:;" id="'+row.posts['id']+'" class="btn btn-sm btn-clean btn-icon btn-icon-md '+action+'" title="'+title+'">\
							<i class="fa '+icon+'"></i>\
						</a>\
						<a href="javascript:;" id="'+row.posts['id']+'" class="btn btn-sm btn-clean btn-icon btn-icon-md reactivePost" title="Reactivate">\
							<i class="la flaticon2-check-mark"></i>\
						</a>\
					';
					},
				}],

		});

    $('#kt_form_status_app_credits').on('change', function() {
      datatable.search($(this).val().toLowerCase(), 'status');
    });

    $('#kt_form_status,#kt_form_type').selectpicker();

	};
	
	
	// $(document).on('click', 'a.blockUser', function(){
	// 	blockUsers(this.id);
	// });
	// $(document).on('click', 'a.unblockUser', function(){
	// 	unblockUsers(this.id);
	// });
	$(document).on('click', 'a.blockPosts', function(){
		blockPosts(this.id);
	});
	$(document).on('click', 'a.reactivePost', function(){
		reactivePost(this.id);
	});

		
	var blockPosts= function(id){
			// alert(id);
		var path = window.location.origin;
			$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});
			swal.fire({
		        title: 'Are you sure want to block the post?',
		        text: "",
		        type: 'warning',
		        showCancelButton: true,
		        confirmButtonText: 'Yes, Block the Post!',
		        cancelButtonText: 'No, cancel!',
		        reverseButtons: true
		    }).then(function(result){

		        if (result.value) {
		        	var request = $.post(path+'/dreamchat/public/admin/blockpost',{id:id});
					request.done(function(response) {
					const obj = JSON.parse(response);
					if(obj=='1'){
						// $('#kt_modal_1_5').modal('hide');
						swal.fire(

		                'Blocked!',
		                'Post has been blocked',
		                'success'

		            )
					}
					location.reload();
					});
		        } else if (result.dismiss === 'cancel') {
		            swal.fire(
		                'Cancelled',
		                'Action has been cancelled :)',
		                'error'
		            )
		        }
		    });
			
			
		}

	var reactivePost= function(id){
			// alert(id);
		var path = window.location.origin;
			$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});
			swal.fire({
		        title: 'Are you sure want to Reactivate?',
		        text: "",
		        type: 'warning',
		        showCancelButton: true,
		        confirmButtonText: 'Yes, Reactivate this post!',
		        cancelButtonText: 'No, cancel!',
		        reverseButtons: true
		    }).then(function(result){

		        if (result.value) {
		        	var request = $.post(path+'/dreamchat/public/admin/reactivatePost',{id:id});
					request.done(function(response) {
					const obj = JSON.parse(response);
					if(obj=='1'){
						// $('#kt_modal_1_5').modal('hide');
						swal.fire(

		                'Reactivated!',
		                'Plan has been unblocked.',
		                'success'

		            )
					}
					location.reload();
					});
		        } else if (result.dismiss === 'cancel') {
		            swal.fire(
		                'Cancelled',
		                'Action has been cancelled :)',
		                'error'
		            )
		        }
		    });
			
			
		}	

	return {
		// public functions
		init: function () {
			demo1();
		}
	};
}();


jQuery(document).ready(function () {
	KTDatatableJsonRemoteDemo_verify_post.init();
});

