"use strict";
// Class definition

var KTDatatableJsonRemoteDemo_user = function () {
	// Private functions

	// basic demo
	var demo2 = function () {

		var datatable = $('.kt-datatable-admin').KTDatatable({
			// datasource definition
			data: {
				type: 'remote',
				source: 'admins',
				pageSize: 10,
			},

			// layout definition
			layout: {
				scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
				footer: false // display/hide footer
			},

			// column sorting
			sortable: true,

			pagination: true,

			search: {
				input: $('#generalSearch')
			},

			// columns definition
			columns: [
			{
					field: 'RecordID',
					title: '#',
					sortable: false,
					width: 20,
					type: 'number',
					selector: {class: 'kt-checkbox--solid'},
					textAlign: 'center',
				},	 {
					field: 'full_name',
					title: 'Full Name',
				}, {
					field: 'email',
					title: 'Email',
				},{
					field: 'status',
					title: 'Status',
					autoHide: false,
					// callback function support for column rendering
					template: function(row) {
						var status = {
							1: {'title': 'Active', 'state': 'success'},
							4: {'title': 'Inactive', 'state': 'primary'},
							2: {'title': 'Blocked', 'state': 'danger'},
							3: {'title': 'Reported', 'state': 'warning'},

						};
						return '<span class="kt-badge kt-badge--' + status[row.status].state + ' kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-' + status[row.status].state + '">' +
								status[row.status].title + '</span>';
					},
				}, {
					field: 'Actions',
					title: 'Actions',
					sortable: false,
					width: 110,
					autoHide: false,
					overflow: 'visible',
					template: function(row) {
						if(row.status== 2){
							var title ="unblock";
							var action="unblockUser";
							var icon ="fa-user-alt-slash"
						}
						else{
							var title ="block";
							var action="blockUser";
							var icon ="fa-user";

						}
						return '\
						<a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit details">\
							<i class="la la-edit"></i>\
						</a>\
						<a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete">\
							<i class="la la-trash"></i>\
						</a>\
						<a href="javascript:;" id="'+row.id+'" class="btn btn-sm btn-clean btn-icon btn-icon-md '+action+'" title="'+title+'">\
							<i class="fa '+icon+'"></i>\
						</a>\
					';
					},
				}],

		});

    $('#kt_form_status_users').on('change', function() {
      datatable.search($(this).val().toLowerCase(), 'status');
    });

	$('#mobile_number').on('change', function() {
		var mobile_number=$('#mobile_number').val();
		console.log(mobile_number.length);
		if(mobile_number.length == '10'){
			$('#mobile_span').hide();
		}
		else{
			$('#mobile_span').show();
			$('#mobile_error').append('Invalid Mobile number');
		}
		console.log(mobile_number);
	});

    $('#kt_form_status,#kt_form_type').selectpicker();

	};
	$(document).on('click', 'a.blockUser', function(){
		blockUsers(this.id);
	});
	$(document).on('click', 'a.unblockUser', function(){
		unblockUsers(this.id);
	});
	var unblockUsers= function(id){
		// alert(id);
	var path = window.location.origin;
		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});
		swal.fire({
	        title: 'Are you sure want to unblock?',
	        text: "",
	        type: 'warning',
	        showCancelButton: true,
	        confirmButtonText: 'Yes, unBlock the user!',
	        cancelButtonText: 'No, cancel!',
	        reverseButtons: true
	    }).then(function(result){

	        if (result.value) {
	        	var request = $.post(path+'/dreamchat/public/unblockuser',{id:id});
				request.done(function(response) {
				const obj = JSON.parse(response);
				if(obj=='1'){
					// $('#kt_modal_1_5').modal('hide');
					swal.fire(

	                'Unblocked!',
	                'User has been unblocked.',
	                'success'

	            )
				}
				location.reload();
				});
	        } else if (result.dismiss === 'cancel') {
	            swal.fire(
	                'Cancelled',
	                'Action has been cancelled :)',
	                'error'
	            )
	        }
	    });
		
		
	}
	var blockUsers= function(id){
		// alert(id);
	var path = window.location.origin;
		$.ajaxSetup({
		    headers: {
		        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		    }
		});
		swal.fire({
	        title: 'Are you sure want to block the user?',
	        text: "",
	        type: 'warning',
	        showCancelButton: true,
	        confirmButtonText: 'Yes, Block the user!',
	        cancelButtonText: 'No, cancel!',
	        reverseButtons: true
	    }).then(function(result){

	        if (result.value) {
	        	var request = $.post(path+'/dreamchat/public/blockuser',{id:id});
				request.done(function(response) {
				const obj = JSON.parse(response);
				if(obj=='1'){
					// $('#kt_modal_1_5').modal('hide');
					swal.fire(

	                'Blocked!',
	                'User has been blocked.',
	                'success'

	            )
				}
				location.reload();
				});
	        } else if (result.dismiss === 'cancel') {
	            swal.fire(
	                'Cancelled',
	                'Action has been cancelled :)',
	                'error'
	            )
	        }
	    });
		
		
	}
	return {
		// public functions
		init: function () {
			demo2();
		}
	};
}();

jQuery(document).ready(function () {
	KTDatatableJsonRemoteDemo_user.init();
});