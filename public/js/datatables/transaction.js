"use strict";
// Class definition

var KTDatatableJsonRemoteDemo_transaction = function () {
	// Private functions
	var path = window.location.origin;

	// basic demo
	var demo1 = function () {


		var datatable = $('.kt-datatable-transaction').KTDatatable({
			// datasource definition
			data: {
				type: 'remote',
				source: 'transactions',
				pageSize: 10,
			},

			// layout definition
			layout: {
				scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
				footer: false // display/hide footer
			},

			// column sorting
			sortable: true,

			pagination: true,

			search: {
				input: $('#generalSearch')
			},

			// columns definition
			columns: [
			{
					field: 'RecordID',
					title: '#',
					sortable: false,
					width: 20,
					type: 'number',
					selector: {class: 'kt-checkbox--solid'},
					textAlign: 'center',
				},{
					field: 'user_id',
					title: 'User ID',
				}, {
					field: 'transaction_id',
					title: 'Transaction ID',
				}, {
					field: 'transaction_id',
					title: 'Transaction ID',
				},
				{
					field: 'transaction_amount',
					title: 'Amount',
				},{
					field: 'transaction_mode',
					title: 'Transaction Mode',
				},{
					field: 'status',
					title: 'Status',
					autoHide: false,
					// callback function support for column rendering
					template: function(row) {
						var status = {
							1: {'title': 'Success', 'state': 'success'},
							2: {'title': 'Failed', 'state': 'danger'},
						};
						return '<span class="kt-badge kt-badge--' + status[row.status].state + ' kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-' + status[row.status].state + '">' +
								status[row.status].title + '</span>';
					},
				}
				],

		});

    $('#kt_form_status_transaction').on('change', function() {
      datatable.search($(this).val().toLowerCase(), 'status');
    });

    $('#kt_form_status,#kt_form_type').selectpicker();

	};
	

	return {
		// public functions
		init: function () {
			demo1();
		}
	};
}();

jQuery(document).ready(function () {
	KTDatatableJsonRemoteDemo_transaction.init();
});

