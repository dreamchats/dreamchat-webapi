"use strict";
// Class definition

var KTDatatableJsonRemoteDemo_credits = function () {
	// Private functions
	var path = window.location.origin;

	// basic demo
	var credits = function () {


		var datatable = $('.kt-datatable-credits').KTDatatable({
			// datasource definition
			data: {
				type: 'remote',
				source: 'http://dev.kaspontech.com/dreamchat/public/credits',
				pageSize: 10,
			},

			// layout definition
			layout: {
				scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
				footer: false // display/hide footer
			},

			// column sorting
			sortable: true,

			pagination: true,

			search: {
				input: $('#generalSearch')
			},

			// columns definition
			columns: [
			{
					field: 'RecordID',
					title: '#',
					sortable: false,
					width: 20,
					type: 'number',
					selector: {class: 'kt-checkbox--solid'},
					textAlign: 'center',
				},{
					field: 'credits_points',
					title: 'Credit Points',
				},{
					field: 'amount',
					title: 'Amunt',
				},{
					field: 'status',
					title: 'Status',
					autoHide: false,
					// callback function support for column rendering
					template: function(row) {
						var status = {
							1: {'title': 'Active', 'state': 'success'},
							2: {'title': 'Inactive', 'state': 'danger'},

						};
						return '<span class="kt-badge kt-badge--' + status[row.status].state + ' kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-' + status[row.status].state + '">' +
								status[row.status].title + '</span>';
					},
				}, {
					field: 'Actions',
					title: 'Actions',
					sortable: false,
					width: 110,
					autoHide: false,
					overflow: 'visible',
					template: function(row) {
						 // console.log(row.id);
						//getPosts(row.id);
						if(row.status== 2){
							var title ="unblock";
							var action="unblockPlan";
							var icon ="fa-comment-slash"
						}
						else{
							var title ="block";
							var action="blockPlan";
							var icon ="fa-money-check-alt";

						}
						return '\
						<a href="javascript:;"  id="'+row.id+'" class="btn btn-sm btn-clean btn-icon btn-icon-md getData" data-toggle="modal" data-target="#kt_modal_4_4" title="Posts">\
							<i class="fa flaticon-edit-1"></i>\
						</a>\
						<a href="javascript:;" id="'+row.id+'" class="btn btn-sm btn-clean btn-icon btn-icon-md '+action+'" title="'+title+'">\
							<i class="fa '+icon+'"></i>\
						</a>\
						<a href="javascript:;" id="'+row.id+'" class="btn btn-sm btn-clean btn-icon btn-icon-md deletePlan" title="Delete">\
							<i class="la la-trash"></i>\
						</a>\
					';
					},
				}],

		});

    $('#kt_form_status_app_credits').on('change', function() {
      datatable.search($(this).val().toLowerCase(), 'status');
    });

    $('#kt_form_status,#kt_form_type').selectpicker();

	};
	
	$(document).on('click', 'a.getData', function(){
		getPosts(this.id);
	});
	$(document).on('click', 'a.blockUser', function(){
		blockUsers(this.id);
	});
	$(document).on('click', 'a.unblockUser', function(){
		unblockUsers(this.id);
	});
	$(document).on('click', 'a.blockPlan', function(){
		blockPlan(this.id);
	});
	$(document).on('click', 'a.unblockPlan', function(){
		unblockPlan(this.id);
	});
	$(document).on('click', 'a.deletePlan', function(){
		deletePlan(this.id);
	});
		

	var getPosts =function(id){
		// console.log(id);
		// $('.menu').html('Loading please wait ...');
		$("#shop").empty();
		var path = window.location.origin;
		var request = $.get(path+'/dreamchat/public/editPlan/'+id);
		request.done(function(response) {
		const obj = JSON.parse(response);
		if(obj.status== 2){
			var title ="unblock";
			var action="unblockPlan";
			var icon ="fa-eye-slash"
		}
		else{
			var title ="block";
			var action="blockPlan";
			var icon ="fa-wallet";

		}
	  	$("#credits").append(
	  	 	'<div class="kt-portlets__body">\
				<div class="kt-notification kt-notification--fit">\
					<div class="form-group row">\
                        <label for="name" class="col-md-4 col-form-label text-md-right">Credit Points</label>\
                        <div class="col-md-6">\
                            <input type="hidden" id ="edit_credit_id" class="form-control" name="credit_id" value="'+obj.id+'"  autocomplete="" autofocus>\
                            <input type="text" id ="edit_credit_points" class="form-control" name="credit_points" value="'+obj.credits_points+'"  autocomplete="" autofocus>\
                        </div>\
					</div>\
					<div class="form-group row">\
                        <label for="name" class="col-md-4 col-form-label text-md-right">Amount</label>\
                        <div class="col-md-6">\
                            <input type="text" id="edit_amount" class="form-control" name="amount" value="'+obj.amount+'"  autocomplete="" autofocus>\
                        </div>\
					</div>\
					<div class="form-group row">\
                        <label for="name" class="col-md-4 col-form-label text-md-right">Status</label>\
                        <div class="col-md-6">\
                           <select id="status" class="form-control" name="status">\
                           		<option value="1">Active</option>\
                           		<option value="2">Inactive</option>\
                           </select>\
                        </div>\
                    </div>\
                    <div class="form-group row mb-0">\
                        <div class="col-md-6 offset-md-4">\
                        	<button type="reset" class="btn btn-secondary" ">Reset</button>\
                            <button type="submit" class="btn btn-primary" id="update_credit">\
                                Update\
                            </button>\
                        </div>\
                    </div>\
				</div>\
			</div>'
			);
	  	 console.log(obj);
	  	});
	}
	
	$(document).on('click','#update_credit', function(){

		var id = $('#credit_id').val();
		var status = $('#status').val();
		var credits_points = $('#credit_points').val();
		var amount = $('#amount').val();
		
		$.ajax({
			headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
			type: 'POST',
			url: 'editPlan',
			data: {
				id: id,
				status: status,
				credits_points: credits_points,
				amount: amount,
			},
			success: function(response){
				if (response == '1') {
					location.reload();
					toastr.success("Credit updated successfully!");
				}else{
					toastr.error("Error while updating. Try again");
				}
				console.log(response);
			}
		});

	});
	var deletePlan = function(id){
			// alert(id);
		var path = window.location.origin;
			$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});
			swal.fire({
		        title: 'Are you sure want to delete the Plan?',
		        text: "",
		        type: 'warning',
		        showCancelButton: true,
		        confirmButtonText: 'Yes, Delete the Plan!',
		        cancelButtonText: 'No, cancel!',
		        reverseButtons: true
		    }).then(function(result){

		        if (result.value) {
		        	var request = $.post(path+'/dreamchat/public/deletePlan',{id:id});
					request.done(function(response) {
					const obj = JSON.parse(response);
					if(obj=='1'){
						// $('#kt_modal_1_5').modal('hide');
						swal.fire(

		                'Deleted!',
		                'Credit plan has been deleted',
		                'success'

		            )
					}
					location.reload();
					});
		        } else if (result.dismiss === 'cancel') {
		            swal.fire(
		                'Cancelled',
		                'Action has been cancelled :)',
		                'error'
		            )
		        }
		    });
			
			
		}
	var unblockPlan= function(id){
			// alert(id);
		var path = window.location.origin;
			$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});
			swal.fire({
		        title: 'Are you sure want to unblock?',
		        text: "",
		        type: 'warning',
		        showCancelButton: true,
		        confirmButtonText: 'Yes, unBlock the Plan!',
		        cancelButtonText: 'No, cancel!',
		        reverseButtons: true
		    }).then(function(result){

		        if (result.value) {
		        	var request = $.post(path+'/dreamchat/public/unblockPlan',{id:id});
					request.done(function(response) {
					const obj = JSON.parse(response);
					if(obj=='1'){
						// $('#kt_modal_1_5').modal('hide');
						swal.fire(

		                'Unblocked!',
		                'Plan has been unblocked.',
		                'success'

		            )
					}
					location.reload();
					});
		        } else if (result.dismiss === 'cancel') {
		            swal.fire(
		                'Cancelled',
		                'Action has been cancelled :)',
		                'error'
		            )
		        }
		    });
			
			
		}
		var blockPlan= function(id){
			// alert(id);
		var path = window.location.origin;
			$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});
			swal.fire({
		        title: 'Are you sure want to block the Plan?',
		        text: "",
		        type: 'warning',
		        showCancelButton: true,
		        confirmButtonText: 'Yes, Block the Plan!',
		        cancelButtonText: 'No, cancel!',
		        reverseButtons: true
		    }).then(function(result){

		        if (result.value) {
		        	var request = $.post(path+'/dreamchat/public/blockPlan',{id:id});
					request.done(function(response) {
					const obj = JSON.parse(response);
					if(obj=='1'){
						// $('#kt_modal_1_5').modal('hide');
						swal.fire(

		                'Blocked!',
		                'Plan has been blocked',
		                'success'

		            )
					}
					location.reload();
					});
		        } else if (result.dismiss === 'cancel') {
		            swal.fire(
		                'Cancelled',
		                'Action has been cancelled :)',
		                'error'
		            )
		        }
		    });
			
			
		}
	return {
		// public functions
		init: function () {
			credits();
		}
	};
}();

jQuery(document).ready(function () {
	KTDatatableJsonRemoteDemo_credits.init();
});

