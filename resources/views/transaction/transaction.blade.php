@extends('layouts.admin')

@section('content')

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">
				<button class="kt-subheader__mobile-toggle kt-subheader__mobile-toggle--left" id="kt_subheader_mobile_toggle"><span></span></button>
				Admin</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="" class="kt-subheader__breadcrumbs-link">
					Transactions</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
			</div>
		</div>
	
	</div>
</div>
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
	<div class="kt-portlet__body">
	<!--begin: Search Form -->
	<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
		<div class="row align-items-center">
			<div class="col-xl-8 order-2 order-xl-1">
				<div class="row align-items-center">
					<div class="col-md-3 kt-margin-b-20-tablet-and-mobile">
						<div class="kt-form__group kt-form__group--inline">
							<div class="kt-form__label">
								<label>Status:</label>
							</div>
							<div class="kt-form__control">
								<select class="form-control" id="kt_form_status_transaction">
									<option value="">All</option>
									<option value="1">Success</option>
									<option value="2">Failed</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-3 kt-margin-b-20-tablet-and-mobile">
						<div class="kt-input-icon kt-input-icon--left">
							<input type="text" class="form-control" placeholder="Search..." id="generalSearch">
							<span class="kt-input-icon__icon kt-input-icon__icon--left">
								<span><i class="la la-search"></i></span>
							</span>
						</div>
					</div>
				
				<!-- 	<a href="{{route('newAdmin')}}" class="btn btn-brand btn-elevate btn-icon-sm" data-toggle="modal" data-target="#kt_modal_4_2">
						<i class="la la-plus"></i>
						New Admin
					</a> -->
				</div>
			</div>
			<!-- <div class="col-xl-4 order-1 order-xl-2 kt-align-right">
				<a href="#" class="btn btn-default kt-hidden">
					<i class="la la-cart-plus"></i> New Order
				</a>
				<div class="kt-separator kt-separator--border-dashed kt-separator--space-lg d-xl-none"></div>
			</div> -->
		</div>
	</div>

	<!--end: Search Form -->
</div>
	<div class="kt-portlet__body kt-portlet__body--fit">
		<!--begin: Datatable -->
		<div class="kt-datatable-transaction" id="json_data1"></div>
		<!--end: Datatable -->
	</div>
</div>
	

@endsection