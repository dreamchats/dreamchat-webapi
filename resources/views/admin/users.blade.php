@extends('layouts.admin')

@section('content')
@error('email')
<script type="text/javascript">
$(function() {
    $('#kt_modal_4_2').modal('show');
});
</script>
@enderror
<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">
				<button class="kt-subheader__mobile-toggle kt-subheader__mobile-toggle--left" id="kt_subheader_mobile_toggle"><span></span></button>
				Admin</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="" class="kt-subheader__breadcrumbs-link">
					Users</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
			</div>
		</div>
	
	</div>
</div>
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
	<div class="kt-portlet__body">
	<!--begin: Search Form -->
	<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
		<div class="row align-items-center">
			<div class="col-xl-8 order-2 order-xl-1">
				<div class="row align-items-center">
					<div class="col-md-3 kt-margin-b-20-tablet-and-mobile">
						<div class="kt-form__group kt-form__group--inline">
							<div class="kt-form__label">
								<label>Status:</label>
							</div>
							<div class="kt-form__control">
								<select class="form-control" id="kt_form_status_users">
									<option value="">All</option>
									<option value="4">Inactive</option>
									<option value="1">Active</option>
									<option value="2">Blocked</option>
									<option value="3">Reported</option>
								</select>
							</div>
						</div>
					</div>
                    <div class="col-md-3 kt-margin-b-20-tablet-and-mobile">
                        <div class="kt-input-icon kt-input-icon--left">
                            <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                            <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                <span><i class="la la-search"></i></span>
                            </span>
                        </div>
                    </div>
					<a href="{{route('newAdmin')}}" class="btn btn-brand btn-elevate btn-icon-sm" data-toggle="modal" data-target="#kt_modal_4_2">
						<i class="la la-plus"></i>
						New Admin
					</a>
				</div>
			</div>
			<!-- <div class="col-xl-4 order-1 order-xl-2 kt-align-right">
				<a href="#" class="btn btn-default kt-hidden">
					<i class="la la-cart-plus"></i> New Order
				</a>
				<div class="kt-separator kt-separator--border-dashed kt-separator--space-lg d-xl-none"></div>
			</div> -->
		</div>
	</div>

	<!--end: Search Form -->
</div>
	<div class="kt-portlet__body kt-portlet__body--fit">
		<!--begin: Datatable -->
		<div class="kt-datatable-admin" id="json_data2"></div>
		<!--end: Datatable -->
	</div>
</div>
	<div class="modal fade" id="kt_modal_4_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-xl" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">New Admin</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
                <div class="card-body">
                    <form method="POST" action="{{ route('newAdmin') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Mobile Number</label>
                            <div class="col-md-6">
                                <input id="mobile_number" type="text" class="form-control" name="mobile_number" value="" required autocomplete="" autofocus>
                                <span  id="mobile_span" style="display: block; width: 100%;margin-top: 0.25rem;font-size: 80%;color: #fd27eb;" role="alert">
                                    <strong id="mobile_error"></strong>
                                </span>
                                <!-- <select id="countries_phone1" class="form-control bfh-countries" data-country="US"></select>
								<br><br>
								<input type="text" class="form-control bfh-phone" data-country="countries_phone1"> -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Gender</label>

                            <div class="col-md-6">
                               <select  class="form-control" name="gender">
                               		<option value="Male">Male</option>
                               		<option value="Female">Female</option>

                               </select>

                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                <p id="EmailMsg" style="display: block; width: 100%;margin-top: 0.25rem;font-size: 80%;color: #fd27eb;"></p>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                         <div class="form-group row">
                            <label for="content" class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>

                            <div class="col-md-6">
                            	<textarea id="address" class="form-control @error('content') is-invalid @enderror" name="address" required autocomplete="address" autofocus>{{ old('address') }}</textarea>
                                
                                @error('content')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        <!-- <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                     <span id="password_check" role="alert" style="display: block; width: 100%;margin-top: 0.25rem;font-size: 80%;color: #fd27eb;" >
                                        <strong id="mismatch"></strong>
                                    </span>

                            </div>
                        </div> -->
                        
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                            	<button type="reset" class="btn btn-secondary" ">Reset</button>
                                <button type="submit" class="btn btn-primary" id="btn_submit">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
				</div>
			</div>
		</div>
	</div>

@endsection