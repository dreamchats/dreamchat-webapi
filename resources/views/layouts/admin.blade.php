
<!DOCTYPE html>

<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4 & Angular 8
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">

	<!-- begin::Head -->
	<head>
		<base href="">
		<meta charset="utf-8" />
		<title>Metronic | Dashboard</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<!--begin::Fonts -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">

		<!--end::Fonts -->

		<!--begin::Page Vendors Styles(used by this page) -->
		<link href="{{asset('/plugins/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet" type="text/css" />

		<!--end::Page Vendors Styles -->

		<!--begin::Global Theme Styles(used by all pages) -->
		<link href="{{asset('/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('/css/layout_styles.css')}}" rel="stylesheet" type="text/css" />

		<!--end::Global Theme Styles -->

		<!--begin::Layout Skins(used by all pages) -->

		<!--end::Layout Skins -->
		<link rel="shortcut icon" href="{{asset('/media/logos/favicon.ico')}}" />


	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--left kt-aside--fixed kt-page--loading">

		<!-- begin::Page loader -->

		<!-- end::Page Loader -->

		<!-- begin:: Page -->

		<!-- begin:: Header Mobile -->
		<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
			<div class="kt-header-mobile__logo">
				<a href="index.html">
					<img alt="Logo" src="{{asset('/media/logos/logo-9-sm.png')}}" />
				</a>
			</div>
			<div class="kt-header-mobile__toolbar">
				<button class="kt-header-mobile__toolbar-toggler kt-header-mobile__toolbar-toggler--left" id="kt_aside_mobile_toggler"><span></span></button>
				<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
				<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more-1"></i></button>
			</div>
		</div>

		<!-- end:: Header Mobile -->
		<div class="kt-grid kt-grid--hor kt-grid--root" id="main">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

					<!-- begin:: Header -->
					<div id="kt_header" class="kt-header  kt-header--fixed headerbg" data-ktheader-minimize="on">
						<div class="kt-container  kt-container--fluid ">

							<!-- begin: Header Menu -->
							<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
							<div class="kt-header-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_header_menu_wrapper">
								<button class="kt-aside-toggler kt-aside-toggler--left" id="kt_aside_toggler" onclick="openNav()"><span></span></button>
								<div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile ">
									<ul class="kt-menu__nav ">
										<li class="kt-menu__item  kt-menu__item--open kt-menu__item--here kt-menu__item--submenu kt-menu__item--rel kt-menu__item--open kt-menu__item--here" data-ktmenu-submenu-toggle="click" aria-haspopup="true"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text">Dashboard</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
											<div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">
												<ul class="kt-menu__subnav">
													<li class="kt-menu__item  kt-menu__item--active " aria-haspopup="true"><a href="index.html" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Default Dashboard</span></a></li>
													<li class="kt-menu__item " aria-haspopup="true"><a href="dashboards/fluid.html" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Fluid Dashboard</span></a></li>
													<li class="kt-menu__item " aria-haspopup="true"><a href="dashboards/aside.html" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Aside Dashboard</span></a></li>
												</ul>
											</div>
										</li>
									</ul>
								</div>
							</div>
							<!-- end: Header Menu -->
							<!-- begin:: Brand -->
							<div class="kt-header__brand   kt-grid__item" id="kt_header_brand">
								<a class="kt-header__brand-logo" href="?page=index">
									<!-- <img alt="Logo" src="{{asset('/media/logos/logo-9.png')}}" /> -->
								</a>
							</div>
							<!-- end:: Brand -->
							
							<!-- begin:: Header Topbar -->
							<div class="kt-header__topbar kt-grid__item">
								<!--begin: Quick actions -->
									<div class="kt-header__topbar-item">
									<div class="kt-header__topbar-wrapper" data-toggle="" data-offset="10px,0px">
										<span class="kt-header__topbar-icon"><a href="{{route('verifyPosts')}}" class="kt-notification__item"><i class="flaticon2-bell-alarm-symbol"></i></a></span>
										<!-- <span class="kt-badge kt-badge--danger"></span> -->
									</div>
									<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">
										
									</div>
								</div>
								<!-- <div class="kt-header__topbar-item">
									<div class="kt-header__topbar-wrapper" data-toggle="" data-offset="10px,0px">
										<span class="kt-header__topbar-icon"><a href="{{route('settings')}}" class="kt-notification__item"><i class="flaticon2-gear"></i></a></span>
									</div>
								</div> -->

								<div class="kt-header__topbar-item kt-header__topbar-item--user">
									<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
										<span class="kt-header__topbar-welcome kt-visible-desktop">Hi,</span>
										<span class="kt-header__topbar-username kt-visible-desktop">{{Auth::user()->full_name}}</span>
										@if(Auth::User()->image)
										<img src="{{asset('/images')}}/{{Auth::USer()->image}}" alt="image">
										@else
										<img src="{{asset('/media/users/300_21.jpg')}}" alt="image">
										@endif

										<!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
										<span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold kt-hidden">S</span>
									</div>
									<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">

										<!--begin: Head -->
										<div class="kt-user-card kt-user-card--skin-light kt-notification-item-padding-x">
											<div class="kt-user-card__avatar">
												<img class="kt-hidden-" alt="Pic" src="{{asset('/media/users/300_25.jpg')}}" />

												<!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
												<span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold kt-hidden">S</span>
											</div>
											<div class="kt-user-card__name">
												{{Auth::user()->full_name}}
											</div>
											<!-- <div class="kt-user-card__badge">
												<span class="btn btn-label-primary btn-sm btn-bold btn-font-md">23 messages</span>
											</div> -->
										</div>

										<!--end: Head -->

										<!--begin: Navigation -->
										<div class="kt-notification">
											<a href="{{route('profile')}}" class="kt-notification__item">
												<div class="kt-notification__item-icon">
													<i class="flaticon2-calendar-3 kt-font-success"></i>
												</div>
												<div class="kt-notification__item-details">
													<div class="kt-notification__item-title kt-font-bold">
														My Profile
													</div>
													<div class="kt-notification__item-time">
														Account settings and more
													</div>
												</div>
											</a>
											
											<div class="kt-notification__custom kt-space-between">
												  <a  href="{{ route('logout') }}" class="btn btn-label btn-label-brand btn-sm btn-bold sign-out"
			                                       onclick="event.preventDefault();
			                                                     document.getElementById('logout-form').submit();">
			                                        {{ __('Sign Out') }}
			                                    </a>

			                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
			                                        @csrf
			                                    </form>
												<!-- <a href="custom/user/login-v2.html" target="_blank" class="btn btn-label btn-label-brand btn-sm btn-bold">Sign Out</a> -->
												<!-- <a href="custom/user/login-v2.html" target="_blank" class="btn btn-clean btn-sm btn-bold">Upgrade Plans</a> -->
											</div>
										</div>

										<!--end: Navigation -->
									</div>
								</div>

								<!--end: User bar -->

								<!--begin: Quick panel toggler -->
							<!-- 	<div class="kt-header__topbar-item" data-toggle="kt-tooltip" title="Quick panel" data-placement="top">
									<div class="kt-header__topbar-wrapper">
										<span class="kt-header__topbar-icon" id="kt_quick_panel_toggler_btn"><i class="flaticon2-cube-1"></i></span>
									</div>
								</div> -->

								<!--end: Quick panel toggler -->
							</div>

							<!-- end:: Header Topbar -->
						</div>
					</div>

					<!-- end:: Header -->

					<!-- begin:: Aside -->
					
					<div class="sidenav"  id="mySidenav">
					<button class="closebtn" onclick="closeNav()" id="kt_aside_close_btn"><i class="la la-close"></i></button>
						<!-- begin:: Aside Menu -->
						<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
							<div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1">
								<ul class="kt-menu__nav ">
									<!-- <li class="kt-menu__item " aria-haspopup="true"><a target="_blank" href="https://keenthemes.com/metronic/preview/demo9/builder.html" class="kt-menu__link "><i class="kt-menu__link-icon fa fa-cog"></i><span class="kt-menu__link-text">Layout </span></a></li> -->
									<li class="kt-menu__section ">
										<h4 class="kt-menu__section-text">Admin</h4>
										<i class="kt-menu__section-icon flaticon-more-v2"></i>
									</li>
									<li class="kt-menu__item " aria-haspopup="true"><a href="{{route('home')}}" class="kt-menu__link "><i class="kt-menu__link-icon fa fa-home"></i><span class"kt-menu__link-text">Dashboard</span></a></li>	
									<li class="kt-menu__item " aria-haspopup="true"><a href="{{route('adminUsers')}}" class="kt-menu__link "><i class="kt-menu__link-icon fa flaticon2-user"></i><span class"kt-menu__link-text">Users</span></a></li>	


								<!-- 	<li class="kt-menu__section ">
										<h4 class="kt-menu__section-text">App</h4>
										<i class="kt-menu__section-icon flaticon-more-v2"></i>
									</li>
									<li class="kt-menu__item " aria-haspopup="true"><a href="{{route('appUsers')}}" class="kt-menu__link "><i class="kt-menu__link-icon fa flaticon-users-1"></i><span class"kt-menu__link-text">Users</span></a></li>	 -->
									<li class="kt-menu__item " aria-haspopup="true"><a href="{{route('appUsers')}}" class="kt-menu__link "><i class="kt-menu__link-icon fa fa-image"></i><span class"kt-menu__link-text">Posts</span></a></li>
									<li class="kt-menu__item " aria-haspopup="true"><a href="{{route('listcredits')}}" class="kt-menu__link "><i class="kt-menu__link-icon fa fa-donate"></i><span class"kt-menu__link-text">Credits</span></a></li>	
									<li class="kt-menu__item " aria-haspopup="true"><a href="{{route('transaction')}}" class="kt-menu__link "><i class="kt-menu__link-icon fa fa-money-check-alt"></i><span class"kt-menu__link-text">Transaction</span></a></li>	
									<li class="kt-menu__item " aria-haspopup="true"><a href="{{route('cms')}}" class="kt-menu__link "><i class="kt-menu__link-icon fa fa-cog"></i><span class"kt-menu__link-text">CMS</span></a></li>	

								</ul>
							</div>
						</div>

						<!-- end:: Aside Menu -->
					</div>

					<!-- end:: Aside -->
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
					  @yield('content')	
					</div>

					<!-- begin:: Footer -->
					<div class="kt-footer kt-grid__item" id="kt_footer" style="background-color:#29c28d">
						<div class="kt-container ">
							<div class="kt-footer__wrapper">
								<div class="kt-footer__copyright footer">
									2020&nbsp;&copy;&nbsp;<a href="https://kaspontech.com" target="_blank" class="kt-link">Kaspontech.com.All Rights Reserved</a>
								</div>
								<div class="kt-footer__menu">
									<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">About</a>
									<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">Team</a>
									<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">Contact</a>
								</div>
							</div>
						</div>
					</div>

					<!-- end:: Footer -->
				</div>
			</div>
		</div>

		<!-- end:: Page -->

		<!-- begin::Quick Panel -->
	  

		<!-- end::Quick Panel -->

		<!-- begin::Scrolltop -->
		<div id="kt_scrolltop" class="kt-scrolltop">
			<i class="fa fa-arrow-up"></i>
		</div>

		<!-- end::Scrolltop -->

		<!-- begin::Sticky Toolbar -->
	<!-- 	<ul class="kt-sticky-toolbar" style="margin-top: 30px;">
			<li class="kt-sticky-toolbar__item kt-sticky-toolbar__item--success" id="kt_demo_panel_toggle" data-toggle="kt-tooltip" title="Check out more demos" data-placement="right">
				<a href="#" class=""><i class="flaticon2-drop"></i></a>
			</li>
			<li class="kt-sticky-toolbar__item kt-sticky-toolbar__item--brand" data-toggle="kt-tooltip" title="Layout Builder" data-placement="left">
				<a href="https://keenthemes.com/metronic/preview/demo9/builder.html" target="_blank"><i class="flaticon2-gear"></i></a>
			</li>
			<li class="kt-sticky-toolbar__item kt-sticky-toolbar__item--warning" data-toggle="kt-tooltip" title="Documentation" data-placement="left">
				<a href="https://keenthemes.com/metronic/?page=docs" target="_blank"><i class="flaticon2-telegram-logo"></i></a>
			</li>
			<li class="kt-sticky-toolbar__item kt-sticky-toolbar__item--danger" id="kt_sticky_toolbar_chat_toggler" data-toggle="kt-tooltip" title="Chat Example" data-placement="left">
				<a href="#" data-toggle="modal" data-target="#kt_chat_modal"><i class="flaticon2-chat-1"></i></a>
			</li>
		</ul> -->

			<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#591df1",
						"light": "#ffffff",
						"dark": "#282a3c",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
		</script>

		<!-- end::Global Config -->

		<!--begin::Global Theme Bundle(used by all pages) -->
		<script src="{{asset('/plugins/global/plugins.bundle.js')}}" type="text/javascript"></script>
		<script src="{{asset('/js/scripts.bundle.js')}}" type="text/javascript"></script>

		<!--end::Global Theme Bundle -->

		<!--begin::Page Vendors(used by this page) -->
		<script src="{{asset('/plugins/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
		<script src="//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM" type="text/javascript"></script>
		<script src="{{asset('/plugins/custom/gmaps/gmaps.js')}}" type="text/javascript"></script>

		<!--end::Page Vendors -->

		<!--begin::Page Scripts(used by this page) -->
		<script src="{{asset('/js/pages/dashboard.js')}}" type="text/javascript"></script>
		<script src="{{asset('js/datatables/users.js')}}" type="text/javascript"></script>
		<script src="{{asset('js/datatables/app_users.js')}}" type="text/javascript"></script>
		<script src="{{asset('js/datatables/transaction.js')}}" type="text/javascript"></script>
		<script src="{{asset('js/datatables/credits.js')}}" type="text/javascript"></script>
		<script src="{{asset('js/datatables/cms.js')}}" type="text/javascript"></script>
		<script src="{{asset('js/datatables/post_verification.js')}}" type="text/javascript"></script>
		<script src="{{asset('js/my-script.js')}}" type="text/javascript"></script>
		<script src="{{asset('js/bootstrap-formhelpers.min.js')}}" type="text/javascript"></script>
		<script src="{{asset('js/validations/form-controls.js')}}" type="text/javascript"></script>

		<!--  editors  -->
		<script src="{{asset('js/editors/ckeditor-classic.bundle.js')}}" type="text/javascript"></script>
		<script src="{{asset('js/editors/ckeditor-classic.js')}}" type="text/javascript"></script>
		<!--end::Page Scripts -->

		<script type="text/javascript">
		 function openNav() {
			document.getElementById("mySidenav").style.width = "250px";
			document.getElementById("main").style.marginLeft = "250px";
		}
		function closeNav() {
			document.getElementById("mySidenav").style.width = "0";
			document.getElementById("main").style.marginLeft = "0";
		}
		</script>
	</body>

	<!-- end::Body -->
</html>