@extends('layouts.admin')

@section('content')

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">
				<button class="kt-subheader__mobile-toggle kt-subheader__mobile-toggle--left" id="kt_subheader_mobile_toggle"><span></span></button>
				Admin</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="" class="kt-subheader__breadcrumbs-link">
					CMS</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
			</div>
		</div>
	
	</div>
</div>
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
	<div class="kt-portlet__body">
	<!--begin: Search Form -->
	<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
		<div class="row align-items-center">
			<div class="col-xl-8 order-2 order-xl-1">
				<div class="row align-items-center">		
					<div class="col-md-3 kt-margin-b-20-tablet-and-mobile">
						<div class="kt-form__group kt-form__group--inline">
							<div class="kt-form__label">
								<label>Status:</label>
							</div>
							<div class="kt-form__control">
								<select class="form-control" id="kt_form_status_app_cms">
									<option value="">All</option>
									<option value="1">Active</option>
									<option value="2">Expired</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-3 kt-margin-b-20-tablet-and-mobile">
						<div class="kt-input-icon kt-input-icon--left">
							<input type="text" class="form-control" placeholder="Search..." id="generalSearch">
							<span class="kt-input-icon__icon kt-input-icon__icon--left">
								<span><i class="la la-search"></i></span>
							</span>
						</div>
					</div>
					<a href="#" class="btn btn-brand btn-elevate btn-icon-sm" data-toggle="modal" data-target="#kt_modal_4_22">
						<i class="la la-plus"></i>
						New CMS
					</a>
				</div>
			</div>
			<!-- <div class="col-xl-4 order-1 order-xl-2 kt-align-right">
				<a href="#" class="btn btn-default kt-hidden">
					<i class="la la-cart-plus"></i> New Order
				</a>
				<div class="kt-separator kt-separator--border-dashed kt-separator--space-lg d-xl-none"></div>
			</div> -->
		</div>
	</div>

	<!--end: Search Form -->
</div>
	<div class="kt-portlet__body kt-portlet__body--fit">
		<!--begin: Datatable -->
		<div class="kt-datatable-cms" id="json_data1"></div>
		<!--end: Datatable -->
	</div>
</div>
	<div class="modal fade" id="kt_modal_4_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-xl" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Edit Cms</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="container">
					    <div class="row justify-content-center">
					        <div class="col-md-12">
					                <div class="card-body">
					                	<div class="cms" id="cms">
					                		<div class="kt-portlet__body">
										  	 	<form id="edit_cms">
													<div class="kt-notification kt-notification--fit">
														<div class="form-group row">
									                        <label for="name" class="col-md-3 col-form-label text-md-right">Title</label>
									                        <div class="col-md-8" id="title">
									                            
									                        </div>
														</div>
														<div class="form-group row">
									                        <label for="name" class="col-md-3 col-form-label text-md-right">Content</label>
									                        <div class="col-md-8">
									                            <textarea id="edit_content" class="form-control " name="content" required autocomplete="content" autofocus ></textarea>

									                        </div>
														</div>
														<div class="form-group row">
									                        <label for="name" class="col-md-3 col-form-label text-md-right">Status</label>
									                        <div class="col-md-8">
									                           <select id="status" class="form-control" name="status">
									                           		<option value="1">Active</option>
									                           		<option value="2">Expired</option>
									                           </select>
									                        </div>
									                    </div>
									                    <div class="form-group row mb-0">
									                        <div class="col-md-6 offset-md-4">
									                        	<button type="reset" class="btn btn-secondary" ">Reset</button>
									                            <button type="submit" class="btn btn-primary" id="update_cms">
									                                Update
									                            </button>
									                        </div>
									                    </div>
													</div>
													</form>
												</div>
					                	</div>
					                </div>
					          </div>
					     </div>
					    </div>
				</div>
			</div>
		</div>
	</div>
		<div class="modal fade" id="kt_modal_1_5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="kt-scroll" data-scroll="true" data-height="200">
						<p>Are you sure want delete ?</p>
						
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<div class="block" id="block_alert">

                	</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="kt_modal_4_22" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-xl" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">New CMS</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="container">
					    <div class="row justify-content-center">
					        <div class="col-md-12">
					                <div class="card-body">
					                    <form method="POST" action="{{ route('newCms') }}" id="kt_form_cms">
					                        @csrf

					                        <div class="form-group row">
					                            <label for="title" class="col-md-3 col-form-label text-md-right">{{ __('Title') }}</label>

					                            <div class="col-md-8">
					                                <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" required autocomplete="title" autofocus>
					                                @error('title')
					                                    <span class="invalid-feedback" role="alert">
					                                        <strong>{{ $message }}</strong>
					                                    </span>
					                                @enderror
					                            </div>
					                        </div>

					                        <div class="form-group row">
					                            <label for="content" class="col-md-3 col-form-label text-md-right">{{ __('Content') }}</label>

					                            <div class="col-md-8">
					                            <!-- 	<textarea name="kt-ckeditor-2" id="kt-ckeditor-2"> -->
					                            	<textarea id="content" class="form-control @error('content') is-invalid @enderror" name="content" required autocomplete="content" autofocus>{{ old('content') }}</textarea>
					                                

					                                @error('content')
					                                    <span class="invalid-feedback" role="alert">
					                                        <strong>{{ $message }}</strong>
					                                    </span>
					                                @enderror
					                            </div>
					                        </div>
					                        
					                        <div class="form-group row">
					                            <label for="status" class="col-md-3 col-form-label text-md-right">{{ __('Status') }}</label>

					                            <div class="col-md-8">
					                                <select class="form-control" id="kt_form_status_app_credits" name="status">
														<option value="1">Active</option>
														<option value="2">Expired</option>
													</select>

					                                @error('status')
					                                    <span class="invalid-feedback" role="alert">
					                                        <strong>{{ $message }}</strong>
					                                    </span>
					                                @enderror
					                            </div>
					                        </div>
					                        
					                        
					                       
					                        
					                        <div class="form-group row mb-0">
					                            <div class="col-md-6 offset-md-4">
					                            	<button type="reset" class="btn btn-secondary" ">Reset</button>
					                                <button type="submit" class="btn btn-primary" id="btn_submit">
					                                    {{ __('Add') }}
					                                </button>
					                            </div>
					                        </div>
					                    </form>
					                </div>
					            </div>
					        </div>
					    </div>
				</div>
			</div>
		</div>
</div>

@endsection