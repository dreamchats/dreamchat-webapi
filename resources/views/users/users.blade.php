@extends('layouts.admin')

@section('content')

<!-- begin:: Subheader -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">
				<button class="kt-subheader__mobile-toggle kt-subheader__mobile-toggle--left" id="kt_subheader_mobile_toggle"><span></span></button>
				Admin</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<a href="" class="kt-subheader__breadcrumbs-link">
					Users</a>
				<span class="kt-subheader__breadcrumbs-separator"></span>
				<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
			</div>
		</div>
	
	</div>
</div>
<div class="kt-container  kt-grid__item kt-grid__item--fluid">
	<div class="kt-portlet__body">
	<!--begin: Search Form -->
	<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
		<div class="row align-items-center">
			<div class="col-xl-8 order-2 order-xl-1">
				<div class="row align-items-center">
					
				
					<div class="col-md-3 kt-margin-b-20-tablet-and-mobile">
						<div class="kt-form__group kt-form__group--inline">
							<div class="kt-form__label">
								<label>Status:</label>
							</div>
							<div class="kt-form__control">
								<select class="form-control" id="kt_form_status_app_users">
									<option value="">All</option>
									<option value="4">Inactive</option>
									<option value="1">Active</option>
									<option value="2">Blocked</option>
									<option value="3">Reported</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-3 kt-margin-b-20-tablet-and-mobile">
						<div class="kt-input-icon kt-input-icon--left">
							<input type="text" class="form-control" placeholder="Search..." id="generalSearch">
							<span class="kt-input-icon__icon kt-input-icon__icon--left">
								<span><i class="la la-search"></i></span>
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--end: Search Form -->
</div>
	<div class="kt-portlet__body kt-portlet__body--fit">
		<!--begin: Datatable -->
		<div class="kt-datatable-users" id="json_data1"></div>
		<!--end: Datatable -->
	</div>
</div>
	<div class="modal fade" id="kt_modal_4_42" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-xl" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Credits</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="container">
					    <div class="row justify-content-center">
					        <div class="col-md-12">
					            <div class="card-body">
					            	<div class="kt-datatable-credit" id="kt_datatable_credits">

								   	</div>
					          	</div>
					     	</div>
					    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="kt_modal_4_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-xl" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Posts</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="container">
					    <div class="row justify-content-center">
					        <div class="col-md-12">
				                <div class="card-body">
								   <div class="kt-datatable-post tablePost" id="kt_datatable">
								   </div>
					          </div>
					     </div>
					    </div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="kt_modal_4_41" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-xl" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Posts</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						</button>
					</div>
					<div class="modal-body">
						<div class="container">
						    <div class="row justify-content-center">
						        <div class="col-md-12">
						                <div class="card-body">
						                	<div class="viewPostData" id="viewPostData">

						                	</div>
						                </div>
						          </div>
						     </div>
						    </div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="kt_modal_1_5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="kt-scroll" data-scroll="true" data-height="200">
						<p>Are you sure want delete ?</p>
						
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<div class="block" id="block_alert">

                	</div>
				</div>
			</div>
		</div>
	</div>

@endsection