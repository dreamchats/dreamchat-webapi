<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikesComment extends Model
{
     protected $fillable = [
        'comment_id', 'user_id','status'
    ];
    protected $table = 'like_comments';
    public $timestamps = true;
}
