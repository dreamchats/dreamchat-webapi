<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
     protected $fillable = [
        'user_id','request_from','request_to','status','read','all_read','message','app','post_id'
    ];
    protected $table = 'nottifications';
    public $timestamps = true;
}
