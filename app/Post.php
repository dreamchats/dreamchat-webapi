<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class Post extends Model
{
    protected $fillable = [
        'title','image','description','user_id','shared_user_id','mood_id','shared_post_id'
    ];
    protected $table = 'posts';
    public $timestamps = true;
    public function user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}
	 public function mood()
	{
		return $this->belongsTo('App\Mood', 'mood_id');
	}
	 public function shared_user()
	{
		return $this->belongsTo('App\User', 'shared_user_id');
	}
	 public function getImageAttribute($image)
      {
      	 $image_url=URL::to('');
        if($image == "" ||  $image == $image_url){
            //$this->attributes['image']="";
        }else{
       		$this->attributes['post_image'] = $image_url.$image;
       		  return $this->attributes['post_image'] ;
       
        }
        
      

      }

}
