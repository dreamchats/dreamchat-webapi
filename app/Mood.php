<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class Mood extends Model
{
      protected $fillable = [
        'mood','other_mood','dream_location','dream_event','tags','post_id','user_id','mood_flag'
    ];
    protected $table = 'moods';
    public $timestamps = true;
    public function getMoodAttribute($mood)
    {
    	//print_r($mood);'<br>';
        $image_url_base=URL::to('');
        if($image_url_base == "" ||  $mood == $image_url_base){
            // $this->attributes['mood']="";
        }else{
            $this->attributes['mood_image'] = $image_url_base.$mood;
             return $this->attributes['mood_image'] ;
        }
        
       
      }
}
