<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Savedpost extends Model
{
   protected $fillable = [
        'post_id', 'user_id','mood_id'
    ];
    protected $table = 'saved_post';
    public $timestamps = true;
     public function post()
	{
		return $this->belongsTo('App\Post', 'post_id');
	}
	 public function user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}
	  public function mood()
	{
		return $this->belongsTo('App\Mood', 'mood_id');
	}
}
