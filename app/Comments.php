<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
 use Kalnoy\Nestedset\NodeTrait;

class Comments extends Model
{
	 use NodeTrait;
    protected $fillable = [
        'comments_description','parent_id','comment_id','user_id','post_id'
    ];
    protected $table = 'comments';
    public $timestamps = true;
    public function subcategory(){
        return $this->hasMany('App\Comments', 'comment_id');
    }
      public function user()
    {
        return $this->belongsTo('App\User', 'user_id','id');
    }

     public function parent()
    {
        return $this->belongsTo('App\Comments','comment_id')->where('comment_id',0);
    }

    public function children()
    {
        return $this->hasMany('App\Comments','comment_id');
    }
    public function likes()
    {
        return $this->hasMany('App\LikesComment','comment_id');
    }
}
