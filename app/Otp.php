<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Otp extends Model
{
    protected $fillable = [
        'otp', 'user_id',
    ];
    protected $table = 'otp_verification';
    public $timestamps = true;


}
