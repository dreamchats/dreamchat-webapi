<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerCredits extends Model
{
	protected $fillable = [
        'credits','user_id','remains_credit','status','credit_id'
    ];
    protected $table = 'customer_credits';
    public $timestamps = true;
}
