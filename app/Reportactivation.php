<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reportactivation extends Model
{
    protected $fillable = [
        'otp', 'user_id',
    ];
    protected $table = 'reported_activation';
    public $timestamps = true;
}
