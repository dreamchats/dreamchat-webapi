<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $fillable = [
        'user_id','theme_color','mete_name','meta_description','report_count','credit_per_request'
    ];
    protected $table = 'settings';
    public $timestamps = true;
}
