<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resetpassword extends Model
{
     protected $fillable = [
        'otp', 'user_id',
    ];
    protected $table = 'reset_passowrd';
    public $timestamps = true;
}
