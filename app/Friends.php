<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class Friends extends Model
{
     protected $fillable = [
        'request_from','request_to','status','follow_receiver'
    ];
    protected $table = 'friends_list';
    public $timestamps = true;
    public function requestFrom()
	{
		return $this->belongsTo('App\User', 'request_from');
	}
	public function requestTo()
	{
		
		return $this->belongsTo('App\User', 'request_to');
	}
	 // public function getImageAttribute($image)
  //     {
  //       $image_url=URL::to('');
  //       $this->attributes['User_image'] = $image_url.$image;

  //     }
}
