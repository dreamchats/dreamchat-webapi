<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LikePost extends Model
{
     protected $fillable = [
        'post_id', 'user_id','status'
    ];
    protected $table = 'like_posts';
    public $timestamps = true;

}
