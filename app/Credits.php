<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credits extends Model
{
    
    protected $fillable = [
        'credits_points','amount','status',
    ];
    protected $table = 'credits';
    public $timestamps = true;
}
