<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Post;
use App\Transaction;
use View;
use Illuminate\Support\Facades\Redirect;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $active_users=User::where('status',1)->count();
        $blocked_users=User::where('status',2)->count();
        $success_transaction=Transaction::where('status',1)->count();
        $failed_transaction=Transaction::where('status',2)->count();
        $all_posts=Post::count();
        $users=User::count();
       return View::make('home')->with(compact('users','active_users','blocked_users','all_posts','success_transaction','failed_transaction'));
        // return view('home');
    }
}
