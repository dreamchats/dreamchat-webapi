<?php
namespace App\Http\Controllers;

use View;
use Illuminate\Http\Request;
use Auth;
use App\Cms;
use Illuminate\Support\Facades\Validator;

class CmsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
	 public function listcms(){
    	$cms=Cms::get();
        return View::make('cms/cms')->with(compact('cms'));
    }
	public function cms(Request $request){
    	$credits=Cms::get();
		return json_encode($credits);
	}
	 public function blockCms(Request $request){
        $plan_id= $request->id;
        $block_plan=Cms::where('id',$plan_id)->update([
            'status' =>2
        ]);
        if($block_plan){
            $message="1";
        }
        else{
            $message="2";
        }
        return json_encode($message);

    }
    public function unblockCms(Request $request){
        $plan_id= $request->id;
        $block_plan=Cms::where('id',$plan_id)->update([
            'status' =>1
        ]);
        if($block_plan){
            $message="1";
        }
        else{
            $message="2";
        }
        return json_encode($message);

    }
    public function editCms($id){
    	$credits=Cms::where('id',$id)->first();
    	return json_encode($credits);
    }
    public function updateCms(Request $request){
    	$id = $request->id;
    	$status = $request->status;
    	$title = $request->title;
    	$content = $request->content;

    	$cms = Cms::where('id', $id)->update([
    		'title' => $title,
    		'content' => $content,
    		'status' => $status
    	]);

    	if($cms) {
    		$success = '1';
    	} else{
            $success="2";
        }
        return $success;

    }
    public function deleteCms(Request $request){
    	$plan_id= $request->id;
    	$delete_credits=Cms::find($plan_id)->delete();
    	if($delete_credits){	
    		$success = '1';
    	} else{
            $success="2";
        }
        return $success;
    }

    public function newCms(Request $request)
    {
        $title = $request->title;
        $content = $request->content;
        $status = $request->status;

        $cms = Cms::create([
            'title' => $title,
            'content' => $content,
            'status' => $status
        ]);

        return redirect()->route('listcms'); 

    }
}


