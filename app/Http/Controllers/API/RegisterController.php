<?php


namespace App\Http\Controllers\API;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use App\Otp;
use Mail;
class RegisterController extends Controller
{
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function signup(Request $request)
    {
        $input['email'] = $request->email;
        $rules = array(
            // 'full_name'=>'required',
            'email' => 'unique:login,email');
        $validator = Validator::make($input, $rules);
        $fourdigitrandom = rand(1000,9999); 
        if ($validator->fails()) {
            $message="Email Already exist";
        }
        else{
             $users = User::create([
            'full_name' => $request->full_name,
            'email' => $request->email,
            'gender' => $request->gender,
            'dob' => $request->dob,
            'password' => bcrypt($request->password),
            'status' =>1
            ]);
            if($users){
                $user_details=User::where('email',$request->email)->first();
                $otp=Otp::create([
                    'otp'=>$fourdigitrandom,
                    'user_id'=>$user_details->id
                 ]);
                if($otp){
                    Mail::send('email.success', ['request' => $request,'fourdigitrandom'=>$fourdigitrandom], function ($m) use ($request,$fourdigitrandom) {
                    $m->from('hello@app.com', 'Dreamchat ');
                    $m->to($request->email, $request->full_name)->subject(' Dreamchat One time password for activation');

                    });
                    $message="Successfully created user! please check email for verification ";
                }
               else{

                    $message="Cannot able to generate OTP,Try again ";
               }
                
            }
            else{
                $message="Something went wrong";
            }
        }
       
        return response()->json([
            'message' => $message,
        ], 201);
    }
  
    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $status=1;
        $status_user=User::where('email',$request->email)->first();
        if($status_user){
            if($status_user->otp_verified==1 && $status_user->status == 1){
                $credentials = request(['email', 'password']);
                if(!Auth::attempt($credentials))
                    return response()->json([
                        'sucess' =>'false',
                        'message' => 'Unauthorized'
                    ], 200);
                $user = $request->user();
                $tokenResult = $user->createToken('Personal Access Token');
                if($user){
                     $api_token = $tokenResult->accessToken;
                      // print_r($api_token);
                     $update_token=User::where('id',$user->id)->where('access_token','')->update([
                        'access_token'=>$api_token
                     ]);
                }
                // print_r('API TOKEN');
                // dd($api_token);
               $user_data=User::where('id',$user->id)->first();
                if ($request->remember_me)
                    $token->expires_at = Carbon::now()->addWeeks(1);
                //$token->save();
                $data=array([
                    'access_token' => $tokenResult->accessToken,
                    'token_type' => 'Bearer',
                    'user'=>$user_data,
                    'login'=>'Yes',
                    'expires_at' => Carbon::parse(
                        $tokenResult->token->expires_at
                    )->toDateTimeString()
                ]);
            }
            else{
                if($status_user->otp_verified==0){
                    $data=array([
                    'message' => 'OTP not verified',
                    ]); 
                }
                else if($status_user->status==2){
                    $data=array([
                    'message' => 'Account is blocked',
                    ]); 
                }
                else if($status_user->status==4){
                    $data=array([
                    'message' => 'Account not activated',
                    ]); 
                }
                else{
                    $data=array([
                    'message' => 'Something went wrong',
                    ]); 
                }
            }
            return response()->json([
                'sucess' => 'True',
                'data'=> $data
            ] );
        }
        else{
            $data=array([
                'message'=>'user doesnot exist',
            ]);
            return response()->json([
                'sucess' => 'false',
                'data'=> $data
            ] );
        }
        
    }
  
    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }
  
    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        $products = auth()->user()->products;
        return response()->json([
            'success' => true,
            'data' => $products
        ]);
    }

    public function otp_verify(Request $request){
        $email=$request->email;
        $otp=$request->otp;
        $user=User::where('email',$email)->first();
        $otp_data=Otp::where('user_id',$user->id)->first();
        if($user->otp_verified==0){
           if($otp == $otp_data->otp){
                $user_update=User::where('email',$email)->update([
                    'otp_verified'=>1,
                    'status'=>1
                ]);
                if($user_update){
                    $message='OTP Successfully verified';
                }
                else{
                    $message='OTP Verification failed';
                }
            }
            else{
               $message='OTP Mismatch';
            } 
        }
        else{
              $message='Already Verified';
        }
        return response()->json([
            'message' => $message,
        ], 201);
    }
    public function otp_resend(Request $request){
        $fourdigitrandom = rand(1000,9999); 
        $user_details=User::where('email',$request->email)->first();
        $otp=Otp::where('user_id',$user_details->id)->update([
            'otp'=>$fourdigitrandom,
            'user_id'=>$user_details->id
         ]);
        if($otp){
            Mail::send('email.success', ['request' => $request,'fourdigitrandom'=>$fourdigitrandom], function ($m) use ($request,$fourdigitrandom) {
            $m->from('hello@app.com', 'Dreamchat ');
            $m->to($request->email, $request->full_name)->subject(' Dreamchat One time password for activation');

            });
            $message="OTP was regenerated! please check email for verification code";
        }
       else{

            $message="Cannot able to generate OTP,Try again ";
       }
        return response()->json([
            'message' => $message,
        ], 201);
    }
    public function email_validation(Request $request){
         $email=$request->email;
         $user=User::where('email',$email)->first();
         if($user){
            $message='Email already exist';
         }
         else{
             $message='Email Doesnot exist';
         }
        return response()->json([
            'message' => $message,
        ], 201);
    }
}
