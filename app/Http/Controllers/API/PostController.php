<?php

namespace App\Http\Controllers\API;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use App\Post;
use App\Mood;
use App\Friends;
use App\Savedpost;
use App\Reportpost;
use App\Reportactivation;
use App\Settings;
use App\CustomerCredits;
use App\Notifications;
use App\LikePost;
use App\Comments;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
class PostController extends Controller
{


     // public function __construct()
  //   {
  //       $this->middleware('auth');
  //   }
    public function index()
    {
        $products = Post::get();
 
        return response()->json([
            'success' => true,
            'data' => $products
        ]);
    }

    public function createPost(Request $request){
        $users = auth()->user();
        $input['title'] = $request->title;
        $input['description'] = $request->description;
        $input['add_mood'] = $request->add_mood;
        $input['mood_flag'] = $request->mood_flag;
        $rules = array(
                        'title' => 'required',
                        'description' => 'required',
                        'add_mood' => 'required',
                        'mood_flag' => 'required',

                       );
        $validator = Validator::make($input, $rules);
        $fourdigitrandom = rand(1000,9999); 
        if ($validator->fails()) {
            $message ="Please fill the required fields";
        }
        else{
            if($users){
                if($request->image != null){
                    $base64_image = $request->image;
                    if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                    $data = substr($base64_image, strpos($base64_image, ',') + 1);

                    $data = base64_decode($data);
                    //dd($data);
                    $image_name=$request->title ."_".$users->id. ".png";
                    //dd($image_name); 
                    Storage::disk('local')->put($image_name, $data);
                    $image_names="/App/Images/".$image_name;
                    }
                }
                else{
                    $image_names="";
                }
                $post=Post::create([
                    'title' => $request->title,
                    'image' => $image_names,
                    'description' => $request->description,
                    'user_id' => $users->id    
                ]);
                if($post){
                    if($request->has('add_mood') || $request->has('other_mood') || $request->has('dream_location') || $request->has('dream_event')){
                       if($request->add_mood != "" || $request->other_mood !="" || $request->dream_location != "" || $request->dream_event != "") {
                        if($request->image != null){
                            $base64_image_add_mood = $request->add_mood;
                            if (preg_match('/^data:image\/(\w+);base64,/', $base64_image_add_mood)) {
                            $data = substr($base64_image_add_mood, strpos($base64_image_add_mood, ',') + 1);

                            $data = base64_decode($data);
                            //dd($data);
                            $image_name_mood=$request->title ."_".$users->id. ".png";
                            //dd($image_name); 
                            Storage::disk('local')->put($image_name_mood, $data);
                            $image_names_mood="/App/Images/".$image_name_mood;
                            }
                        }
                        else{
                            $image_names="";
                        }
                            $mood=Mood::create([
                                'mood'=>$image_names_mood,
                                'other_mood'=> $request->other_mood,
                                'dream_location' => $request->dream_location,
                                'dream_event' => $request->dream_event,
                                'tags' => $request->tags,
                                'post_id' =>$post->id,
                                'user_id'=>$users->id,
                                'mood_flag'=>$request->mood_flag
                            ]);
                            if($mood){
                                $update_user=Post::where('id',$post->id)->update([
                                    'mood_id'=>$mood->id
                                ]);
                                 $notification_create=Notifications::create([
                                    'user_id'=>$users->id,
                                    'request_from'=>$users->id,
                                    'request_to' =>0,
                                    'message'=>'Created post',
                                    'post_id'=>$post->id,
                                    'app'=>0,
                                    'status'=>1
                                ]);
                            }
                       }
                    }
                    $message="Post created Successfully";
                }
                else{
                    $message ="Error in adding post";
                }
            }
            else{
            $message = "Unauthorized";
            }
        }
        return response()->json([
            'message' => $message
        ]);
    }

    public function publicWall(){
        $users = auth()->user();
         if($users){
            // if($users['image'] == ""){
            //     $image_url=URL::to('').$users['image'];
            //     $users->setAttribute('image', $image_url);
            // }
            // else{
            //     $users->setAttribute('image', '');
            // }
           
            $data = collect([]);
            $posts=collect([]);
            $friends_lists=Friends::where('request_from',$users->id)->orWhere('request_to',$users->id)->get();
                if($friends_lists){//if have friends so that share friends post
                    foreach ($friends_lists as $friends_list) {
                        if($friends_list->request_from == $users->id){
                            $user=User::where('id',$friends_list->request_to)->first();
                        }
                        else if($friends_list->request_to == $users->id){
                            $user=User::where('id',$friends_list->request_from)->first();
                        }
                        else{
                            $user=User::where('id',$friends_list->request_from)->orWhere('id',$friends_list->request_to)->first();
                        }

                       if($user){//user exist
                        // print_r($user);
                         if($user->account_type == 0) {
                            // print_r('User_id', $user->user_id);
                            $posts=Post::with('user','mood','shared_user')->where('user_id',$user->id)->where('status',1)->orderBy('id', 'desc')->paginate(5);
                            if($posts){
                                foreach ($posts as $post) {
                                 
                                    $like=LikePost::where('post_id',$post->id)->where('status',1)->count();
                                    $post->setAttribute('likecount', $like);
                                    $likes=LikePost::where('post_id',$post->id)->where('user_id',$users->id)->where('status',1)->count();
                                    $post->setAttribute('likes', $likes);
                                    $comments=Comments::where('post_id',$post->id)->count();
                                    $post->setAttribute('comments', $comments);
                                    if ($data->where('id', $post->id)->count() === 0){
                                        $data->push($post);
                                    }  
                                 }
                            }
                            else{
                                $data=$data;
                            }
                        }
                        else if($user->account_type == 1){
                            if($friends_list->request_from == $users->id){ //request from current user
                                if($friends_list->status == 1){ //request send so check the status as accepted
                                    $posts=Post::with('user','mood','shared_user')->where('user_id',$user->id)->where('status',1)->orderBy('id', 'desc')->paginate(5);
                                    if($posts){
                                        foreach ($posts as $post) {
                                           
                                                $like=LikePost::where('post_id',$post->id)->where('status',1)->count();
                                                $post->setAttribute('likecount', $like);
                                                $likes=LikePost::where('post_id',$post->id)->where('user_id',$users->id)->where('status',1)->count();
                                                $post->setAttribute('likes', $likes);
                                                $comments=Comments::where('post_id',$post->id)->count();
                                                 $post->setAttribute('comments', $comments);
                                            if ($data->where('id', $post->id)->count() === 0){
                                                $data->push($post);
                                            }  
                                         }
                                    }
                                }

                            }
                            else if($friends_list->request_to == $users->id){ // request  to
                                if($friends_list->follow_receiver == 1){ //request send so check the status as accepted
                                    $posts=Post::with('user','mood','shared_user')->where('user_id',$user->id)->where('status',1)->orderBy('id', 'desc')->paginate(5);
                                    if($posts){
                                        foreach ($posts as $post) {
                                         
                                                $like=LikePost::where('post_id',$post->id)->where('status',1)->count();
                                                $post->setAttribute('likecount', $like);
                                                $likes=LikePost::where('post_id',$post->id)->where('user_id',$users->id)->where('status',1)->count();
                                                $post->setAttribute('likes', $likes);
                                                $comments=Comments::where('post_id',$post->id)->count();
                                              $post->setAttribute('comments', $comments);
                                            if ($data->where('id', $post->id)->count() === 0){
                                                $data->push($post);
                                            }  
                                         }
                                    }
                                }
                            }
                        }
                    }
                    else{//if not user exist
                        $posts=$posts;
                    }
      

            }
            return response()->json([
                'success' => 'True',
                'data' => $data,
              //  'friend'=>$friends_lists
            ]);
         
         }
    }
}
     public function myWall(){
        $users = auth()->user();
        // if($users['image']==""){
        //     $users->setAttribute('image','');
        // }else{
        //     $users_image_url=URL::to('').$users['image'];
        //     $users->setAttribute('image', $users_image_url);
        // }
        
           $data = collect([]);
        // dd($users->id);
         if($users){
            $public_wall=Post::with('mood','shared_user')->where('user_id',$users->id)->where('status',1)->orderBy('id', 'desc')->paginate(10);

             foreach ($public_wall as $user) {
                // if($user['image']==""){
                   
                //     $user->setAttribute('image', '');
                // }else{
                //     $image_url=URL::to('').$user['image'];
                //     $user->setAttribute('image', $image_url);
                // }
                  
                  $like=LikePost::where('post_id',$user->id)->where('status',1)->count();
                    $user->setAttribute('likecount', $like);
                    $likes=LikePost::where('post_id',$user->id)->where('user_id',$users->id)->where('status',1)->count();
                    $user->setAttribute('likes', $likes);

                    $comments=Comments::where('post_id',$user->id)->count();
                    $user->setAttribute('comments', $comments);
                 if ($data->where('id', $user->id)->count() === 0){
                    $data->push($user);
                }  
            }
            return response()->json([
                'success' => 'True',
                'data' => $data,
                'user' => $users
            ]);
         }
         else{
             $message = "Unauthorized";
             return response()->json([
                    'message' => $message
                ]);
         }
         
    }

    public function getFriends(){
       $users = auth()->user(); 
       if($users){
         $friend_list=Friends::with('requestFrom','requestTo')->where('request_from',$users->id)
            ->orWhere('request_to',$users->id)->where('status',1)->get();
            // $friend_list->map(function ($friend) {
            //     $friend['image'] =URL::to('').$friend['image'];
            //     // return $post;
            // });
          return response()->json([
             'success' => 'True',
                'data' => $friend_list
            ]);
        }
        else{
             $message = "Unauthorized";
             return response()->json([
                    'message' => $message
                ]);
         }
         
    }
    public function getUsers(){
       $users = auth()->user(); 
       if($users){
            $data = collect([]);
            //$user_data=User::with('request_from','request_to')->where('role_id',0)->where('status',1)->get();
            $user_data=User::where('id', '!=', $users->id)->where('role_id',0)->where('status',1)->get();
          // $user_data=User::with('request_from','request_to')->get();
         
          return response()->json([
             'success' => 'True',
                'data' => $user_data
            ]);
        }
        else{
             $message = "Unauthorized";
             return response()->json([
                    'message' => $message
                ]);
         }
         
    }

    public function sendRequest(Request $request){
        $users = auth()->user(); 
        $input['request_from'] = $request->request_from;
        $input['request_to'] = $request->request_to;
        $rules = array(
                    'request_from' => 'required',
                    'request_to' => 'required',
                   );
        $validator = Validator::make($input, $rules);
        if($validator->fails()) {
            $message ="Please fill the required fields";
        }
        else{
            if($users){
                $get_data_from=Friends::where('request_from',$input['request_from'])
                            ->Where('request_to',$input['request_to'])
                            ->first();
                $get_data_to=Friends::where('request_from',$input['request_to'])
                            ->Where('request_to',$input['request_from'])
                            ->first();
                if($get_data_from || $get_data_to){
                    if($get_data_from){
                        if($get_data_from->status == 1 && $get_data_from->follow_receiver == 1)  {
                             $message='Already Friends';
                        }
                        else if($get_data_from->status == 1 && $get_data_from->follow_receiver == 0)  {
                            $message = "Following ";
                        }
                        else if($get_data_from->status == 2){
                             $message='Request send,Waiting for Approval';
                        }
                         else{
                            $message='Rejected';
                        }
                    }
                    else if($get_data_to){
                       if($get_data_to->status == 1 && $get_data_to->follow_receiver == 1)  {
                             $message='Already Friends';
                        }
                        else if($get_data_to->status == 1 && $get_data_to->follow_receiver == 0)  {
                            $update_follow_back=Friends::where('id',$get_data_to->id)->update([
                                'follow_receiver' => 2
                            ]);
                            if($update_follow_back){
                                $message = "successfully following back ";
                            }
                            else{
                                $message="Something went wrong";
                            }
                        }
                        else if($get_data_to->status == 2){
                             $message='Request send,Waiting for Approval';
                        }
                        else{
                            $message='Rejected';
                        }
                    }
                }
                else{
                    $friend=Friends::create([
                        'request_from'=>$request->request_from,
                        'request_to' =>$request->request_to,
                        'status' => '2'
                    ]);
                    if($friend){
                        $notification_create=Notifications::create([
                            'user_id'=>$users->id,
                            'request_from'=>$request->request_from,
                            'request_to' =>$request->request_to,
                            'message'=>'Friend request send',
                            'app'=>0,
                            'status'=>1
                        ]);
                        $settings=Settings::first();
                        if($settings){
                            $settings_request_point=$settings->credit_per_request;
                        }
                        else{ 
                            $settings_request_point=0;
                        }                 
                        $customercredits=CustomerCredits::where('user_id',$users->id)->first();
                         if($customercredits){
                            $customer_points=$customercredits->remains_credit;
                        }
                        else{ 
                            $customer_points=0;
                        }   
                        if($customercredits){
                            $finalCredit=CustomerCredits::where('user_id',$users->id)->get();
                            $final_point=$customer_points - $settings_request_point;
                            foreach ($finalCredit as $cuscredit) {
                                $customer_credit=CustomerCredits::where('id',$cuscredit->id)->update([
                                    'remains_credit'=>$final_point
                                ]);
                            }
                        }
                        $message="Request send";
                    }
                    else{
                     $message="Error in request";
                   }
                }
                
            }
            else{
                $message = "Unauthorized";
            }
        }
        return response()->json([
            'message' => $message
        ]);
    }

    public function getRequest(){

       $users = auth()->user(); 
       if($users){
        $data = collect([]);
        $request_data = collect([]);
        $get_request = Friends::with('requestFrom')->where('request_to',$users->id)->where('status',2)->get();
            foreach($get_request as  $friend_list) {
               // $data->push($friend_list);
                $friend_list->setAttribute('user_id', $friend_list->requestFrom['id']);
                $friend_list->setAttribute('full_name', $friend_list->requestFrom['full_name']);
                $friend_list->setAttribute('image', $friend_list->requestFrom['image']);
                $friend_list->setAttribute('email', $friend_list->requestFrom['email']);
                $friend_list->setAttribute('mobile_number', $friend_list->requestFrom['mobile_number']);
                $friend_list->setAttribute('dob', $friend_list->requestFrom['dob']);
                $friend_list->setAttribute('provider_id', $friend_list->requestFrom['provider_id']);
                $friend_list->setAttribute('gender', $friend_list->requestFrom['gender']);
                $friend_list->setAttribute('address', $friend_list->requestFrom['address']);
                $friend_list->setAttribute('zodiac', $friend_list->requestFrom['zodiac']);
                $friend_list->setAttribute('role_id', $friend_list->requestFrom['role_id']);
                $friend_list->setAttribute('status', $friend_list->requestFrom['status']);
                $friend_list->setAttribute('otp_verified', $friend_list->requestFrom['otp_verified ']);
                $friend_list->setAttribute('report_count', $friend_list->requestFrom['report_count']);
                $friend_list->setAttribute('account_type', $friend_list->requestFrom['account_type']);

                $relations = $friend_list->getRelations();

                unset($friend_list['request_to']);
                unset($friend_list->requestFrom);
                unset($friend_list['request_from']);
                unset($friend_list['follow_receiver']);
               // print_r($friend_list->requestFrom['full_name']);

                // $friend_list->setAttribute('ids', '1');
                 if ($data->where('id', $friend_list->id)->count() === 0){
                    $data->push($friend_list);
                 }  
        
       } 
         return response()->json([
             'success' => 'True',
                'data' => $data
            ]);
   }
    else{
        $message = "Unauthorized";
         return response()->json([
         'success' => $message,
        ]);
    }
     
    }

    public function getPosts(){
        $users = auth()->user(); 
        if($users){
            $get_post_count = Post::where('user_id',$users->id)->where('status',1)->count();
            return response()->json([
             'success' => 'True',
                'data' => $get_post_count
            ]);
        } 
        else{
            $message = "Unauthorized";
             return response()->json([
             'success' => $message,             
            ]);
        }
    }

    public function friendsWall(){
        $users = auth()->user();
         if($users){

           $data = collect([]);

            $get_friends = Friends::with('requestFrom','requestTo')->where('request_from',$users->id)->orWhere('request_to',$users->id)->where('status',1)->get();
            if($get_friends){//
                foreach ($get_friends as $get_friend) {
                    $friends_lists=Friends::with('requestFrom','requestTo')->where('request_from',$users->id)->orWhere('request_to',$users->id)->get();
                    foreach ($friends_lists as  $friends_list) {
                       if($friends_list){
                            if($users->account_type==0){
                                 // print_r($friends_list->request_from);
                                if($friends_list->request_from == $users->id){
                                    $user_id=$friends_list->request_to;
                                    $friend_data=User::where('id',$friends_list->request_to)->first();
                                    //print_r($friend_data->account_type);
                                    if($friend_data->account_type==0){
                                     //   print_r('public public');
                                         $posts=Post::with('user','mood','shared_user')->where('user_id',$user_id)->where('status',1)->orderBy('id', 'desc')->paginate(10);
                                        if($posts){
                                            foreach ($posts as $post) {
                                                
                                                 $like=LikePost::where('post_id',$post->id)->where('status',1)->count();
                                                    $post->setAttribute('likecount', $like);
                                                    $likes=LikePost::where('post_id',$post->id)->where('user_id',$users->id)->where('status',1)->count();
                                                    $post->setAttribute('likes', $likes);
                                                    $comments=Comments::where('post_id',$post->id)->count();
                                                     $post->setAttribute('comments', $comments);
                                                if ($data->where('id', $post->id)->count() === 0){
                                                    $data->push($post);
                                                }  
                                             }
                                        }
                                        else{

                                            $data=$data;
                                        }
                                    }
                                    else if($friend_data->account_type==1){
                                       // print_r('01');
                                        $user_id=$friends_list->request_to;
                                        $posts=Post::with('user','mood','shared_user')->where('user_id',$user_id)->where('status',1)->orderBy('id', 'desc')->paginate(10);
                                        if($posts){
                                                foreach ($posts as $post) {
                                                
                                                 $like=LikePost::where('post_id',$post->id)->where('status',1)->count();
                                                    $post->setAttribute('likecount', $like);
                                                    $likes=LikePost::where('post_id',$post->id)->where('user_id',$users->id)->where('status',1)->count();
                                                    $post->setAttribute('likes', $likes);
                                                    $comments=Comments::where('post_id',$post->id)->count();
                                                     $post->setAttribute('comments', $comments);
                                                if ($data->where('id', $post->id)->count() === 0){
                                                    $data->push($post);
                                                }  
                                             }
                                        }
                                        else{
                                            $data=$data;
                                        }
                                    }
                                    else{
                                        $data=$data;
                                    }
                                }else{//request to
                                if($friends_list->request_to=== $users->id){
                                    $friend_data=User::where('id',$friends_list->request_from)->first();
                                    if($friend_data->account_type == 1 ){
                                         // print_r('11 else');
                                        if($friends_list->follow_receiver==1){
                                            $user_id=$friends_list->request_from;
                                             if($friends_list->status == 1 &&  $friends_list->follow_receiver == 1){
                                                 $posts=Post::with('user','mood','shared_user')->where('user_id',$user_id)->where('status',1)->orderBy('id', 'desc')->paginate(10);
                                                if($posts){
                                                    foreach ($posts as $post) {
                                                        
                                                         $like=LikePost::where('post_id',$post->id)->where('status',1)->count();
                                                            $post->setAttribute('likecount', $like);
                                                            $likes=LikePost::where('post_id',$post->id)->where('user_id',$users->id)->where('status',1)->count();
                                                            $post->setAttribute('likes', $likes);
                                                            $comments=Comments::where('post_id',$post->id)->count();
                                                             $post->setAttribute('comments', $comments);
                                                        if ($data->where('id', $post->id)->count() === 0){
                                                            $data->push($post);
                                                        }  
                                                     }
                                                }
                                                else{

                                                    $data=$data;
                                                }
                                             }
                                            else{

                                                $data=$data;
                                            }
                                         }
                                          else{

                                                $data=$data;
                                            }
                                        
                                }
                                else{
                              //  print_r('to public');

                                        $user_id=$friends_list->request_to;
                                         $posts=Post::with('user','mood','shared_user')->where('user_id',$user_id)->orderBy('id', 'desc')->where('status',1)->paginate(10);
                                            if($posts){
                                                foreach ($posts as $post) {
                                                    
                                                     $like=LikePost::where('post_id',$post->id)->where('status',1)->count();
                                                        $post->setAttribute('likecount', $like);
                                                        $likes=LikePost::where('post_id',$post->id)->where('user_id',$users->id)->where('status',1)->count();
                                                        $post->setAttribute('likes', $likes);
                                                        $comments=Comments::where('post_id',$post->id)->count();
                                                         $post->setAttribute('comments', $comments);
                                                    if ($data->where('id', $post->id)->count() === 0){
                                                        $data->push($post);
                                                    }  
                                                 }
                                            }
                                            else{

                                                $data=$data;
                                            }
                                 
                                    }
                                }
                            }
                            }
                            else{//type 1
                                if($friends_list->request_to=== $users->id){
                                    $friend_data=User::where('id',$friends_list->request_from)->first();
                                    if($friend_data->account_type == 1 ){
                                         // print_r('11 e');

                                         $user_id=$friends_list->request_from;
                                         $posts=Post::with('user','mood','shared_user')->where('user_id',$user_id)->where('status',1)->orderBy('id', 'desc')->paginate(10);
                                            if($posts){
                                                foreach ($posts as $post) {
                                                    
                                                     $like=LikePost::where('post_id',$post->id)->where('status',1)->count();
                                                        $post->setAttribute('likecount', $like);
                                                        $likes=LikePost::where('post_id',$post->id)->where('user_id',$users->id)->where('status',1)->count();
                                                        $post->setAttribute('likes', $likes);
                                                        $comments=Comments::where('post_id',$post->id)->count();
                                                         $post->setAttribute('comments', $comments);
                                                    if ($data->where('id', $post->id)->count() === 0){
                                                        $data->push($post);
                                                    }  
                                                 }
                                            }
                                            else{

                                                $data=$data;
                                            }
                                }
                                else{
                               // print_r('10 e');

                                        $user_id=$friends_list->request_to;
                                         $posts=Post::with('user','mood','shared_user')->where('user_id',$user_id)->orderBy('id', 'desc')->paginate(10);
                                            if($posts){
                                                foreach ($posts as $post) {
                                                    
                                                     $like=LikePost::where('post_id',$post->id)->where('status',1)->count();
                                                        $post->setAttribute('likecount', $like);
                                                        $likes=LikePost::where('post_id',$post->id)->where('user_id',$users->id)->where('status',1)->count();
                                                        $post->setAttribute('likes', $likes);
                                                        $comments=Comments::where('post_id',$post->id)->count();
                                                        $post->setAttribute('comments', $comments);
                                                    if ($data->where('id', $post->id)->count() === 0){
                                                        $data->push($post);
                                                    }  
                                                 }
                                            }
                                            else{

                                                $data=$data;
                                            }
                                 
                                    }
                                }
                            }
        
                    }
                }
               
        }

   
         return response()->json([
            'success' => 'True',
            'data' => $data,
          ]);
              
        }
            
         else{
             $message = "Unauthorized";
             return response()->json([
                    'message' => $message
                ]);
         }
         }
        }
        public function follow(){
        $users = auth()->user();
        if($users){
            $get_friends = Friends::with('requestFrom','requestTo')->where('request_from',$users->id)->where('request_to',$users->id)->update([
                'follow_receiver' => 1
            ]);

            $message="";
        }
        else{
            $message = "Unauthorized";
            
        }
         return response()->json([
                'message' => $message
            ]);
        }

        public function unfollow_Following(Request $request){
        $users = auth()->user();
        $friend=$request->friend_id;
        if($users){
            $friend_check=Friends::where('id',$friend)->first();
            if($friend_check){
                if($friend_check->request_from == $users->id){
                   $friends=Friends::where('id',$friend)->update([
                    'status' => 4
                    ]); 
                    if($friends){
                         $message="unfollowing Successfully";

                    }     
                    else{
                        $message="Something went wrong";
                    }
                }
                else{
                        $message="Not requested";
                    }
                }
           else{
                $message="Not exist";
           }
        
        }
        else{
            $message = "Unauthorized";
           
        }
          return response()->json([
                'message' => $message
            ]);
        }

        public function unfollow_Follower(Request $request){
            $users = auth()->user();
            $friend=$request->friend_id;
            if($users){
                $friend_check=Friends::where('id',$friend)->first();
                if($friend_check){
                    if($friend_check->request_to == $users->id){
                       $friends=Friends::where('id',$friend)->update([
                        'follow_receiver' => 4
                         ]);  
                         if($friends){
                             $message="unfollowing Successfully";

                        }     
                        else{
                            $message="Something went wrong";
                        }    
                    }
                    else{
                        $message="Not requested";
                    }
                }
           else{
                $message="Not exist";
           }
        }
        else{
            $message = "Unauthorized";
           
        }
          return response()->json([
                'message' => $message
            ]);
        }

        public function changeAccount(Request $request){
            $users = auth()->user();
            if($users){
                $get_friends = User::where('id',$users->id)->update([
                    'account_type' => $request->account_type
                ]);

                $message="Successfully Updated";
            }
            else{
                $message = "Unauthorized"; 
            }
            return response()->json([
                    'message' => $message
                ]);
        }
          public function manageRequest(Request $request){
            $users = auth()->user();
            if($users){
                // if($request->status == 1){ //accepted
                //     $follow_receiver=0; //following and follower
                // }
                // else if($request->status == 0){ //declined
                //     $follow_receiver=0; //following
                // }
                // else{
                //      $follow_receiver=0;
                // }
                $get_friends = Friends::where('request_from',$request->request_from)->where('request_to',$request->request_to)->update([
                    'status' => $request->status,
                    'follow_receiver' => $request->follow_receiver
                ]);
                if($request->status=='1'){
                    $res_message="Request accepted";
                }
                elseif($res_message=='2'){
                     $res_message="Request send";
                }
                if($get_friends){ 
                    $notification_create=Notifications::create([
                        'user_id'=>$users->id,
                        'request_from'=>$request->request_from,
                        'request_to' =>$request->request_to,
                        'message'=>$res_message,
                        'app'=>0,
                        'status'=>1
                    ]);
                      $message="Successfully Updated";
                }
                else{
                    $message="SOmething went wrong";
                }
            }
            else{
                $message = "Unauthorized"; 
            }
            return response()->json([
                    'message' => $message
                ]);
        }

        public function getFollowings(){
            $users = auth()->user();
            if($users){
                $data=collect([]);
                if($users->account_type == 0){ // current user type is public no need of request acceptance
                    $friends=Friends::where('request_from',$users->id)->orWhere('request_to',$users->id)->get();
                    foreach ($friends as $friend) {
                        if($friend->request_from == $users->id){
                            if($friend->status != 4){
                                 if ($data->where('id', $friend->id)->count() === 0){
                                    $data->push($friend);
                                }     
                            }
                             
                        }
                    }
                       
                }else{// current user type is private so request send from the current user and need the acceptance to add in to following count 
                    $friends_lists=Friends::where('request_from',$users->id)->orWhere('request_to',$users->id)->get();
                    foreach ($friends_lists as  $friends_list) {
                       if($friends_list){
                            if($friends_list->request_from == $users->id){ // if send request from current user
                                $request_to=User::where('id',$friends_list->request_to)->first();
                                if($request_to){
                                    if($request_to->account_type == 0){ // if the request to is public
                                         if ($data->where('id', $friends_list->id)->count() === 0){
                                                $data->push($friends_list);
                                        }
                                    }
                                    else{
                                        if($friends_list->status == 1){
                                           if ($data->where('id', $friends_list->id)->count() === 0){

                                            } 
                                        }
                                        
                                    }
                                }
                                
                            }
                            else if($friends_list->request_to == $users->id){ // if get request from current user
                                $request_from=User::where('id',$friends_list->request_from)->first();
                                if($request_from){
                                    if($request_from->account_type == 0){ // if the request to is public
                                        if ($data->where('id', $friends_list->id)->count() === 0){
                                                $data->push($friends_list);
                                        }
                                    }
                                    else{
                                        if($friends_list->follow_receiver == 1){
                                           if ($data->where('id', $friends_list->id)->count() === 0){
                                                $data->push($friends_list);
                                            } 
                                        }
                                        
                                    }
                                    
                                }
                            }
                       }
                    }
                }
                $message ="sucess";
            }
            else{
                $friends_lists=collect([]);
                $message = "Unauthorized"; 
            }
            $following=count($data);
            return response()->json([
                'message' => $message,
                'following' => $following
            ]);
        }

         public function getFollowers(){
            $users = auth()->user();
            if($users){
                $data=collect([]);
                if($users->account_type == 0){ // current user type is public no need of request acceptance
                    // $data=Friends::where('request_from',$users->id)->orWhere('request_to',$users->id)->get();
                     $friends=Friends::where('request_from',$users->id)->orWhere('request_to',$users->id)->get();
                    foreach ($friends as $friend) {
                        if($friend->request_to== $users->id){
                            if($friend->follow_receiver != 4){
                                 if ($data->where('id', $friend->id)->count() === 0){
                                    $data->push($friend);
                                }     
                            }
                             
                        }
                    }
                }else{// current user type is private so request send from the current user and need the acceptance to add in to following count 
                    $friends_lists=Friends::where('request_from',$users->id)->orWhere('request_to',$users->id)->get();
                    foreach ($friends_lists as  $friends_list) {
                       if($friends_list){
                            if($friends_list->request_from == $users->id){ // if send request from current user
                                $request_to=User::where('id',$friends_list->request_to)->first();
                                if($request_to){
                                        if($friends_list->status == 1 &&  $friends_list->follow_receiver == 1){
                                           if ($data->where('id', $friends_list->id)->count() === 0){
                                                $data->push($friends_list);
                                            } 
                                        }
                                      
                                }
                                
                            }
                            else if($friends_list->request_to == $users->id){ // if get request from current user
                                $request_from=User::where('id',$friends_list->request_from)->first();
                                if($request_from){
                                        if($friends_list->status == 1 && $friends_list->follow_receiver == 1){
                                           if ($data->where('id', $friends_list->id)->count() === 0){
                                                $data->push($friends_list);
                                            } 
                                        }
                                }
                            }
                       }
                    }
                }
                $message ="sucess";
            }
            else{
                $friends_lists=collect([]);
                $message = "Unauthorized"; 
            }
            $following=count($data);
            return response()->json([
                'message' => $message,
                'following' => $following
            ]);
        }

        public function listSavePost(Request $request){
            $users = auth()->user();
            if($users){
                 $save_post_exist=Savedpost::with('post','mood')->where('user_id',$users->id)->get();
                 return response()->json([
                    'success' => 'True',
                    'data' => $save_post_exist,
                    'user'=>$users
                    ]);
                         
            }
            else{
                $message = "Unauthorized"; 
                return response()->json([
                 'success' => $message,
                ]);
            }
           
        }
         public function getFollowingsList(){
            $users = auth()->user();
            if($users){
                $data=collect([]);
                if($users->account_type == 0){ // current user type is public no need of request acceptance
                    $datas=Friends::with('requestFrom','requestTo')->where('request_from',$users->id)->orWhere('request_to',$users->id)->get();
                     foreach ($datas as  $friend_list) {
                        if($friend_list->request_from == $users->id){ 
                           // $data->push($friend_list);
                            $friend_list->setAttribute('user_id', $friend_list->requestTo['id']);
                            $friend_list->setAttribute('full_name', $friend_list->requestTo['full_name']);
                            $friend_list->setAttribute('image', $friend_list->requestTo['image']);
                            $friend_list->setAttribute('email', $friend_list->requestTo['email']);
                            $friend_list->setAttribute('mobile_number', $friend_list->requestTo['mobile_number']);
                            $friend_list->setAttribute('dob', $friend_list->requestTo['dob']);
                            $friend_list->setAttribute('provider_id', $friend_list->requestTo['provider_id']);
                            $friend_list->setAttribute('gender', $friend_list->requestTo['gender']);
                            $friend_list->setAttribute('address', $friend_list->requestTo['address']);
                            $friend_list->setAttribute('zodiac', $friend_list->requestTo['zodiac']);
                            $friend_list->setAttribute('role_id', $friend_list->requestTo['role_id']);
                            $friend_list->setAttribute('status', $friend_list->requestTo['status']);
                            $friend_list->setAttribute('otp_verified', $friend_list->requestTo['otp_verified ']);
                            $friend_list->setAttribute('report_count', $friend_list->requestTo['report_count']);
                            $friend_list->setAttribute('account_type', $friend_list->requestTo['account_type']);

                            $relations = $friend_list->getRelations();

                            unset($friend_list['request_to']);
                            unset($friend_list->requestFrom);
                            unset($friend_list['request_from']);
                            unset($friend_list['follow_receiver']);
                            unset($friend_list->requestTo);
                            unset($friend_list['request_to']);
                           // print_r($friend_list->requestFrom['full_name']);

                            // $friend_list->setAttribute('ids', '1');
                             if ($data->where('id', $friend_list->id)->count() === 0){
                                $data->push($friend_list);
                             }  
                         }
                         else{
                             $friend_list->setAttribute('user_id', $friend_list->requestFrom['id']);
                            $friend_list->setAttribute('full_name', $friend_list->requestFrom['full_name']);
                            $friend_list->setAttribute('image', $friend_list->requestFrom['image']);
                            $friend_list->setAttribute('email', $friend_list->requestFrom['email']);
                            $friend_list->setAttribute('mobile_number', $friend_list->requestFrom['mobile_number']);
                            $friend_list->setAttribute('dob', $friend_list->requestFrom['dob']);
                            $friend_list->setAttribute('provider_id', $friend_list->requestFrom['provider_id']);
                            $friend_list->setAttribute('gender', $friend_list->requestFrom['gender']);
                            $friend_list->setAttribute('address', $friend_list->requestFrom['address']);
                            $friend_list->setAttribute('zodiac', $friend_list->requestFrom['zodiac']);
                            $friend_list->setAttribute('role_id', $friend_list->requestFrom['role_id']);
                            $friend_list->setAttribute('status', $friend_list->requestFrom['status']);
                            $friend_list->setAttribute('otp_verified', $friend_list->requestFrom['otp_verified ']);
                            $friend_list->setAttribute('report_count', $friend_list->requestFrom['report_count']);
                            $friend_list->setAttribute('account_type', $friend_list->requestFrom['account_type']);

                            $relations = $friend_list->getRelations();

                            unset($friend_list['request_to']);
                            unset($friend_list->requestFrom);
                            unset($friend_list['request_from']);
                            unset($friend_list['follow_receiver']);
                            unset($friend_list->requestTo);
                            unset($friend_list['request_to']);
                           // print_r($friend_list->requestFrom['full_name']);

                            // $friend_list->setAttribute('ids', '1');
                             if ($data->where('id', $friend_list->id)->count() === 0){
                                $data->push($friend_list);
                             } 
                         }
                     }
                }else{// current user type is private so request send from the current user and need the acceptance to add in to following count 
                    $friends_lists=Friends::with('requestFrom','requestTo')->where('request_from',$users->id)->orWhere('request_to',$users->id)->get();
                    foreach ($friends_lists as  $friend_list) {
                       if($friend_list){
                            if($friend_list->request_from == $users->id){ // if send request from current user
                                $request_to=User::where('id',$friend_list->request_to)->first();
                                if($request_to){
                                    if($request_to->account_type == 0){
                                       // print_r('inside of 0');
                                        $friend_list->setAttribute('user_id', $friend_list->requestTo['id']);
                                                $friend_list->setAttribute('full_name', $friend_list->requestTo['full_name']);
                                                $friend_list->setAttribute('image', $friend_list->requestTo['image']);
                                                $friend_list->setAttribute('email', $friend_list->requestTo['email']);
                                                $friend_list->setAttribute('mobile_number', $friend_list->requestTo['mobile_number']);
                                                $friend_list->setAttribute('dob', $friend_list->requestTo['dob']);
                                                $friend_list->setAttribute('provider_id', $friend_list->requestTo['provider_id']);
                                                $friend_list->setAttribute('gender', $friend_list->requestTo['gender']);
                                                $friend_list->setAttribute('address', $friend_list->requestTo['address']);
                                                $friend_list->setAttribute('zodiac', $friend_list->requestTo['zodiac']);
                                                $friend_list->setAttribute('role_id', $friend_list->requestTo['role_id']);
                                                $friend_list->setAttribute('status', $friend_list->requestTo['status']);
                                                $friend_list->setAttribute('otp_verified', $friend_list->requestTo['otp_verified ']);
                                                $friend_list->setAttribute('report_count', $friend_list->requestTo['report_count']);
                                                $friend_list->setAttribute('account_type', $friend_list->requestTo['account_type']);

                                                $relations = $friend_list->getRelations();

                                                unset($friend_list['request_to']);
                                                unset($friend_list->requestFrom);
                                                unset($friend_list['request_from']);
                                                unset($friend_list['follow_receiver']);
                                                unset($friend_list->requestTo);
                                                unset($friend_list['request_to']);
                                               // print_r($friend_list->requestFrom['full_name']);

                                                // $friend_list->setAttribute('ids', '1');
                                                 if ($data->where('id', $friend_list->id)->count() === 0){
                                                    $data->push($friend_list);
                                                 }  
                                     // if the request to is public
                                        //  if ($data->where('id', $friend_list->id)->count() === 0){
                                        //         $data->push($friend_list);
                                        // }
                                    }
                                    else{
                                        if($friend_list->status == 1){
                                           if ($data->where('id', $friend_list->id)->count() === 0){

                                            } 
                                        }
                                        
                                    }
                                }
                                
                            }
                            else if($friend_list->request_to == $users->id){ // if get request from current user
                                $request_from=User::where('id',$friend_list->request_from)->first();
                                if($request_from){
                                    if($request_from->account_type == 0){ // if the request to is public
                                        $friend_list->setAttribute('user_id', $friend_list->requestFrom['id']);
                                        $friend_list->setAttribute('full_name', $friend_list->requestFrom['full_name']);
                                        $friend_list->setAttribute('image', $friend_list->requestFrom['image']);
                                        $friend_list->setAttribute('email', $friend_list->requestFrom['email']);
                                        $friend_list->setAttribute('mobile_number', $friend_list->requestFrom['mobile_number']);
                                        $friend_list->setAttribute('dob', $friend_list->requestFrom['dob']);
                                        $friend_list->setAttribute('provider_id', $friend_list->requestFrom['provider_id']);
                                        $friend_list->setAttribute('gender', $friend_list->requestFrom['gender']);
                                        $friend_list->setAttribute('address', $friend_list->requestFrom['address']);
                                        $friend_list->setAttribute('zodiac', $friend_list->requestFrom['zodiac']);
                                        $friend_list->setAttribute('role_id', $friend_list->requestFrom['role_id']);
                                        $friend_list->setAttribute('status', $friend_list->requestFrom['status']);
                                        $friend_list->setAttribute('otp_verified', $friend_list->requestFrom['otp_verified ']);
                                        $friend_list->setAttribute('report_count', $friend_list->requestFrom['report_count']);
                                        $friend_list->setAttribute('account_type', $friend_list->requestFrom['account_type']);

                                        $relations = $friend_list->getRelations();

                                        unset($friend_list['request_to']);
                                        unset($friend_list->requestFrom);
                                        unset($friend_list['request_from']);
                                        unset($friend_list['follow_receiver']);
                                        unset($friend_list->requestTo);
                                        unset($friend_list['request_to']);
                                       // print_r($friend_list->requestFrom['full_name']);

                                        // $friend_list->setAttribute('ids', '1');
                                         if ($data->where('id', $friend_list->id)->count() === 0){
                                            $data->push($friend_list);
                                         } 
                                        // if ($data->where('id', $friend_list->id)->count() === 0){
                                        //         $data->push($friend_list);
                                        // }
                                    }
                                    else{
                                        if($friend_list->follow_receiver == 1){
                                            $friend_list->setAttribute('user_id', $friend_list->requestFrom['id']);
                                            $friend_list->setAttribute('full_name', $friend_list->requestFrom['full_name']);
                                            $friend_list->setAttribute('image', $friend_list->requestFrom['image']);
                                            $friend_list->setAttribute('email', $friend_list->requestFrom['email']);
                                            $friend_list->setAttribute('mobile_number', $friend_list->requestFrom['mobile_number']);
                                            $friend_list->setAttribute('dob', $friend_list->requestFrom['dob']);
                                            $friend_list->setAttribute('provider_id', $friend_list->requestFrom['provider_id']);
                                            $friend_list->setAttribute('gender', $friend_list->requestFrom['gender']);
                                            $friend_list->setAttribute('address', $friend_list->requestFrom['address']);
                                            $friend_list->setAttribute('zodiac', $friend_list->requestFrom['zodiac']);
                                            $friend_list->setAttribute('role_id', $friend_list->requestFrom['role_id']);
                                            $friend_list->setAttribute('status', $friend_list->requestFrom['status']);
                                            $friend_list->setAttribute('otp_verified', $friend_list->requestFrom['otp_verified ']);
                                            $friend_list->setAttribute('report_count', $friend_list->requestFrom['report_count']);
                                            $friend_list->setAttribute('account_type', $friend_list->requestFrom['account_type']);

                                            $relations = $friend_list->getRelations();

                                            unset($friend_list['request_to']);
                                            unset($friend_list->requestFrom);
                                            unset($friend_list['request_from']);
                                            unset($friend_list['follow_receiver']);
                                            unset($friend_list->requestTo);
                                            unset($friend_list['request_to']);
                                           // print_r($friend_list->requestFrom['full_name']);

                                            // $friend_list->setAttribute('ids', '1');
                                             if ($data->where('id', $friend_list->id)->count() === 0){
                                                $data->push($friend_list);
                                             } 
                                           // if ($data->where('id', $friend_list->id)->count() === 0){
                                           //      $data->push($friend_list);
                                           //  } 
                                        }
                                        
                                    }
                                    
                                }
                            }
                       }
                    }
                }
                $message ="sucess";
            }
            else{
                $friends_lists=collect([]);
                $message = "Unauthorized"; 
            }
            // $following=count($data);
            return response()->json([
                'message' => $message,
                'following' => $data
            ]);
        }

         public function getFollowersList(){
            $users = auth()->user();
            if($users){
                $data=collect([]);
                if($users->account_type == 0){ // current user type is public no need of request acceptance
                    $data=Friends::with('requestFrom','requestTo')->where('request_from',$users->id)->orWhere('request_to',$users->id)->get();
                     foreach($data as  $friend_list) {
                         if($friend_list->request_from == $users->id){ 
                           // $data->push($friend_list);
                            $friend_list->setAttribute('user_id', $friend_list->requestTo['id']);
                            $friend_list->setAttribute('full_name', $friend_list->requestTo['full_name']);
                            $friend_list->setAttribute('image', $friend_list->requestTo['image']);
                            $friend_list->setAttribute('email', $friend_list->requestTo['email']);
                            $friend_list->setAttribute('mobile_number', $friend_list->requestTo['mobile_number']);
                            $friend_list->setAttribute('dob', $friend_list->requestTo['dob']);
                            $friend_list->setAttribute('provider_id', $friend_list->requestTo['provider_id']);
                            $friend_list->setAttribute('gender', $friend_list->requestTo['gender']);
                            $friend_list->setAttribute('address', $friend_list->requestTo['address']);
                            $friend_list->setAttribute('zodiac', $friend_list->requestTo['zodiac']);
                            $friend_list->setAttribute('role_id', $friend_list->requestTo['role_id']);
                            $friend_list->setAttribute('status', $friend_list->requestTo['status']);
                            $friend_list->setAttribute('otp_verified', $friend_list->requestTo['otp_verified ']);
                            $friend_list->setAttribute('report_count', $friend_list->requestTo['report_count']);
                            $friend_list->setAttribute('account_type', $friend_list->requestTo['account_type']);

                            $relations = $friend_list->getRelations();

                            unset($friend_list['request_to']);
                            unset($friend_list->requestFrom);
                            unset($friend_list['request_from']);
                            unset($friend_list['follow_receiver']);
                            unset($friend_list->requestTo);
                            unset($friend_list['request_to']);
                           // print_r($friend_list->requestFrom['full_name']);

                            // $friend_list->setAttribute('ids', '1');
                             if ($data->where('id', $friend_list->id)->count() === 0){
                                $data->push($friend_list);
                             }  
                         }
                         else{
                             $friend_list->setAttribute('user_id', $friend_list->requestFrom['id']);
                            $friend_list->setAttribute('full_name', $friend_list->requestFrom['full_name']);
                            $friend_list->setAttribute('image', $friend_list->requestFrom['image']);
                            $friend_list->setAttribute('email', $friend_list->requestFrom['email']);
                            $friend_list->setAttribute('mobile_number', $friend_list->requestFrom['mobile_number']);
                            $friend_list->setAttribute('dob', $friend_list->requestFrom['dob']);
                            $friend_list->setAttribute('provider_id', $friend_list->requestFrom['provider_id']);
                            $friend_list->setAttribute('gender', $friend_list->requestFrom['gender']);
                            $friend_list->setAttribute('address', $friend_list->requestFrom['address']);
                            $friend_list->setAttribute('zodiac', $friend_list->requestFrom['zodiac']);
                            $friend_list->setAttribute('role_id', $friend_list->requestFrom['role_id']);
                            $friend_list->setAttribute('status', $friend_list->requestFrom['status']);
                            $friend_list->setAttribute('otp_verified', $friend_list->requestFrom['otp_verified ']);
                            $friend_list->setAttribute('report_count', $friend_list->requestFrom['report_count']);
                            $friend_list->setAttribute('account_type', $friend_list->requestFrom['account_type']);

                            $relations = $friend_list->getRelations();

                            unset($friend_list['request_to']);
                            unset($friend_list->requestFrom);
                            unset($friend_list['request_from']);
                            unset($friend_list['follow_receiver']);
                            unset($friend_list->requestTo);
                            unset($friend_list['request_to']);
                           // print_r($friend_list->requestFrom['full_name']);

                            // $friend_list->setAttribute('ids', '1');
                             if ($data->where('id', $friend_list->id)->count() === 0){
                                $data->push($friend_list);
                             } 
                         }
                }
                }else{// current user type is private so request send from the current user and need the acceptance to add in to following count 
                    $friends_lists=Friends::with('requestFrom','requestTo')->where('request_from',$users->id)->orWhere('request_to',$users->id)->get();
                    foreach ($friends_lists as  $friend_list) {
                       if($friend_list){
                            if($friend_list->request_from == $users->id){ // if send request from current user
                                $request_to=User::where('id',$friend_list->request_to)->first();
                                if($request_to){
                                        if($friend_list->status == 1 &&  $friend_list->follow_receiver == 1){
                                              $friend_list->setAttribute('user_id', $friend_list->requestTo['id']);
                                                $friend_list->setAttribute('full_name', $friend_list->requestTo['full_name']);
                                                $friend_list->setAttribute('image', $friend_list->requestTo['image']);
                                                $friend_list->setAttribute('email', $friend_list->requestTo['email']);
                                                $friend_list->setAttribute('mobile_number', $friend_list->requestTo['mobile_number']);
                                                $friend_list->setAttribute('dob', $friend_list->requestTo['dob']);
                                                $friend_list->setAttribute('provider_id', $friend_list->requestTo['provider_id']);
                                                $friend_list->setAttribute('gender', $friend_list->requestTo['gender']);
                                                $friend_list->setAttribute('address', $friend_list->requestTo['address']);
                                                $friend_list->setAttribute('zodiac', $friend_list->requestTo['zodiac']);
                                                $friend_list->setAttribute('role_id', $friend_list->requestTo['role_id']);
                                                $friend_list->setAttribute('status', $friend_list->requestTo['status']);
                                                $friend_list->setAttribute('otp_verified', $friend_list->requestTo['otp_verified ']);
                                                $friend_list->setAttribute('report_count', $friend_list->requestTo['report_count']);
                                                $friend_list->setAttribute('account_type', $friend_list->requestTo['account_type']);

                                                $relations = $friend_list->getRelations();

                                                unset($friend_list['request_to']);
                                                unset($friend_list->requestFrom);
                                                unset($friend_list->requestTo);
                                                unset($friend_list['request_to']);

                                                unset($friend_list['request_from']);
                                                unset($friend_list['follow_receiver']);
                                               //print_r($friend_list->requestFrom['full_name']);
                                           if ($data->where('id', $friend_list->id)->count() === 0){

                                                $data->push($friend_list);
                                            } 
                                        }
                                      
                                }
                                
                            }
                            else if($friend_list->request_to == $users->id){ // if get request from current user
                                $request_from=User::where('id',$friend_list->request_from)->first();
                                if($request_from){
                                        if($friend_list->status == 1 && $friend_list->follow_receiver == 1){
                                            $friend_list->setAttribute('user_id', $friend_list->requestFrom['id']);
                                                $friend_list->setAttribute('full_name', $friend_list->requestFrom['full_name']);
                                                $friend_list->setAttribute('image', $friend_list->requestFrom['image']);
                                                $friend_list->setAttribute('email', $friend_list->requestFrom['email']);
                                                $friend_list->setAttribute('mobile_number', $friend_list->requestFrom['mobile_number']);
                                                $friend_list->setAttribute('dob', $friend_list->requestFrom['dob']);
                                                $friend_list->setAttribute('provider_id', $friend_list->requestFrom['provider_id']);
                                                $friend_list->setAttribute('gender', $friend_list->requestFrom['gender']);
                                                $friend_list->setAttribute('address', $friend_list->requestFrom['address']);
                                                $friend_list->setAttribute('zodiac', $friend_list->requestFrom['zodiac']);
                                                $friend_list->setAttribute('role_id', $friend_list->requestFrom['role_id']);
                                                $friend_list->setAttribute('status', $friend_list->requestFrom['status']);
                                                $friend_list->setAttribute('otp_verified', $friend_list->requestFrom['otp_verified ']);
                                                $friend_list->setAttribute('report_count', $friend_list->requestFrom['report_count']);
                                                $friend_list->setAttribute('account_type', $friend_list->requestFrom['account_type']);

                                                $relations = $friend_list->getRelations();

                                                unset($friend_list['request_to']);
                                                unset($friend_list->requestFrom);
                                                unset($friend_list['request_from']);
                                                unset($friend_list['follow_receiver']);
                                                unset($friend_list->requestTo);
                                                unset($friend_list['request_to']);
                                           if ($data->where('id', $friend_list->id)->count() === 0){
                                                $data->push($friend_list);
                                            } 
                                        }
                                    
                                }
                            }
                       }
                    }
                }
                $message ="sucess";
            }
            else{
                $friends_lists=collect([]);
                $message = "Unauthorized"; 
            }
            // $following=count($data);
            return response()->json([
                'message' => $message,
                'following' => $data
            ]);
        }

        // public function listSavePost(Request $request){
        //     $users = auth()->user();
        //     if($users){
        //          $save_post_exist=Savedpost::with('post')->where('user_id',$users->id)->first();
        //          return response()->json([
        //             'success' => 'True',
        //             'data' => $save_post_exist,
        //             'user'=>$users
        //             ]);
                         
        //     }
        //     else{
        //         $message = "Unauthorized"; 
        //         return response()->json([
        //          'success' => $message,
        //         ]);
        //     }
           
        // }

    
   public function savePost(Request $request){
        $users = auth()->user();
        $input['post_id'] = $request->post_id;
        $rules = array(
            'post_id' => 'required',
           );
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $message ="Please fill the required fields";
        }
        else{
            if($users){
                $save_post_exist=Savedpost::where('post_id',$request->post_id)->where('user_id',$users->id)->first();
                if($save_post_exist){
                    $message = "Already save this post";
                }else{
                    $post=Post::where('id',$request->post_id)->first();
                   // dd($post);
                    $savepost=Savedpost::create([
                        'post_id'=>$request->post_id,
                        'user_id'=>$users->id,
                        'mood_id'=>$post->mood_id              
                        ]);
                    if($savepost){
                         $notification_create=Notifications::create([
                            'user_id'=>$users->id,
                            'request_from'=>$request->request_from,
                            'request_to' =>$request->request_to,
                            'message'=>'Friend request send',
                            'app'=>0,
                            'status'=>1
                        ]);
                        $message = "Successfully saved the post"; 
                    }
                    else{
                        $message="Error while saving post";
                    }
                }
            }
            else{
                $message = "Unauthorized"; 
            }
               
        } 
            return response()->json([
                'message' => $message,
            ]);
        }

     public function reportPost(Request $request){
        $users = auth()->user();
        $input['post_id'] = $request->post_id;
        $input['reason'] = $request->reason;
        $rules = array(
                'post_id' => 'required',
                'reason' => 'required',
               );
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $message ="Please fill the required fields";
        }
        else{
            if($users){
                $get_reported=Reportpost::where('user_id',$users->id)->where('post_id',$request->post_id)->where('verified',1)->first();
                if($get_reported){
                    $message="This post was already reported";
                }else{
                    $reportpost=Reportpost::create([
                    'post_id'=>$request->post_id,
                    'user_id'=>$users->id,
                    'reason'=>$request->reason
                    ]);

                    $get_report_post=Post::where('id',$request->post_id)->first();
                    $blk_user_id=$get_report_post->user_id;
                    $count_exist=User::where('id',$blk_user_id)->first();
                    $count_exist_repored=$count_exist->report_count;
                    $settings=Settings::first();
                    $db_count=$settings->report_counts;;
                    if($count_exist_repored>$db_count){
                       $blk_user= User::where('id',$blk_user_id)->update([
                        'status'=>2,
                        ]);
                    }
                    if($reportpost){
                         $blk_user= User::where('id',$blk_user_id)->update([
                            'report_count'=>$count_exist_repored+1
                        ]);
                        $blk_post=Post::where('id',$request->post_id)->update([
                            'status'=>2
                        ]);
                        $message = "Successfully repoted the post"; 
                    }
                    else{
                        $message="Error while saving reporting";
                    }
                }
                 
             }
             else{
              $message = "Unauthorized"; 
            }
         }
         
    return response()->json([
        'message' => $message
    ]);
  }
   public function reported_otp_verify(Request $request){
        $email=$request->email;
        $otp=$request->otp;
        $user=User::where('email',$email)->first();
        $otp_data=Reportactivation::where('user_id',$user->id)->first();
           if($otp == $otp_data->otp){
                $user_update=User::where('email',$email)->update([
                    'status'=>1,
                    'report_count'=>0
                ]);
                if($user_update){
                    $r_post=reportPost::where('user_id',$user->id)->get();
                    foreach ($r_post as $value) {
                        // dd($value);
                       Reportpost::where('id',$value->id)->update([
                        'verified'=>1
                       ]);
                    }
                    $message='OTP Successfully verified';
                }
                else{
                    $message='OTP Verification failed';
                }
            }
            else{
               $message='OTP Mismatch';
            } 
        return response()->json([
            'message' => $message,
        ], 201);
    }
     public function otp_reactivation(Request $request){
        $fourdigitrandom = rand(1000,9999); 
        $user_details=User::where('email',$request->email)->first();
        $otp=Reportactivation::where('user_id',$user_details->id)->update([
            'otp'=>$fourdigitrandom,
            'user_id'=>$user_details->id
         ]);
        if($otp){
            Mail::send('email.success', ['request' => $request,'fourdigitrandom'=>$fourdigitrandom], function ($m) use ($request,$fourdigitrandom) {
            $m->from('hello@app.com', 'Dreamchat ');
            $m->to($request->email, $request->full_name)->subject(' Dreamchat One time password for activation');

            });
            $message="OTP was regenerated! please check email for verification code";
        }
       else{

            $message="Cannot able to generate OTP,Try again ";
       }
        return response()->json([
            'message' => $message,
        ], 201);
    }
     public function post($page = 1){
        $users = auth()->user();
         if($users){
           $data = collect([]);
            $get_friends = Friends::with('requestFrom','requestTo')->where('request_from',$users->id)->orWhere('request_to',$users->id)->where('status',1)->get();
            if($get_friends){
                foreach ($get_friends as $get_friend) {
                    $friends_lists=Friends::with('requestFrom','requestTo')->where('request_from',$users->id)->orWhere('request_to',$users->id)->get();
                    foreach ($friends_lists as  $friends_list) {
                       if($friends_list){
                            if($friends_list->status == 1 &&  $friends_list->follow_receiver == 1){
                               // if ($data->where('id', $friends_list->id)->count() === 0){
                               //      $data->push($friends_list);
                               //  } 
                                if($users->id==$friends_list->request_from){
                                    $user_id=$friends_list->request_to;
                                }
                                else{
                                $user_id=$friends_list->request_from;

                                }
                                 $posts=Post::with('user','mood','shared_user')->where('user_id',$user_id)->where('status',1)->get();
                                    if($posts){
                                        foreach ($posts as $post) {
                                            if ($data->where('id', $post->id)->count() === 0){
                                                $data->push($post);
                                            }  
                                         }
                                    }
                                    else{
                                        $data=$data;
                                    }
                            }
                        }           
                    }
                }
            return response()->json([
                'success' => 'True',
                'data' => $data,
              ]);
            }
            else{
                  return response()->json([
                'success' => 'Nothing to show',
            ]);
            }
         }
         else{
             $message = "Unauthorized";
             return response()->json([
                    'message' => $message
                ]);
         }
         
        }
   
}