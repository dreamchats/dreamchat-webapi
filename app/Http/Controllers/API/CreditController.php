<?php

namespace App\Http\Controllers\API;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use App\CustomerCredits;
use App\Credits;
class CreditController extends Controller
{
	 public function creditPurchase(Request $request){
         $users = auth()->user();
         if($users){
            $input['credit_id'] = $request->credit_id;
            $rules = array(
                'credit_id' => 'required',
               );
            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                $message ="Please fill the required fields";
            }
            else{

                $credits=Credits::where('id',$request->credit_id)->first();
                if($credits){
                    $check_credit=CustomerCredits::where('user_id',$users->id)->first();
                    if($check_credit){
                        $remains_credit=$check_credit->remains_credit+$credits->credits_points;
                    }
                    else
                    {
                        $remains_credit=$credits->credits_points;
                    }
                        $customer_purchase=CustomerCredits::create([
                        'credits'=> $credits->credits_points,
                        'credit_id'=>$credits->id,
                        'user_id'=>$users->id,
                        'remains_credit'=>$remains_credit
                  ]);
                    if($customer_purchase){
                        $exist_credit=CustomerCredits::where('user_id',$users->id)->get();
                        foreach ($exist_credit as  $credit) {
                           $remains_update=CustomerCredits::where('id',$credit->id)->update([
                            'remains_credit'=>$remains_credit
                           ]);
                        }
                        $message="Credits added";
 
                    }
                    else{
                        $message="something went wrong";
                    }
                }
                else{
                    $message="Credits not exist";

                }
            }
        }else{
            $message="unauthorized";
        }
         return response()->json([
               'success' => $message,

         ]);
    }
      public function getCredits(){
        $users = auth()->user(); 
        if($users){
            $getcreditpoints = CustomerCredits::where('user_id',$users->id)->first();
            if($getcreditpoints){
                $creditpoints=$getcreditpoints->remains_credit;
            }else{
                $creditpoints=0;
            }
            return response()->json([
             'success' => 'True',
                'data' => $creditpoints
            ]);
        } 
        else{
            $message = "Unauthorized";
             return response()->json([
             'success' => $message,
            ]);
        }
    }
     public function listCredits(){
        $users = auth()->user(); 
        if($users){
            $getcreditpoints = Credits::where('status',1)->get();
            
            return response()->json([
             'success' => 'True',
                'data' => $getcreditpoints
            ]);
        } 
        else{
            $message = "Unauthorized";
             return response()->json([
             'success' => $message,
            ]);
        }
    }
}