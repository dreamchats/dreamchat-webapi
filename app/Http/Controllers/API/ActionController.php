<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\LikePost;
use App\SharePost;
use App\Post;
use App\LikesComment;
use App\Comments;
use App\Notifications;
use App\Cms;
use App\Mood;
use App\Savedpost;
class ActionController extends Controller{

	public function likes(Request $request){
	 $users = auth()->user();
	 if($users){
	 	$input['post_id'] = $request->post_id;
	 	$rules = array(
		    'post_id' => 'required',
		   );
		$validator = Validator::make($input, $rules);
		if ($validator->fails()) {
            $message ="Please fill the required fields";
        }
        else{
	 		$likes=LikePost::where('post_id',$request->post_id)->where('status',1)->count();
	 		 return response()->json([
			       'success' => 'True',
		           'data' => $likes
			 ]);
	 	}
	 }
	 else{
        $message = "Unauthorized";
       }
	   return response()->json([
	        'message' => $message
	    ]);
	}
	public function addLikes(Request $request){
	 $users = auth()->user();
	 if($users){
	 	$input['post_id'] = $request->post_id;
	 	$rules = array(
		    'post_id' => 'required',
		   );
		$validator = Validator::make($input, $rules);
		if ($validator->fails()) {
            $message ="Please fill the required fields";
        }
        else{
	 		$likes=LikePost::where('post_id',$request->post_id)->where('user_id',$users->id)->where('status',1)->first();
	 		if($likes){
	 			$message="Already given llike for this post";
	 		}
	 		else{
	 			$existslikes=LikePost::where('post_id',$request->post_id)->where('user_id',$users->id)->where('status',0)->first();
	 			if($existslikes){
	 				$likes=LikePost::where('id',$existslikes->id)->update([
	 					'status'=>1
	 				]);
	 			}else{
	 				$likes=LikePost::create([
	 				'user_id'=>$users->id,
	 				'post_id'=>$request->post_id
	 			]);
	 			}
	 		
	 			if($likes){
	 				$notification_create=Notifications::create([
                        'user_id'=>$users->id,
                        'request_from'=>$users->id,
                        'request_to' =>0,
                        'message'=>'like added',
                        'post_id'=>$request->post_id,
                        'app'=>1,
                        'status'=>1
                    ]);
	 				$message="Successfully like added";
	 			}
	 			else{
	 				$message="Something went wrong";

	 			}
	 		}
	 		 return response()->json([
			       'success' => $message,

			 ]);
	 	}
	 }
	 else{
        $message = "Unauthorized";
       }
	   return response()->json([
	        'message' => $message
	    ]);
	}

	public function checkLikes(Request $request){
	 $users = auth()->user();
	 if($users){
	 	$input['post_id'] = $request->post_id;
	 	$rules = array(
		    'post_id' => 'required',
		   );
		$validator = Validator::make($input, $rules);
		if ($validator->fails()) {
            $message ="Please fill the required fields";
        }
        else{
	 		$likes=LikePost::where('post_id',$request->post_id)->where('user_id',$users->id)->where('status',1)->first();
	 	}
	 }
	 else{
        $message = "Unauthorized";
       }
	   return response()->json([
	        'likes' => $likes
	    ]);
	}


	public function unLikes(Request $request){
		 $users = auth()->user();
		 if($users){
		 	$input['post_id'] = $request->post_id;
		 	$rules = array(
			    'post_id' => 'required',
			   );
			$validator = Validator::make($input, $rules);
			if ($validator->fails()) {
	            $message ="Please fill the required fields";
	        }
	        else{
		 		$likes=LikePost::where('post_id',$request->post_id)->where('user_id',$users->id)->where('status',0)->first();
		 		if($likes){
		 			$message="Already given unliked for this post";
		 		}
		 		else{
		 			$likes=LikePost::where('post_id',$request->post_id)->where('user_id',$users->id)->where('status',1)->first();
		 			if($likes){
		 				$unlike=LikePost::where('id',$likes->id)->update([
		 					'status'=>0
		 				]);
		 				if($unlike){
		 					$message="Unlike Success";
		 				}
		 				else{
		 					$message="Error on unlike";
		 				}
		 			}else{
		 				$message="Not yet given like for this post";
		 			}
		 		}
		 		 return response()->json([
				       'success' => $message,

				 ]);
		 	}
		 }
		 else{
	        $message = "Unauthorized";
	       }
		   return response()->json([
		        'message' => $message
		    ]);
		}

	public function commentLikes(Request $request){
	 $users = auth()->user();
	 if($users){
	 	$input['comment_id'] = $request->comment_id;
	 	$rules = array(
		    'comment_id' => 'required',
		   );
		$validator = Validator::make($input, $rules);
		if ($validator->fails()) {
            $message ="Please fill the required fields";
        }
        else{
	 		$likes=LikesComment::where('comment_id',$request->comment_id)->where('status',1)->count();
	 		 return response()->json([
			       'success' => 'True',
		           'data' => $likes
			 ]);
	 	}
	 }
	 else{
        $message = "Unauthorized";
       }
	   return response()->json([
	        'message' => $message
	    ]);
	}
	public function addCommentLikes(Request $request){
	 $users = auth()->user();
	 if($users){
	 	$input['comment_id'] = $request->comment_id;
	 	$rules = array(
		    'comment_id' => 'required',
		   );
		$validator = Validator::make($input, $rules);
		if ($validator->fails()) {
            $message ="Please fill the required fields";
        }
        else{
        	$comment=Comments::where('id',$request->comment_id)->get();
        	if($comment){
        		$likes=LikesComment::where('comment_id',$request->comment_id)->where('user_id',$users->id)->where('status',1)->first();
		 		if($likes){
		 			$message="Already given llike for this Comment";
		 		}
		 		else{
		 			$unlikes=LikesComment::where('comment_id',$request->comment_id)->where('user_id',$users->id)->where('status',0)->first();
		 				if($unlikes){
		 					$like=LikesComment::where('id',$unlikes->id)->update([
			 					'status'=>1
			 				]);
			 				if($like){
					 			$message="Successfully like added";
				 			}
				 			else{
				 				$message="Something went wrong";

			 			}
		 				}
		 				else{
		 					$likes=LikesComment::create([
				 				'user_id'=>$users->id,
				 				'comment_id'=>$request->comment_id
				 			]);
				 			if($likes){
				 				$notification_create=Notifications::create([
			                        'user_id'=>$users->id,
			                        'request_from'=>$users->id,
			                        'request_to' =>0,
			                        'message'=>'like added',
			                        'comment_id'=>$request->comment_id,
			                        'app'=>1,
			                        'status'=>1
			                    ]);
					 				$message="Successfully like added";
					 			}
					 			else{
					 				$message="Something went wrong";

				 			}
		 				}
			 			

		    	}
		 	}
		 	else{
		 		$message="Comment not exist";
		 	}
			return response()->json([
	       'success' => $message,
			 ]);
	 	}
	 }
	 else{
        $message = "Unauthorized";
       }
	   return response()->json([
	        'message' => $message
	    ]);
	}


	public function unCommentLikes(Request $request){
		 $users = auth()->user();
		 if($users){
		 	$input['comment_id'] = $request->comment_id;
		 	$rules = array(
			    'comment_id' => 'required',
			   );
			$validator = Validator::make($input, $rules);
			if ($validator->fails()) {
	            $message ="Please fill the required fields";
	        }
	        else{
		 		$likes=LikesComment::where('comment_id',$request->comment_id)->where('user_id',$users->id)->where('status',0)->first();
		 		if($likes){
		 			$message="Already given unliked for this Comment";
		 		}
		 		else{
		 			$likes=LikesComment::where('comment_id',$request->comment_id)->where('user_id',$users->id)->where('status',1)->first();
		 			if($likes){
		 				$unlike=LikesComment::where('id',$likes->id)->update([
		 					'status'=>0
		 				]);
		 				if($unlike){
		 					$message="Unlike Success";
		 				}
		 				else{
		 					$message="Error on unlike";
		 				}
		 			}else{
		 				$message="Not yet given like for this post";
		 			}
		 		}
		 		 return response()->json([
				       'success' => $message,

				 ]);
		 	}
		 }
		 else{
	        $message = "Unauthorized";
	       }
		   return response()->json([
		        'message' => $message
		    ]);
	}

	public function sharePost(Request $request){
	 $users = auth()->user();
	 if($users){
	 	$input['post_id'] = $request->post_id;
	 	$rules = array(
		    'post_id' => 'required',
		   );
		$validator = Validator::make($input, $rules);
		if ($validator->fails()) {
            $message ="Please fill the required fields";
        }
        else{
	 			$likes=SharePost::create([
	 				'user_id'=>$users->id,
	 				'post_id'=>$request->post_id
	 			]);
	 			if($likes){
	 				$post=Post::where('id',$request->post_id)->first();
	 				$createPost=Post::create([
	                    'title' => $post->title,
	                    'image' => $post->image,
	                    'description' => $post->description,
	                    'user_id' => $users->id,
	                    'shared_post_id'=>$post->id,
	                    'shared_user_id'=>$post->user_id,
	                    'mood_id'=>$post->mood_id,    
	                ]);
	 				$message="Successfully shared the post";
	 			}
	 			else{
	 				$message="Something went wrong";

	 			}
	 		}
	 		 return response()->json([
			       'success' => $message,

			 ]);
	 }
	 else{
        $message = "Unauthorized";
       }
	   return response()->json([
	        'message' => $message
	    ]);
	}
	public function deletePost(Request $request){
	 $users = auth()->user();
	 if($users){
	 	$input['post_id'] = $request->post_id;
	 	$rules = array(
		    'post_id' => 'required',
		   );
		$validator = Validator::make($input, $rules);
		if ($validator->fails()) {
            $message ="Please fill the required fields";
        }
        else{ 
        	if($request->type=='saved'){
        		$saved=Savedpost::find($request->post_id)->delete();
        		if($saved){
        			$message="Post deleted successfully";
        		}
        		else{
		 			$message="Something went wrong";
		 		}
        	}
        	else{
        		$check_ownership=Post::where('id',$request->post_id)->first();
	        	if($check_ownership){
	        		if($check_ownership->user_id == $users->id){
			        		$deletePost=Post::find($check_ownership->id)->delete();
					 		if($deletePost){
								$mood=Mood::where('post_id',$check_ownership->id)->first();
								if($mood){
									$mood->delete();
								}
					 			$message="Post deleted successfully";
					 		}
					 		else{

					 			$message="Something went wrong";
					 		}
			        	}
			        	else{
			        		$message="Your not the owner of this post";
			        	}
			        	
	        	}
	 			else{
	 				$message="post not available";
	 			}
        	}
        	
	 		
	 	}
	 }
	 else{
        $message = "Unauthorized";
       }
	  
	   return response()->json([
	        'message' => $message
	    ]);
	}
	public function shareCount(Request $request){
	 $users = auth()->user();
	 if($users){
	 	$input['post_id'] = $request->post_id;
	 	$rules = array(
		    'post_id' => 'required',
		   );
		$validator = Validator::make($input, $rules);
		if ($validator->fails()) {
            $message ="Please fill the required fields";
        }
        else{
	 		$postCount=Post::where('shared_post_id',$request->post_id)->count();
			return response()->json([
				 'success' => 'True',
		        'data' => $postCount
		    ]);
	 	}
	 }
	 else{
        $message = "Unauthorized";
       }
	   return response()->json([
	        'message' => $message
	    ]);
	}
	public function cms(Request $request){
		 $users = auth()->user();
	 if($users){
	 	$input['title'] = $request->title;
	 	$rules = array(
		    'title' => 'required',
		   );
		$validator = Validator::make($input, $rules);
		if ($validator->fails()) {
            $message ="Please fill the required fields";
        }
        else{
	 		$cms=Cms::where('title',$request->title)->first();
			return response()->json([
				 'success' => 'True',
		        'data' => $cms
		    ]);
	 	}
	 }
	 else{
        $message = "Unauthorized";
       }
	   return response()->json([
	        'message' => $message
	    ]);
	}
	public function patterns(Request $request){
		$users = auth()->user();
	 	if($users){
	 		$input['patterns'] = $request->patterns;
		 	$rules = array(
			    'patterns' => 'required',
			   );
			$validator = Validator::make($input, $rules);
			if ($validator->fails()) {
	            $message ="Please fill the required fields";
	            return response()->json([
			        'message' => $message
			    ]);
	        }
	        else{
	        	if($request->patterns=='1'){
	        		$date = \Carbon\Carbon::today();
		        	$sad=Mood::where('mood_flag','sad')->where('user_id',$users->id)->where('created_at', '>=', $date)->count();
			 		$love=Mood::where('mood_flag','love')->where('user_id',$users->id)->where('created_at', '>=', $date)->count();
			 		$happy=Mood::where('mood_flag','happy')->where('user_id',$users->id)->where('created_at', '>=', $date)->count();
			 		$weird=Mood::where('mood_flag','weird')->where('user_id',$users->id)->where('created_at', '>=', $date)->count();
			 		$crazy=Mood::where('mood_flag','crazy')->where('user_id',$users->id)->where('created_at', '>=', $date)->count();
			 		$scary=Mood::where('mood_flag','scary')->where('user_id',$users->id)->where('created_at', '>=', $date)->count();
	        	}
		 		elseif($request->patterns=='2'){
					$date = \Carbon\Carbon::today()->subDays(7);
					$sad=Mood::where('mood_flag','sad')->where('user_id',$users->id)->where('created_at', '>=', $date)->count();
		 			// $sad=Mood::where('mood_flag','sad')->whereBetween('created_at', [$start_week, $end_week])->count();
			 		$love=Mood::where('mood_flag','love')->where('user_id',$users->id)->where('created_at', '>=', $date)->count();
			 		$happy=Mood::where('mood_flag','happy')->where('user_id',$users->id)->where('created_at', '>=', $date)->count();
			 		$weird=Mood::where('mood_flag','weird')->where('user_id',$users->id)->where('created_at', '>=', $date)->count();
			 		$crazy=Mood::where('mood_flag','crazy')->where('user_id',$users->id)->where('created_at', '>=', $date)->count();
			 		$scary=Mood::where('mood_flag','scary')->where('user_id',$users->id)->where('created_at', '>=', $date)->count();
		 		}
		 		elseif($request->patterns=='3'){
		 			$currentMonth = date('m');
		 			$sad=Mood::where('mood_flag','sad')->where('user_id',$users->id)->whereRaw('MONTH(created_at) = ?',[$currentMonth])->count();
			 		$love=Mood::where('mood_flag','love')->where('user_id',$users->id)->whereRaw('MONTH(created_at) = ?',[$currentMonth])->count();
			 		$happy=Mood::where('mood_flag','happy')->where('user_id',$users->id)->whereRaw('MONTH(created_at) = ?',[$currentMonth])->count();
			 		$weird=Mood::where('mood_flag','weird')->where('user_id',$users->id)->whereRaw('MONTH(created_at) = ?',[$currentMonth])->count();
			 		$crazy=Mood::where('mood_flag','crazy')->where('user_id',$users->id)->whereRaw('MONTH(created_at) = ?',[$currentMonth])->count();
			 		$scary=Mood::where('mood_flag','scary')->where('user_id',$users->id)->whereRaw('MONTH(created_at) = ?',[$currentMonth])->count();
		 		}
		 		else{
		 			$sad=Mood::where('mood_flag','sad')->where('user_id',$users->id)->count();
			 		$love=Mood::where('mood_flag','love')->where('user_id',$users->id)->count();
			 		$happy=Mood::where('mood_flag','happy')->where('user_id',$users->id)->count();
			 		$weird=Mood::where('mood_flag','weird')->where('user_id',$users->id)->count();
			 		$crazy=Mood::where('mood_flag','crazy')->where('user_id',$users->id)->count();
			 		$scary=Mood::where('mood_flag','scary')->where('user_id',$users->id)->count();
		 		}
		 		return response()->json([
				 'success' => 'True',
				 'sad'=>$sad,
				 'love'=>$love,
				 'happy'=>$happy,
				 'weird'=>$weird,
				 'crazy'=>$crazy,
				 'scary'=>$scary,

		        // 'data' => $posts
		    ]);
		 	}
            // $posts=Post::with('user','mood','shared_user')->where('user_id',$users->id)->where('status',1)->get();
			
	 	}
	 else{
        $message = "Unauthorized";
          return response()->json([
	        'message' => $message
	    ]);
       }
	 
	}
	public function logoutApi()
	{ 
	    if (Auth::check()) {
	       Auth::user()->AauthAcessToken()->delete();
	       $message = "Successfully  logged out";
	    }else{
	    	 $message = "Something went wrong";
	    }
	     return response()->json([
	        'message' => $message
	    ]);
	}
	
}