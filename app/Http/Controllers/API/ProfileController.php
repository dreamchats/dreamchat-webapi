<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use App\Resetpassword;
use Illuminate\Support\Facades\Hash;
use Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class ProfileController extends Controller
{
	public function index()
    {
        $users = auth()->user();
        
        return response()->json([
            'success' => true,
            'data' => $users
        ]);
    }
    public function profile(Request $request){
         $users = auth()->user();
         if($users){
            $profile=User::where('id',$users->id)->first();
             // dd($profile['image']);
            return response()->json([
                'success' => 'True',
                'data' => $profile,
            ]);
        }
        else{
           $message="Unauthorized"; 
           return response()->json([
                'message' => $message
            ]);
        }
         
    }
    public function edit_profile(Request $request){
        $users = auth()->user();
        $email=$request->email;
        if($users){
            if($request->image != null){
                $base64_image = $request->image;
                if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                $data = substr($base64_image, strpos($base64_image, ',') + 1);

                $data = base64_decode($data);
                //dd($data);
                $image_name=$request->full_name ."_".$users->id. ".png";
                //dd($image_name); 
                Storage::disk('local')->put($image_name, $data);
                $image_names="/App/Images/".$image_name;
                }
            }
            else{
                $image_names="";
            }
            
      
        $profile=User::where('id',$users->id)->update([
            'full_name'=>$request->full_name,
            'mobile_number'=>$request->mobile_number,
            'dob'=>$request->dob,
            'gender'=>$request->gender,
            'zodiac'=>$request->zodiac,
            'image' =>$image_names

            ]);
            if($profile){
                $message="Profile updated successfully";
            }
            else{
                $message="Profile updation failed";
            }
        }
        else{
           $message="Unauthorized"; 
        }
         return response()->json([
            'message' => $message
        ]);
    }

    public function change_password(Request $request){
       $users = auth()->user();
       if (Hash::check($request->current_password, $users->password)) {
           $user=User::where('id',$users->id)->update([
            'password'=>Hash::make($request->new_password),
           ]);
           if($user){
             $message="Password changed successfully";
           }
           else{
                $message="Password updation failed";
           }
        }
        else{
            $message="Current password mismatch";
        }
         return response()->json([
            'message' => $message
        ]);
    }

     public function forget_password_mail(Request $request){
        $fourdigitrandom = rand(1000,9999); 
        $user_details=User::where('email',$request->email)->first();
        if($user_details){
            $reset_data=Resetpassword::where('user_id',$user_details->id)->first();
            if($reset_data){
                $otp=Resetpassword::where('user_id',$user_details->id)->update([
                    'otp'=>$fourdigitrandom,
                    'user_id'=>$user_details->id
                 ]);
            }
            else{
                $otp=Resetpassword::create([
                    'otp'=>$fourdigitrandom,
                    'user_id'=>$user_details->id
                 ]);
            }
        }
        else{
            $message="user not exist";  
        }
        
        if($otp){
            Mail::send('email.success', ['request' => $request,'fourdigitrandom'=>$fourdigitrandom], function ($m) use ($request,$fourdigitrandom) {
            $m->from('hello@app.com', 'Dreamchat ');
            $m->to($request->email, $request->full_name)->subject(' Dreamchat reset - One time password for activation');

            });
            $message="OTP was regenerated! please check email for verification code";
        }
       else{

            $message="Cannot able to generate OTP,Try again ";
       }
        return response()->json([
            'message' => $message,
        ], 201);
    }

    public function forget_password_verifyOTP(Request $request){
        $fourdigitrandom = rand(1000,9999); 
        $user_details=User::where('email',$request->email)->first();
        $otp=$request->otp;
        $otp_data=Resetpassword::where('user_id',$user_details->id)->first();
        if($otp == $otp_data->otp){
           $message="OTP Match";
        }
        else{
           $message='OTP Mismatch';
        } 
        return response()->json([
            'message' => $message,
        ], 201);
    }

     public function forget_password_update(Request $request){
        $fourdigitrandom = rand(1000,9999); 
        $user_details=User::where('email',$request->email)->first();
        if($user_details){
            $user_update=User::where('email',$request->email)->update([
                'password'=>Hash::make($request->new_password)
            ]);   
            if($user_update){
                $message='Password changed Successfully';
            }
            else{
                $message='Password updation failed';
            } 
        }
        else{
           $message='User doesnot exist';
        } 
        return response()->json([
            'message' => $message,
        ], 201);
    }
}