<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Comments;
use App\Post;
use App\Notifications;
use App\LikesComment;
class CommentsController extends Controller{

	public function addComment(Request $request){
		$users = auth()->user();
		 if($users){
		 	$input['post_id'] = $request->post_id;
		 	$input['comments'] = $request->comments;
		 	$input['comments_id'] = $request->comments_id;
		 	//print_r($request->comments_id);
		 	$rules = array(
			    'comments' => 'required',
			    'post_id'=>'required',
			     'comments_id'=>'required',
			   );
			$validator = Validator::make($input, $rules);
			if ($validator->fails()) {
	            $message ="Please fill the required fields";
	        }
	        else{
	        	$posts=Post::where('id',$request->post_id)->first();
	        	if($posts){
	        		$comments=Comments::create([
			 			'comments_description'=>$request->comments,
			 			'post_id'=>$request->post_id,
			 			'user_id'=>$users->id,
			 			'comment_id'=>$request->comments_id,
       	                'parent_id'=>$request->comments_id,
			 		]);

			 		if($comments){
			 			$notification_create=Notifications::create([
                        'user_id'=>$users->id,
                        'request_from'=>$users->id,
                        'request_to' =>0,
                        'message'=>'Comment added',
                        'comment_id'=>$comments->id,
                        'parent_id'=>$comments->id,
                        'app'=>1,
                        'status'=>1
                    ]);
			 			$message="comments successfully added";
			 		}
			 		else{
			 			$message="error while adding";
			 		}
	        	}
			 	else{
			 		$message="Post not available ,Kindly try again";
			 	}	
		 	}
		 }
		 else{
	        $message = "Unauthorized";
	       }
		   return response()->json([
		        'message' => $message
		    ]);
		}
		public function replyComment(Request $request){
		$users = auth()->user();
		 if($users){
		 	$input['post_id'] = $request->post_id;
		 	$input['comments'] = $request->comments;
		 	$input['comments_id'] = $request->comments_id;

		 	$rules = array(
			    'comments' => 'required',
			    'post_id'=>'required',
			    'comments_id'=>'required',
			   );
			$validator = Validator::make($input, $rules);
			if ($validator->fails()) {
	            $message ="Please fill the required fields";
	        }
	        else{
	        	$posts=Post::where('id',$request->post_id)->first();
	        	if($posts){
	        		$comment_check=Comments::where('id',$request->comments_id)->first();
		        		if($comment_check){
		        			$comments=Comments::create([
				 			'comments_description'=>$request->comments,
				 			'post_id'=>$request->post_id,
				 			'user_id'=>$users->id,
				 			'comment_id'=>$request->comments_id,
				 			'parent_id'=>$request->comments_id
				 		]);
				 		if($comments){
				 			$notification_create=Notifications::create([
		                        'user_id'=>$users->id,
		                        'request_from'=>$users->id,
		                        'request_to' =>0,
		                        'message'=>'Comment added',
		                        'comment_id'=>$comments->id,
		                        'app'=>1,
		                        'status'=>1
		                    ]);
				 			$message="comments successfully added";
				 		}
				 		else{
				 			$message="error while adding";
				 		}
	        		}
	        		else{
	        			$message="commnet doesnot exist";                                
	        		}
	        		
	        	}
			 	else{
			 		$message="Post not available ,Kindly try again";
			 	}	
		 	}
		 }
		 else{
	        $message = "Unauthorized";
	       }
		   return response()->json([
		        'message' => $message
		    ]);
		}


		public function getComments(Request $request){
		$users = auth()->user();
		 if($users){
		 	$input['post_id'] = $request->post_id;

		 	$rules = array(
			    'post_id'=>'required',
			   );
			$validator = Validator::make($input, $rules);
			if ($validator->fails()) {
	            $message ="Please fill the required fields";
	        }
	        else{
	        	$data = collect([]);
	        	$rdata = collect([]);

	        	$posts=Post::where('id',$request->post_id)->first();
	        	if($posts){
	        		  //$comments=Comments::with('user')->where('post_id',$request->post_id)->get()->toTree();
	        		$comments=Comments::with('children.user','user','likes')->where('post_id',$request->post_id)->where('comment_id',0)->get();
	        		 foreach ($comments as $comment) {
	        		 	if($comment->children){
	        		 		foreach ($comment->children as $child) {
		        		 		$like_co=LikesComment::where('comment_id',$child->id)->where('user_id',$users->id)->where('status',1)->get();
			        		 	$child->setAttribute('like', $like_co);
		                        	//print_r('Hi');
		                    	if ($data->where('id', $comment->id)->count() === 0){
									$data->push($comment);
								} 
		        		 	}
	        		 	}
	        		 	$like_co=LikesComment::where('comment_id',$comment->id)->where('user_id',$users->id)->where('status',1)->get();
	        		 	$comment->setAttribute('like', $like_co);
                        	//print_r('Hi');
                    	if ($data->where('id', $comment->id)->count() === 0){
							$data->push($comment);
						} 
					} 
					return response()->json([
	        		 	'success'=>1,
				        'data' => $data
				    ]);
	        		//  if($comments){
	        		//  	   return response()->json([
				       //  		 	'success'=>1,
							    //     'data' => $data
							    // ]);
       //                  foreach ($comments as $comment) {
       //                  	print_r('Hi');
       //                  	if ($data->where('id', $comment->id)->count() === 0){
							// 	$data->push($comment);
							// }  
       //                  	$childcomments=Comments::where('parent_id',$comment->id)->where('post_id',$request->post_id)->get();
                        	
       //                  	$data->push($childcomments);

		                
       //             		 }
	 				// }
      //               else{
      //                   $data=$data;
      //               }
	       //  		 return response()->json([
	       //  		 	'success'=>1,
				    //     'data' => $data
				    // ]);
	        	}
			 	else{
			 		$message="Post not available ,Kindly try again";
			 	}	
		 	}
		 }
		 else{
	        $message = "Unauthorized";
	       }
		   return response()->json([
		        'message' => $message
		    ]);
		}
		public function getReply($id,$post_id){

			// $sql = "select * from category where parent_id ='".$catid."'";
			// $result = $conn->query($sql);
			// $comments_data=Comments::where('comment_id',$id)->get(); 
			// while($comments_data):
			// $i = 0;
			// if ($i == 0) echo '
			// <ul>';
			//  echo '
			// <li>' . $comments_data->comments_description;
			//  getReply($comments_data->comment_id,$post_id);
			//  echo '</li>
			 
			// ';
			// $i++;
			//  if ($i > 0) echo '</ul>
			 
			// ';
			// endwhile;

			//  $data = collect([]);
			 $comments=Comments::where('comment_id',$id)->get()->toTree();
			// foreach ($comments as $key => $comment) {
			// 	if($comment->comment_id == 0){
			// 		if ($data->where('id', $comment->id)->count() === 0){
   //                      $data->push($comment);
   //                      $this->getReply($comment->id,$post_id);
   //                  }  
			// 	}
			// 	else{
			// 		 $this->getReply($comment->id,$post_id);

			// 	}
			// }

			// while($comments):
			// 	$i = 0;
			// 	if ($i == 0) echo '
			// 	<ul>';
			// 	 echo '
			// 	<li>' . $comments->comments_description;
			// 	 $this->getReply($comments->id,$post_id);
			// 	 echo '</li>
				 
			// 	';
			// 	$i++;
			// 	 if ($i > 0) echo '</ul>
				 
			// 	';
			// 	endwhile;
			$message=$comments;
				}
		
}