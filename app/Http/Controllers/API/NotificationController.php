<?php


namespace App\Http\Controllers\API;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Notifications;
use Mail;
class NotificationController extends Controller
{
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function appNotification(Request $request)
    {
        $users = auth()->user();
        if($users){
          // $message="User exists";
        $data = collect([]);
        $app_notifications=Notifications::where('app',1)->where('all_read',0)->where('user_id',$users->id)->get();
        return response()->json([
                'sucess' => 'True',
                'data'=> $app_notifications
            ] );
        }
        else{
           $message = "Unauthorized"; 
            return response()->json([
                'message' => $message,
            ], 201);
        }
    }
    public function pushNotification(Request $request)
    {
        $users = auth()->user();
        $data = collect([]);
        if($users){
          // $message="User exists";
          $push_notifications=Notifications::where('app',0)->where('all_read',0)->where('user_id',$users->id)->get();
           return response()->json([
                'sucess' => 'True',
                'data'=> $push_notifications
            ] );
        }
        else{
           $message = "Unauthorized"; 
            return response()->json([
            'message' => $message,
        ], 201);
        }
    }
   public function readNotification(Request $request)
    {
        $users = auth()->user();
        $data = collect([]);
        if($users){
          // $message="User exists";
          $push_notifications=Notifications::where('app',0)->where('user_id',$users->id)->get();
          foreach ($push_notifications as $push_notification) {
                $notify=Notifications::where('app',0)->where('id',$push_notification->id)->update([
                'all_read'=>'1'
              ]);
              
          }
         $message="success";
        }
        else{
           $message = "Unauthorized"; 
          
        }
        return response()->json([
            'message' => $message,
        ], 201);
    }
  
}
