<?php
namespace App\Http\Controllers\Auth;

use App\User;
use Exception;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Http\Request;


class SocialAuthController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * List of providers configured in config/services acts as whitelist
     *
     * @var array
     */
    protected $providers = [
        'github',
        'facebook',
        'google',
        'twitter'
    ];

    /**
     * Show the social login page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        return view('auth.login');
    }

    /**
     * Redirect to provider for authentication
     *
     * @param $driver
     * @return mixed
     */
    public function redirectToProvider($driver)
    {
        if( ! $this->isProviderAllowed($driver) ) {
            return $this->sendFailedResponse("{$driver} is not currently supported");
        }

        try {
            return Socialite::driver($driver)->redirect();
        } catch (Exception $e) {
            // You should show something simple fail message
            return $this->sendFailedResponse($e->getMessage());
        }
    }

    /**
     * Handle response of authentication redirect callback
     *
     * @param $driver
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handleProviderCallback( $driver )
    {
        try {
            $user = Socialite::driver($driver)->user();
        } catch (Exception $e) {
            return $this->sendFailedResponse($e->getMessage());
        }

        // check for email in returned user
        return empty( $user->email )
            ? $this->sendFailedResponse("No email id returned from {$driver} provider.")
            : $this->loginOrCreateAccount($user, $driver);
    }

    /**
     * Send a successful response
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendSuccessResponse()
    {
        return redirect()->intended('home');
    }

    /**
     * Send a failed response with a msg
     *
     * @param null $msg
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendFailedResponse($msg = null)
    {
        return redirect()->route('social.login')
            ->withErrors(['msg' => $msg ?: 'Unable to login, try with another provider to login.']);
    }

    protected function loginOrCreateAccount(Request $request)
    {
        // check for already has account
        
        $user = User::where('email', $request->email)->first();
       // dd($user);
        // if user already found
        if( $user ) {
            // update the avatar and provider that might have changed
            if($user->email == $request->email){
                $users=User::where('email', $request->email)->update([
                    // 'avatar' => $providerUser->avatar,
                    'provider' => $request->provider,
                    'provider_id' => $request->provider_id,
                    'access_token' => $request->access_token
                ]);
                 $userdata = array(
                      'provider_id' => $request->provider_id,
                      'password'=>'123456789'
                    );
                     if (Auth::attempt($userdata)) {
                       $user = Auth::user();
                        $token = $user->createToken('My API')->accessToken;
                         return response()->json([
                            'success'=>'True',
                            'token' => $token,
                            'user'=>$user
                        ]);
                   } else{
                        return response()->json([
                            'success'=>'false'
                        ]);
                     }
            }
            
        } else {
           // dd($request);
            $provider=$request->provider;
            $provider_id=$request->provider_id;
            //create a new userd
            $user = User::create([
                'provider'=>$request->provider,
                'provider_id'=>$request->provider_id,
                'full_name'=>$request->full_name,
                'email'=>$request->email,
                'access_token' => $request->access_token,
                'password' => bcrypt('123456789')
            ]);
             $userdata = array(
            'provider_id' => $request->provider_id,
            'password'=>'123456789'
            );
             if (Auth::attempt($userdata)) {
                $user = Auth::user();
                $token = $user->createToken('My API')->accessToken;
                 return response()->json([
                     'success'=>'True',
                    'token' => $token,
                    'user'=>$user
                ]);
             }else{
                return response()->json([
                    'success'=>'false'
                ]);
             }
        }
        
          // return response()->json(['token' => $token]);
         //  } 
        // return response()->json([
        //     'success'=>1,
        //     'data' => $token
        // ]);
        // login the user
        //Auth::($user, true);
    // Auth::attempt($user);
// if (Auth::attempt(['provider_id',$request->input('provider_id')])) {
//         $user = Auth::user();
//         $token = $user->createToken('My API')->accessToken;
//         return response()->json(['token' => $token]);
//     }

      //  return $this->sendSuccessResponse();
    }

    /**
     * Check for provider allowed and services configured
     *
     * @param $driver
     * @return bool
     */
    private function isProviderAllowed($driver)
    {
        return in_array($driver, $this->providers) && config()->has("services.{$driver}");
    }
    public function FB_login(Request $requesy){
        $user = Socialite::driver('facebook')->user();
        $create['name'] = $user->getName();
        $create['email'] = $user->getEmail();
        $create['facebook_id'] = $user->getId();
        $userModel = new User;
        $createdUser = $userModel->addNew($create);
        Auth::loginUsingId($createdUser->id);

    }
}
