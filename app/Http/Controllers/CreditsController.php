<?php

namespace App\Http\Controllers;

use View;
use Illuminate\Http\Request;
use Auth;
use App\Credits;
use Illuminate\Support\Facades\Validator;

class CreditsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
	 public function listcredits(){
    	$credits=Credits::get();
        return View::make('credits/credits')->with(compact('credits'));
    }
	public function credits(Request $request){
    	$credits=Credits::get();
		return json_encode($credits);
	}
	 public function blockPlan(Request $request){
        $plan_id= $request->id;
        $block_plan=Credits::where('id',$plan_id)->update([
            'status' =>2
        ]);
        if($block_plan){
            $message="1";
        }
        else{
            $message="2";
        }
        return json_encode($message);

    }
    public function unblockPlan(Request $request){
        $plan_id= $request->id;
        $block_plan=Credits::where('id',$plan_id)->update([
            'status' =>1
        ]);
        if($block_plan){
            $message="1";
        }
        else{
            $message="2";
        }
        return json_encode($message);

    }
    public function editplan($id){
    	$credits=Credits::where('id',$id)->first();
    	return json_encode($credits);
    }
    public function updatePlan(Request $request){
    	$id = $request->id;
    	$status = $request->status;
    	$credits_points = $request->credits_points;
    	$amount = $request->amount;

    	$credits = Credits::where('id', $id)->update([
    		'credits_points' => $credits_points,
    		'amount' => $amount,
    		'status' => $status
    	]);

    	if($credits) {
    		$success = '1';
    	} else{
            $success="2";
        }
        return $success;

    }
    public function deletePlan(Request $request){
    	$plan_id= $request->id;
    	$delete_credits=Credits::find($plan_id)->delete();
    	if($delete_credits){	
    		$success = '1';
    	} else{
            $success="2";
        }
        return $success;
    }

    public function newCredit(Request $request)
    {
        $credits_points = $request->credits_points;
        $amount = $request->amount;
        $status = $request->status;

        $credits = Credits::create([
            'credits_points' => $credits_points,
            'amount' => $amount,
            'status' => $status
        ]);

        return redirect()->route('listcredits'); 

    }
}
