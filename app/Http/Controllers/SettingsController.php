<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Settings;
use View;
use Auth;
class SettingsController extends Controller
{
   	 public function __construct()
    {
        $this->middleware('auth');
    }
    public function settings(Request $request){
        $user=Auth::user();
        $settings=Settings::where('user_id',$user->id)->first();
        if($settings){
            $settings=$settings;
        }
        else{
            $settings=Settings::create([
                'meta_name'=>"",
                'meta_description'=>"",
                'user_id'=>$user->id,
                'report_counts'=>"",
                'credit_per_request'=>""
            ]);
            $settings=Settings::where('user_id',$user->id)->first();
        }
        return View::make('profile/settings')->with(compact('settings'));
    }
    public function theme_setting(Request $request){
       // dd($request);
    	$color=$request->theme_color;
    	$user_id=Auth::User()->id;
    	$settings=Settings::where('user_id',$user_id)->first();;
    	if($settings){
    		$update=Settings::where('user_id',$user_id)->update([
    		'theme_color'=>$color,
            'meta_name'=>$request->meta_name,
            'meta_description'=>$request->meta_description,
            'report_counts'=>$request->report_count,
            'credit_per_request'=>$request->credit_per_request
    		]);
             if($update==1){  
                return redirect()->back()->with('theme_success', 'Settings updated successfully');
            }
            else{
                 return redirect()->route('theme_setting')->withErrors('theme_error','Something went wrong');
            }
    	}
    	else{
    		$create=Settings::create([
    		'theme_color'=>$color,

    		]);
            if($create==1){  
                return redirect()->back()->with('theme_success', 'Settings updated successfully');
            }
            else{
                 return redirect()->route('theme_setting')->withErrors('theme_error','Something went wrong');
            }
    	}
        
    }
}
