<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use View;
use Illuminate\Http\Request;
use Auth;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function adminUsers(){
    	$user=User::where('role_id',1)->get();
        return View::make('admin/users')->with(compact('user'));
    }

    public function admin(){
    	$user=User::where('role_id',1)->get();
       return json_encode($user);
    }
     public function newAdmin(Request $request){
        $user=User::where('role_id',1)->get();
        $input=[
            'email' => $request->email,
        ];
        $messages = [
            'email.required' => '',
        ];
        $rules = array(
        'email'=>'required|unique:login',
        );

        $validator = Validator::make($input, $rules, $messages);
        if ($validator->fails()) {
            return Redirect::back()
                        ->withErrors($validator)
                        ->withInput();
        }else{
            $characters='123456789abcdefghijklmnopqrstuvwxyz';
            $password=rand(0,strlen($characters)); 

             User::create([
                'full_name' => $request->name,
                'email' => $request->email,
                'gender' =>  $request->gender,
                'mobile_number' =>  $request->phone_number,
                'address' => $request->address,   
                // 'dob' => $request->dob,        
                'password' => Hash::make($password),
                'role_id'=>1,
                'status' =>1,
            ]);
             return redirect('/admin/users');
        }
    	

    }
    public function appUsers(){
    	$user=User::with('user_credits')->where('role_id',0)->get();
        // dd($user);
        return View::make('users/users')->with(compact('user'));
    }
      public function users(){
        $user=User::with('user_credits')->where('role_id',0)->get();
        return json_encode($user);
    }
    public function blockuser(Request $request){
        $user_id= $request->id;
        $block_user=User::where('id',$user_id)->update([
            'status' =>2
        ]);
        if($block_user){
            $message="1";
        }
        else{
            $message="2";
        }
        return json_encode($message);

    }
    public function unblockuser(Request $request){
        $user_id= $request->id;
        $block_user=User::where('id',$user_id)->update([
            'status' =>1
        ]);
        if($block_user){
            $message="1";
        }
        else{
            $message="2";
        }
        return json_encode($message);

    }
    public function unique_email(Request $request){
        $email = $request->email;
        $user=User::where('email',$email)->first();
        if($user){
            $message= "Email exist";
            $success=1;

        }else{
         $message= "Doesnot exist";
         $success=2;
        }
          return response()->json([
            'success'=>$success,
            'message' => $message
        ]);
    }
}
