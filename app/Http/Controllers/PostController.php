<?php

namespace App\Http\Controllers;

use View;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Post;
use App\CustomerCredits;
use App\Reportpost;
class PostController extends Controller
{
    public function getPosts($id){
    	//print_r($id);
    	$posts=Post::where('user_id',$id)->get();
    	return json_encode($posts);
    }
    public function getCredits($id){
        //print_r($id);
        $credit=CustomerCredits::where('user_id',$id)->get();
        return json_encode($credit);
    }
     public function blockpost(Request $request){
        $post_id= $request->id;
        $block_post=Post::where('id',$post_id)->update([
            'status' =>2
        ]);
        if($block_post){
            $message="1";
        }
        else{
            $message="2";
        }
        return json_encode($message);

    }
    public function unblockpost(Request $request){
        $post_id= $request->id;
        $block_post=Post::where('id',$post_id)->update([
            'status' =>1
        ]);
        if($block_post){
            $message="1";
        }
        else{
            $message="2";
        }
        return json_encode($message);

    }
    public function viewPost(Request $request)
    {
        $id = $request->id;
        $posts=Post::where('id',$id)->first();
        if ($posts) {
            $posts;
        }else{
            $posts = array();
        }
        return json_encode($posts);
    }
    public function adminViewpost(){
        return View::make('admin/post_verification')->with(compact(''));
    }
    public function getAllReportedPosts(Request $request){
        $post=Reportpost::with('posts')->where('verified',2)->get();
        return json_encode($post);
    }

     public function adminBlockPost(Request $request){
        $post_id= $request->id;
        $block_post=Post::where('id',$post_id)->update([
            'status' =>2
        ]);
        if($block_post){
            $reportPost=Reportpost::where('post_id',$post_id)->update([
            'verified' =>1
          ]);
            $message="1";
        }
        else{
            $message="2";
        }
        return json_encode($message);

    }
     public function reactivatePost(Request $request){
        $post_id= $request->id;
        $block_post=Post::where('id',$post_id)->update([
            'status' =>1
        ]);
        if($block_post){
          $reportPost=Reportpost::where('post_id',$post_id)->update([
            'verified' =>1
          ]);
            $message="1";
        }
        else{
            $message="2";
        }
        return json_encode($message);

    }

}
