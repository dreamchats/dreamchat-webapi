<?php

namespace App\Http\Controllers;

use View;
use Illuminate\Http\Request;
use Auth;
use App\Transaction;

class TransactionController extends Controller
{
	 public function __construct()
    {
        $this->middleware('auth');
    }
    public function getAllTransaction(){
    	//print_r($id);
    	$transaction=Transaction::get();
    	return json_encode($transaction);
    }

	public function transaction(){
    	$transaction=Transaction::get();
	    return View::make('transaction/transaction')->with(compact('transaction'));
	}
}
