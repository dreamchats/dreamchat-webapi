<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reportpost extends Model
{
    protected $fillable = [
        'post_id', 'user_id','reason','read_post','verified'
    ];
    protected $table = 'reported_posts';
    public $timestamps = true;
     public function posts()
    {
        return $this->belongsTo('App\Post','post_id');
    }
      public function user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}
}
