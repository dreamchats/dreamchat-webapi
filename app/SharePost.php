<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SharePost extends Model
{
    protected $fillable = [
        'post_id', 'user_id',
    ];
    protected $table = 'share_post';
    public $timestamps = true;
}
