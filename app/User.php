<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\URL;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
    protected $table = 'login';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email','password','full_name','role_id','mobile_number','dob','gender','zodiac','image','account_type','address','report_count','provider','provider_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','access_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
     public function request_from()
    {
        return $this->belongsTo('App\Friends', 'id','request_from');
    }
     public function request_to()
    {
        return $this->belongsTo('App\Friends', 'id','request_to');
    }
     public function user_credits()
    {
        return $this->belongsTo('App\CustomerCredits', 'id','user_id');
    }
    public function AauthAcessToken(){
        return $this->hasMany('\App\OauthAccessToken');
    }
    public function getImageAttribute($image)
    {
        $image_url=URL::to('');
        // print_r($image_url);
        if($image == "" ||  $image == $image_url ){
            // print_r("ini user model");
            $this->attributes['user_image']=null;
        }else{
            $this->attributes['user_image'] = $image_url.$image;
            return $this->attributes['user_image'] ;
        }
        
      }

      // protected $appends = ['image'];

}
