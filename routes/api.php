<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::post('login', 'API\RegisterController@login');
Route::post('signup', 'API\RegisterController@signup');
Route::post('otp_verification', 'API\RegisterController@otp_verify');
Route::post('otp_resend', 'API\RegisterController@otp_resend');
Route::post('email_validation', 'API\RegisterController@email_validation');

Route::post('reported_otp_verify', 'API\PostController@reported_otp_verify');
Route::post('otp_reactivation', 'API\PostController@otp_reactivation');

Route::middleware('auth:api')->group(function () {
   	Route::get('user', 'API\ProfileController@index');
   	Route::post('edit_profile', 'API\ProfileController@edit_profile');
   	Route::post('profile', 'API\ProfileController@profile');
   	Route::post('change_password', 'API\ProfileController@change_password');
   	Route::post('createPost', 'API\PostController@createPost');
 	Route::get('publicWall', 'API\PostController@publicWall');
 	Route::get('myWall', 'API\PostController@myWall');
 	Route::get('friendsWall', 'API\PostController@friendsWall');
 	Route::get('getFriends', 'API\PostController@getFriends');
 	Route::get('getUsers', 'API\PostController@getUsers');
 	Route::post('sendRequest', 'API\PostController@sendRequest');
 	Route::post('manageRequest', 'API\PostController@manageRequest');
 	Route::get('getRequest', 'API\PostController@getRequest');
 	Route::get('getPosts', 'API\PostController@getPosts'); 	
 	Route::post('follow', 'API\PostController@follow');
 	Route::post('changeAccount', 'API\PostController@changeAccount');
 	Route::get('getFollowings', 'API\PostController@getFollowings');
 	Route::get('getFollowers', 'API\PostController@getFollowers'); 	
 	Route::get('getFollowingsList', 'API\PostController@getFollowingsList');
 	Route::get('getFollowersList', 'API\PostController@getFollowersList'); 	
 	Route::post('savePost', 'API\PostController@savePost');
 	Route::post('listSavePost', 'API\PostController@listSavePost');
 	Route::post('reportPost', 'API\PostController@reportPost');
 	Route::post('likes', 'API\ActionController@likes');
 	Route::post('addLikes', 'API\ActionController@addLikes');
 	Route::post('unLikes', 'API\ActionController@unLikes');
 	Route::post('checkLikes', 'API\ActionController@checkLikes');
 	Route::post('commentLikes', 'API\ActionController@commentLikes');
 	Route::post('addCommentLikes', 'API\ActionController@addCommentLikes');
 	Route::post('unLikesComments', 'API\ActionController@unCommentLikes');
 	Route::post('sharePost', 'API\ActionController@sharePost');
 	Route::post('deletePost', 'API\ActionController@deletePost');
 	Route::post('shareCount', 'API\ActionController@shareCount');
 	Route::post('creditPurchase', 'API\CreditController@creditPurchase');
 	Route::get('getCredits', 'API\CreditController@getCredits');
 	Route::get('listCredits', 'API\CreditController@listCredits');
 	Route::post('addComment', 'API\CommentsController@addComment');
 	Route::post('replyComment', 'API\CommentsController@replyComment');
 	Route::post('getComments', 'API\CommentsController@getComments');

 	Route::post('unfollowFollowing', 'API\PostController@unfollow_Following');
 	Route::post('unfollowFollowers', 'API\PostController@unfollow_Follower');

 	Route::post('appNotification', 'API\NotificationController@appNotification');
 	Route::post('pushNotification', 'API\NotificationController@pushNotification');
 	Route::post('readNotification', 'API\NotificationController@readNotification');

 	Route::post('cms', 'API\ActionController@cms');
 	Route::post('patterns', 'API\ActionController@patterns');
 	Route::post('logout','API\ActionController@logoutApi');
 	Route::get('posts/{page?}', 'API\PostController@post');



});
Route::post('forget_password_mail', 'API\ProfileController@forget_password_mail');
Route::post('forget_password', 'API\ProfileController@forget_password_update');
Route::post('forget_otp_verify', 'API\ProfileController@forget_password_verifyOTP');

Route::get('auth/social', 'Auth\SocialAuthController@show')->name('social.login');
Route::get('oauth/{driver}', 'Auth\SocialAuthController@redirectToProvider')->name('social.oauth');
Route::get('oauth/{driver}/callback', 'Auth\SocialAuthController@handleProviderCallback')->name('social.callback');

Route::post('/loginOrCreateAccount', 'Auth\SocialAuthController@loginOrCreateAccount')->name('sup ');
