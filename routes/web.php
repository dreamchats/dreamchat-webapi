<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//edit profile
Route::get('/profile', 'ProfileController@profile')->name('profile');
Route::post('/profile', 'ProfileController@Update_profile')->name('update_profile');
//change password
Route::get('/profile/settings', 'SettingsController@settings')->name('settings');
Route::post('/profile/updatePassword', 'ProfileController@updatePassword')->name('updatePassword');
Route::post('/profile/update_settings', 'ProfileController@update_setting')->name('update_setting');
Route::post('/theme_setting', 'SettingsController@theme_setting')->name('theme_setting');

Route::get('/profile/changePassword', 'ProfileController@changePassword')->name('changePassword');
Route::get('/admin/users', 'UsersController@adminUsers')->name('adminUsers');
Route::post('/admin/admins', 'UsersController@admin')->name('admins');

Route::post('/newAdmin', 'UsersController@newAdmin')->name('newAdmin');
//app users
Route::get('/admin/appUsers', 'UsersController@appUsers')->name('appUsers');
Route::post('/admin/users', 'UsersController@users')->name('users');
Route::post('/admin/viewPost', 'PostController@viewPost')->name('viewPost');

Route::get('/posts/{id}', 'PostController@getPosts')->name('getPost');
Route::get('/credits/{id}', 'PostController@getCredits')->name('getCredit');

Route::get('/admin/transaction', 'TransactionController@transaction')->name('transaction');
Route::post('/admin/transactions', 'TransactionController@getAllTransaction')->name('getAllTransaction');

Route::post('/blockuser', 'UsersController@blockuser')->name('blockuser');
Route::post('/unblockuser', 'UsersController@unblockuser')->name('unblockuser');

Route::post('/admin/unique_email', 'UsersController@unique_email')->name('unique_email');

Route::post('/blockpost', 'PostController@blockpost')->name('blockpost');
Route::post('/unblockpost', 'PostController@unblockpost')->name('unblockpost');

Route::post('/admin/credits', 'CreditsController@credits')->name('credits');
Route::get('/admin/credits', 'CreditsController@listcredits')->name('listcredits');
Route::post('/newCredit', 'CreditsController@newCredit')->name('newCredit');

Route::post('/blockPlan', 'CreditsController@blockPlan')->name('blockplan');
Route::post('/unblockPlan', 'CreditsController@unblockPlan')->name('unblockplan');

Route::get('/editPlan/{id}', 'CreditsController@editPlan')->name('editPlan');
Route::post('/admin/editPlan', 'CreditsController@updatePlan')->name('updatePlan');
Route::post('/deletePlan', 'CreditsController@deletePlan')->name('deletePlan');

Route::post('/admin/cms', 'CmsController@cms')->name('cms');
Route::get('/admin/cms', 'CmsController@listcms')->name('listcms');
Route::get('/editCms/{id}', 'CmsController@editCms')->name('editCms');
Route::post('/admin/editCms', 'CmsController@updateCms')->name('updateCms');
Route::post('/deleteCms', 'CmsController@deleteCms')->name('deleteCms');
Route::post('/newCms', 'CmsController@newCms')->name('newCms');

Route::post('/blockCms', 'CmsController@blockCms')->name('blockCms');
Route::post('/unblockCms', 'CmsController@unblockCms')->name('unblockCms');

Route::get('/admin/verifyPosts', 'PostController@adminViewpost')->name('verifyPosts');

Route::post('/admin/getAllReportedPosts', 'PostController@getAllReportedPosts')->name('getAllReportedPosts');
Route::post('/admin/blockpost', 'PostController@adminBlockPost')->name('adminBlockPost');
Route::post('/admin/reactivatePost', 'PostController@reactivatePost')->name('reactivatePost');


Route::post('/loginOrCreateAccount', 'Auth\SocialAuthController@loginOrCreateAccount')->name('sup ');

Route::get('auth/social', 'Auth\SocialAuthController@show')->name('social.login');
Route::get('oauth/{driver}', 'Auth\SocialAuthController@redirectToProvider')->name('social.oauth');
Route::get('oauth/{driver}/callback', 'Auth\SocialAuthController@handleProviderCallback')->name('social.callback');


Route::get('facebook', function () {
    return view('auth.login');
});
Route::get('auth/facebook', 'Auth\FacebookController@redirectToFacebook');
Route::get('auth/facebook/callback', 'Auth\FacebookController@handleFacebookCallback');